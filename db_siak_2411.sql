-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 24, 2014 at 12:43 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `notfix`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `biodata_wni_view`
--
CREATE TABLE IF NOT EXISTS `biodata_wni_view` (
`nik` varchar(16)
,`no_ktp` varchar(40)
,`tmpt_sbl` varchar(300)
,`no_paspor` varchar(30)
,`tgl_akh_paspor` date
,`nama_lgkp` varchar(60)
,`jenis_klmin` varchar(1)
,`tmpt_lhr` varchar(60)
,`tgl_lhr` date
,`akta_lhr` varchar(1)
,`no_akta_lhr` varchar(40)
,`gol_drh` varchar(3)
,`agama` varchar(20)
,`stat_kwn` varchar(1)
,`akta_kwn` varchar(1)
,`no_akta_kwn` varchar(40)
,`tgl_kwn` date
,`akta_crai` varchar(1)
,`no_akta_crai` varchar(40)
,`tgl_crai` date
,`stat_hbkel` varchar(2)
,`klain_fsk` varchar(1)
,`pnydng_cct` varchar(1)
,`pddk_akh` varchar(2)
,`jenis_pkrjn` varchar(2)
,`nik_ibu` varchar(16)
,`nama_lgkp_ibu` varchar(60)
,`nik_ayah` varchar(16)
,`nama_lgkp_ayah` varchar(60)
,`nama_ket_rt` varchar(60)
,`nama_ket_rw` varchar(60)
,`nama_pet_reg` varchar(60)
,`nip_pet_reg` varchar(18)
,`nama_pet_entri` varchar(60)
,`nip_pet_entri` varchar(18)
,`tgl_entri` date
,`no_kk` varchar(16)
,`jenis_bntu` varchar(2)
,`no_prop` varchar(2)
,`no_kab` varchar(2)
,`no_kec` varchar(2)
,`no_kel` varchar(4)
,`stat_hidup` varchar(10)
,`tgl_ubah` date
,`tgl_cetak_ktp` date
,`tgl_ganti_ktp` date
,`tgl_pjg_ktp` date
,`stat_ktp` varchar(1)
,`als_numpang` varchar(1)
,`pflag` varchar(1)
,`cflag` varchar(1)
,`sync_flag` varchar(1)
,`ket_agama` varchar(60)
,`kebangsaan` varchar(60)
,`gelar` varchar(5)
,`ket_pkrjn` varchar(60)
,`glr_agama` varchar(1)
,`glr_akademis` varchar(1)
,`glr_bangsawan` varchar(1)
,`is_pros_datang` varchar(1)
,`desc_pekerjaan` varchar(30)
,`desc_kepercayaan` varchar(30)
,`flag_status` varchar(1)
,`count_ktp` varchar(5)
,`count_biodata` varchar(5)
,`flagsink` varchar(50)
);
-- --------------------------------------------------------

--
-- Table structure for table `tbl_administrasi_kependudukan`
--

CREATE TABLE IF NOT EXISTS `tbl_administrasi_kependudukan` (
  `ad_id` int(11) NOT NULL,
  `jenis` varchar(40) NOT NULL,
  `keterangan` varchar(40) NOT NULL,
  PRIMARY KEY (`ad_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_administrasi_kependudukan`
--

INSERT INTO `tbl_administrasi_kependudukan` (`ad_id`, `jenis`, `keterangan`) VALUES
(1, 'Pembuatan Akta Kelahiran', ''),
(2, 'Pembuatan Kartu Keluarga', ''),
(3, 'Pembuatan KTP', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_akta_lahir_relation`
--

CREATE TABLE IF NOT EXISTS `tbl_akta_lahir_relation` (
  `bayi_no` int(16) NOT NULL,
  `kandidat_id` int(16) NOT NULL,
  `insert_datetime` date NOT NULL,
  `insert_by` varchar(20) NOT NULL,
  PRIMARY KEY (`bayi_no`,`kandidat_id`),
  KEY `kandidat_id` (`kandidat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bidan`
--

CREATE TABLE IF NOT EXISTS `tbl_bidan` (
  `nik` varchar(16) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `tmpt_lahir` varchar(40) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` varchar(128) NOT NULL,
  `alamat_praktek` varchar(128) NOT NULL,
  PRIMARY KEY (`nik`),
  UNIQUE KEY `fk_nik` (`nik`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_bidan`
--

INSERT INTO `tbl_bidan` (`nik`, `nama`, `tmpt_lahir`, `tgl_lahir`, `alamat`, `alamat_praktek`) VALUES
('1305100000000001', 'Nurhayati,Amd.Keb, S.SiT', 'Kayu Tanam', '1974-04-01', 'Kayu Tanam\r\n', 'RB & BP " SUMATERA BARAT " Jr.Kp. Ladang Lubuk Alung Kab.Pd. Pariaman\r\n'),
('1305100000000002', 'Rina Rahayu. AM, Amd. Keb', 'Medan', '1987-07-21', 'Petak Talao Mundam/Ketaping\r\n', 'Klinik Alifha Jl.Raya Padang Bukittinggi KM. 21 Kasai Kec. Batang Anai\r\n'),
('1305100000000003', 'Hj Yetti Latif S.ST', 'Padang Pariaman', '1966-07-03', 'Sungai Abang Lubuk Alung\r\n', 'Sungai Abang Lubuk Alung\r\n'),
('1305100000000004', 'Mardianan, Amd. Keb', 'Tanjung Aur', '1959-12-20', 'Tanjung Aur Kec Enam Lingkung\r\n', 'Ringan-Ringan Kec . Enam Lingkung\r\n'),
('1305100000000005', 'Elia Munawwari,Amd.Keb.SKM', 'Jambak', '1976-06-18', 'Jl.Raya Padang-Bukit Tinggi ,Sungai Abang Kec.Lubuk Alung\r\n', 'Jl.Raya Padang - Bukit Tinggi Sungai Abang Kec.Lubuk Alung  Pd Pariaman\r\n'),
('1305100000000006', 'Afrisa Elya Fitri . Amd.Keb', 'Padang Alai', '1991-04-14', 'Jl.WR Supratman No.155 Ampalu  Pariaman\r\n', 'Klinik Aisyiah III Lubuk Alung ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_biodata_wni`
--

CREATE TABLE IF NOT EXISTS `tbl_biodata_wni` (
  `nik` varchar(16) NOT NULL,
  `no_ktp` varchar(40) DEFAULT NULL,
  `tmpt_sbl` varchar(300) DEFAULT NULL,
  `no_paspor` varchar(30) DEFAULT NULL,
  `tgl_akh_paspor` date DEFAULT NULL,
  `nama_lgkp` varchar(60) NOT NULL,
  `jenis_klmin` varchar(1) NOT NULL,
  `tmpt_lhr` varchar(60) NOT NULL,
  `tgl_lhr` date NOT NULL,
  `akta_lhr` varchar(1) DEFAULT NULL,
  `no_akta_lhr` varchar(40) DEFAULT NULL,
  `gol_drh` varchar(3) NOT NULL,
  `agama` varchar(20) NOT NULL,
  `stat_kwn` varchar(1) NOT NULL,
  `akta_kwn` varchar(1) DEFAULT NULL,
  `no_akta_kwn` varchar(40) DEFAULT NULL,
  `tgl_kwn` date DEFAULT NULL,
  `akta_crai` varchar(1) DEFAULT NULL,
  `no_akta_crai` varchar(40) DEFAULT NULL,
  `tgl_crai` date DEFAULT NULL,
  `stat_hbkel` varchar(2) DEFAULT NULL,
  `klain_fsk` varchar(1) DEFAULT NULL,
  `pnydng_cct` varchar(1) DEFAULT NULL,
  `pddk_akh` varchar(2) DEFAULT NULL,
  `jenis_pkrjn` varchar(2) DEFAULT NULL,
  `nik_ibu` varchar(16) DEFAULT NULL,
  `nama_lgkp_ibu` varchar(60) DEFAULT NULL,
  `nik_ayah` varchar(16) DEFAULT NULL,
  `nama_lgkp_ayah` varchar(60) DEFAULT NULL,
  `nama_ket_rt` varchar(60) DEFAULT NULL,
  `nama_ket_rw` varchar(60) DEFAULT NULL,
  `nama_pet_reg` varchar(60) DEFAULT NULL,
  `nip_pet_reg` varchar(18) DEFAULT NULL,
  `nama_pet_entri` varchar(60) DEFAULT NULL,
  `nip_pet_entri` varchar(18) DEFAULT NULL,
  `tgl_entri` date DEFAULT NULL,
  `no_kk` varchar(16) DEFAULT NULL,
  `jenis_bntu` varchar(2) DEFAULT NULL,
  `no_prop` varchar(2) NOT NULL DEFAULT '13',
  `no_kab` varchar(2) NOT NULL,
  `no_kec` varchar(2) NOT NULL,
  `no_kel` varchar(4) NOT NULL,
  `stat_hidup` varchar(10) DEFAULT NULL,
  `tgl_ubah` date DEFAULT NULL,
  `tgl_cetak_ktp` date DEFAULT NULL,
  `tgl_ganti_ktp` date DEFAULT NULL,
  `tgl_pjg_ktp` date DEFAULT NULL,
  `stat_ktp` varchar(1) DEFAULT NULL,
  `als_numpang` varchar(1) DEFAULT NULL,
  `pflag` varchar(1) DEFAULT NULL,
  `cflag` varchar(1) DEFAULT NULL,
  `sync_flag` varchar(1) DEFAULT NULL,
  `ket_agama` varchar(60) DEFAULT NULL,
  `kebangsaan` varchar(60) DEFAULT NULL,
  `gelar` varchar(5) DEFAULT NULL,
  `ket_pkrjn` varchar(60) DEFAULT NULL,
  `glr_agama` varchar(1) DEFAULT '0',
  `glr_akademis` varchar(1) DEFAULT 'N',
  `glr_bangsawan` varchar(1) DEFAULT 'N',
  `is_pros_datang` varchar(1) DEFAULT 'N',
  `desc_pekerjaan` varchar(30) DEFAULT NULL,
  `desc_kepercayaan` varchar(30) DEFAULT NULL,
  `flag_status` varchar(1) DEFAULT '0',
  `count_ktp` varchar(5) DEFAULT NULL,
  `count_biodata` varchar(5) DEFAULT NULL,
  `flagsink` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`nik`),
  KEY `fk_no_kk` (`no_kk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_biodata_wni`
--

INSERT INTO `tbl_biodata_wni` (`nik`, `no_ktp`, `tmpt_sbl`, `no_paspor`, `tgl_akh_paspor`, `nama_lgkp`, `jenis_klmin`, `tmpt_lhr`, `tgl_lhr`, `akta_lhr`, `no_akta_lhr`, `gol_drh`, `agama`, `stat_kwn`, `akta_kwn`, `no_akta_kwn`, `tgl_kwn`, `akta_crai`, `no_akta_crai`, `tgl_crai`, `stat_hbkel`, `klain_fsk`, `pnydng_cct`, `pddk_akh`, `jenis_pkrjn`, `nik_ibu`, `nama_lgkp_ibu`, `nik_ayah`, `nama_lgkp_ayah`, `nama_ket_rt`, `nama_ket_rw`, `nama_pet_reg`, `nip_pet_reg`, `nama_pet_entri`, `nip_pet_entri`, `tgl_entri`, `no_kk`, `jenis_bntu`, `no_prop`, `no_kab`, `no_kec`, `no_kel`, `stat_hidup`, `tgl_ubah`, `tgl_cetak_ktp`, `tgl_ganti_ktp`, `tgl_pjg_ktp`, `stat_ktp`, `als_numpang`, `pflag`, `cflag`, `sync_flag`, `ket_agama`, `kebangsaan`, `gelar`, `ket_pkrjn`, `glr_agama`, `glr_akademis`, `glr_bangsawan`, `is_pros_datang`, `desc_pekerjaan`, `desc_kepercayaan`, `flag_status`, `count_ktp`, `count_biodata`, `flagsink`) VALUES
('1305010607810001', NULL, NULL, NULL, NULL, 'Agussantoso', 'L', 'Toboh', '2014-07-06', NULL, NULL, '1', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305010607910002', NULL, NULL, NULL, NULL, 'Rahmat', 'L', 'piaman', '2014-07-06', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1305103007990002', NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305010707820001', NULL, NULL, NULL, NULL, 'Indra Irawan', 'L', 'Pariaman', '2014-07-07', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1305103007990001', NULL, '13', '5', '10', 'O', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100000000001', NULL, NULL, NULL, NULL, 'Nurhayati', 'P', 'Kayu Tanam', '1974-04-01', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100000000002', NULL, NULL, NULL, NULL, 'Rina Rahayu', 'P', 'Medan', '1987-07-21', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100000000003', NULL, NULL, NULL, NULL, 'Yetti Latif', 'P', 'Padang Pariaman', '1966-07-03', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100000000004', NULL, NULL, NULL, NULL, 'Mardianan', 'P', 'Tanjung Aur', '1959-12-20', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100000000005', NULL, NULL, NULL, NULL, 'Elia Munawwari', 'P', 'Jambak', '1976-06-18', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100000000006', NULL, NULL, NULL, NULL, 'Afrisa Elya Fitri', 'P', 'Padang Alai', '1991-04-14', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100607110001', NULL, NULL, NULL, NULL, 'Mufidatun', 'P', 'Toboh', '2014-07-06', NULL, NULL, '1', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100607130001', NULL, NULL, NULL, NULL, 'Yulaini', 'P', 'piaman', '2014-07-06', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100607140001', NULL, NULL, NULL, NULL, 'Ujang', 'L', 'piaman', '2014-07-06', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100607150001', NULL, NULL, NULL, NULL, 'Udin', 'L', 'Toboh', '2014-07-06', NULL, NULL, '1', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100607640001', NULL, NULL, NULL, NULL, 'Puspita Sari', 'P', 'Toboh', '2014-07-06', NULL, NULL, '1', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100607760001', NULL, NULL, NULL, NULL, 'M. Akbar', 'L', 'piaman', '2014-07-06', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100607790001', NULL, NULL, NULL, NULL, 'Reza Wijaya', 'L', 'Toboh', '2014-07-06', NULL, NULL, '1', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100607840001', NULL, NULL, NULL, NULL, 'Marlina', 'P', 'piaman', '2014-07-06', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100607880001', NULL, NULL, NULL, NULL, 'Ahmad Faisal', 'L', 'piaman', '2014-07-06', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100607940001', NULL, NULL, NULL, NULL, 'Sri Maya Sari', 'P', 'Toboh', '2014-07-06', NULL, NULL, '1', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100707120001', NULL, NULL, NULL, NULL, 'Tarno', 'L', 'Pariaman', '2014-07-07', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'O', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100707740001', NULL, NULL, NULL, NULL, 'Astri Dwi', 'P', 'Pariaman', '2014-07-07', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'O', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100707750001', NULL, NULL, NULL, NULL, 'Dewi Puspa', 'P', 'Pariaman', '2014-07-07', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'O', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100707780001', NULL, NULL, NULL, NULL, 'Fitraini', 'P', 'Pariaman', '2014-07-07', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'O', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL),
('1305100707890001', NULL, NULL, NULL, NULL, 'Yessi Wulandari', 'P', 'Pariaman', '2014-07-07', NULL, NULL, '0', 'Islam', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13', '5', '10', 'O', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'N', 'N', 'N', NULL, NULL, '0', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_capil_lahir`
--

CREATE TABLE IF NOT EXISTS `tbl_capil_lahir` (
  `bayi_no` int(16) NOT NULL AUTO_INCREMENT,
  `bayi_org_asing` varchar(1) DEFAULT NULL,
  `bayi_nik` varchar(16) DEFAULT NULL,
  `bayi_nama_lgkp` varchar(128) DEFAULT NULL,
  `bayi_tmpt_lahir` varchar(128) DEFAULT NULL,
  `bayi_tgl_lahir` varchar(7) DEFAULT NULL,
  `bayi_wkt_lahir` varchar(6) DEFAULT NULL,
  `bayi_jns_kelamin` varchar(22) DEFAULT NULL,
  `bayi_jns_klhr` varchar(22) DEFAULT NULL,
  `bayi_tmpt_klhr` varchar(22) DEFAULT NULL,
  `bayi_anak_ke` varchar(22) DEFAULT NULL,
  `bayi_berat` varchar(22) DEFAULT NULL,
  `bayi_panjang` varchar(22) DEFAULT NULL,
  `bayi_pnlg_klhr` varchar(22) DEFAULT NULL,
  `bayi_dom_lahir` varchar(1) DEFAULT NULL,
  `bayi_ln_lahir` varchar(1) DEFAULT NULL,
  `no_kk` varchar(16) DEFAULT NULL,
  `nama_kk` varchar(65) DEFAULT NULL,
  `ibu_nik` varchar(16) DEFAULT NULL,
  `ibu_nama_lgkp` varchar(128) DEFAULT NULL,
  `ibu_tgl_lahir` varchar(7) DEFAULT NULL,
  `ibu_tgl_kawin` varchar(7) DEFAULT NULL,
  `ibu_wrg_ngr` varchar(22) DEFAULT NULL,
  `ibu_pekerjaan` varchar(22) DEFAULT NULL,
  `ibu_kebangsaan` varchar(64) DEFAULT NULL,
  `ibu_alamat` varchar(128) DEFAULT NULL,
  `ibu_no_rt` varchar(2) DEFAULT NULL,
  `ibu_no_rw` varchar(2) DEFAULT NULL,
  `ibu_no_prov` varchar(22) DEFAULT NULL,
  `ibu_no_kab` varchar(22) DEFAULT NULL,
  `ibu_no_kec` varchar(22) DEFAULT NULL,
  `ibu_no_kel` varchar(22) DEFAULT NULL,
  `ayah_nik` varchar(16) DEFAULT NULL,
  `ayah_nama_lgkp` varchar(128) DEFAULT NULL,
  `ayah_tgl_lahir` varchar(7) DEFAULT NULL,
  `ayah_wrg_ngr` varchar(22) DEFAULT NULL,
  `ayah_pekerjaan` varchar(22) DEFAULT NULL,
  `ayah_kebangsaan` varchar(64) DEFAULT NULL,
  `ayah_alamat` varchar(128) DEFAULT NULL,
  `ayah_no_rt` varchar(2) DEFAULT NULL,
  `ayah_no_rw` varchar(2) DEFAULT NULL,
  `ayah_no_prov` varchar(22) DEFAULT NULL,
  `ayah_no_kab` varchar(22) DEFAULT NULL,
  `ayah_no_kec` varchar(22) DEFAULT NULL,
  `ayah_no_kel` varchar(22) DEFAULT NULL,
  `plpr_nik` varchar(16) DEFAULT NULL,
  `plpr_tgl_lapor` varchar(7) DEFAULT NULL,
  `plpr_umur` varchar(22) DEFAULT NULL,
  `plpr_nama_lgkp` varchar(128) DEFAULT NULL,
  `plpr_kelamin` varchar(22) DEFAULT NULL,
  `plpr_pekerjaan` varchar(22) DEFAULT NULL,
  `plpr_alamat` varchar(128) DEFAULT NULL,
  `plpr_no_rt` varchar(2) DEFAULT NULL,
  `plpr_no_rw` varchar(2) DEFAULT NULL,
  `plpr_no_prov` varchar(22) DEFAULT NULL,
  `plpr_no_kab` varchar(22) DEFAULT NULL,
  `plpr_no_kec` varchar(22) DEFAULT NULL,
  `plpr_no_kel` varchar(22) DEFAULT NULL,
  `plpr_hub_kel` varchar(64) DEFAULT NULL,
  `saksi1_nik` varchar(16) DEFAULT NULL,
  `saksi1_nama_lgkp` varchar(128) DEFAULT NULL,
  `saksi1_umur` varchar(22) DEFAULT NULL,
  `saksi1_kelamin` varchar(22) DEFAULT NULL,
  `saksi1_pekerjaan` varchar(22) DEFAULT NULL,
  `saksi1_alamat` varchar(128) DEFAULT NULL,
  `saksi1_no_rt` varchar(2) DEFAULT NULL,
  `saksi1_no_rw` varchar(2) DEFAULT NULL,
  `saksi1_no_prov` varchar(22) DEFAULT NULL,
  `saksi1_no_kab` varchar(22) DEFAULT NULL,
  `saksi1_no_kec` varchar(22) DEFAULT NULL,
  `saksi1_no_kel` varchar(22) DEFAULT NULL,
  `saksi2_nik` varchar(16) DEFAULT NULL,
  `saksi2_nama_lgkp` varchar(128) DEFAULT NULL,
  `saksi2_umur` varchar(22) DEFAULT NULL,
  `saksi2_kelamin` varchar(22) DEFAULT NULL,
  `saksi2_pekerjaan` varchar(22) DEFAULT NULL,
  `saksi2_alamat` varchar(128) DEFAULT NULL,
  `saksi2_no_rt` varchar(2) DEFAULT NULL,
  `saksi2_no_rw` varchar(2) DEFAULT NULL,
  `saksi2_kode_pos` varchar(22) DEFAULT NULL,
  `saksi2_no_prov` varchar(22) DEFAULT NULL,
  `saksi2_no_kab` varchar(22) DEFAULT NULL,
  `saksi2_no_kec` varchar(22) DEFAULT NULL,
  `saksi2_no_kel` varchar(22) DEFAULT NULL,
  `adm_no_kedubes` varchar(22) DEFAULT NULL,
  `adm_no_konjen` varchar(22) DEFAULT NULL,
  `adm_no_konsul` varchar(22) DEFAULT NULL,
  `adm_no_prov` varchar(22) DEFAULT NULL,
  `adm_no_kab` varchar(22) DEFAULT NULL,
  `adm_no_kec` varchar(22) DEFAULT NULL,
  `adm_no_kel` varchar(22) DEFAULT NULL,
  `adm_akta_no` varchar(64) DEFAULT NULL,
  `adm_dokumen` varchar(255) DEFAULT NULL,
  `adm_sp_no` varchar(64) DEFAULT NULL,
  `adm_sp_tgl` varchar(7) DEFAULT NULL,
  `adm_sp_nama` varchar(255) DEFAULT NULL,
  `adm_akta_tgl` varchar(7) DEFAULT NULL,
  `adm_akta_ktp_tgl` varchar(7) DEFAULT NULL,
  `adm_kades_nama` varchar(128) DEFAULT NULL,
  `adm_kades_nip` varchar(16) DEFAULT NULL,
  `adm_regs_nama` varchar(128) DEFAULT NULL,
  `adm_regs_nip` varchar(16) DEFAULT NULL,
  `adm_tgl_entry` varchar(7) DEFAULT NULL,
  `adm_tgl_update` varchar(7) DEFAULT NULL,
  `user_id` varchar(24) DEFAULT NULL,
  `flag_update` varchar(22) DEFAULT NULL,
  `flag_ctk_akta` varchar(22) DEFAULT NULL,
  `flag_ctk_kakta` varchar(22) DEFAULT NULL,
  `flag_ctk_sk` varchar(22) DEFAULT NULL,
  `flag_status` varchar(22) DEFAULT '0',
  PRIMARY KEY (`bayi_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_checklist_persyaratan_pengajuan`
--

CREATE TABLE IF NOT EXISTS `tbl_checklist_persyaratan_pengajuan` (
  `pengajuan_id` int(16) NOT NULL,
  `pa_id` int(16) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `isQualified` tinyint(1) NOT NULL,
  PRIMARY KEY (`pengajuan_id`,`pa_id`),
  KEY `pa_id` (`pa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_checklist_persyaratan_pengajuan`
--

INSERT INTO `tbl_checklist_persyaratan_pengajuan` (`pengajuan_id`, `pa_id`, `kuantitas`, `isQualified`) VALUES
(21, 1, 2, 0),
(22, 1, 2, 0),
(23, 1, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_data_keluarga`
--

CREATE TABLE IF NOT EXISTS `tbl_data_keluarga` (
  `no_kk` varchar(16) NOT NULL,
  `nama_kep` varchar(60) NOT NULL,
  `alamat` varchar(120) NOT NULL,
  `no_rt` varchar(3) NOT NULL,
  `no_rw` varchar(3) NOT NULL,
  `dusun` varchar(60) NOT NULL,
  `kode_pos` varchar(5) NOT NULL,
  `telp` varchar(30) NOT NULL,
  `als_prmohon` varchar(1) NOT NULL,
  `als_numpang` varchar(1) NOT NULL,
  `no_prop` varchar(2) DEFAULT NULL,
  `no_kab` varchar(2) DEFAULT NULL,
  `no_kec` varchar(2) DEFAULT NULL,
  `no_kel` varchar(4) DEFAULT NULL,
  `userid` varchar(11) NOT NULL,
  `tgl_insertion` date NOT NULL,
  `tgl_updation` date NOT NULL,
  `pflag` char(1) NOT NULL,
  `cflag` char(1) NOT NULL,
  `sync_flag` varchar(1) NOT NULL,
  `oa_nama_pertama` varchar(60) NOT NULL,
  `oa_nama_keluarga` varchar(60) NOT NULL,
  `tipe_kk` varchar(1) NOT NULL,
  `nik_kk` varchar(16) NOT NULL,
  `count_kk` varchar(5) NOT NULL,
  `flagsink` varchar(50) NOT NULL,
  `tgl_siak_plus` date NOT NULL,
  PRIMARY KEY (`no_kk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_data_keluarga`
--

INSERT INTO `tbl_data_keluarga` (`no_kk`, `nama_kep`, `alamat`, `no_rt`, `no_rw`, `dusun`, `kode_pos`, `telp`, `als_prmohon`, `als_numpang`, `no_prop`, `no_kab`, `no_kec`, `no_kel`, `userid`, `tgl_insertion`, `tgl_updation`, `pflag`, `cflag`, `sync_flag`, `oa_nama_pertama`, `oa_nama_keluarga`, `tipe_kk`, `nik_kk`, `count_kk`, `flagsink`, `tgl_siak_plus`) VALUES
('1305103007990001', 'Indra Irawan', 'Padang Salai', '1', '1', 'Padang Salai', '12345', '081312345678', '1', '1', '13', '05', NULL, NULL, '1234567', '2014-01-01', '2014-01-02', '1', '1', '1', 'antah lah', 'asdfghjk', 'a', '2147483647', '3', 'qwerty', '2014-06-22'),
('1305103007990002', 'Rahmat', 'Kurai Taji', '11', '2', 'Kurai Taji', '12345', '0812345678998', '1', '1', '13', '05', NULL, NULL, '0', '2014-06-01', '2014-06-03', '1', '1', '1', 'poiuyt', 'lkjhgf', 'a', '9876543', '2', 'mnbvcxz', '2014-06-15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_kelahiran`
--

CREATE TABLE IF NOT EXISTS `tbl_detail_kelahiran` (
  `kandidat_id` int(16) NOT NULL,
  `nama` varchar(40) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `waktu_lahir` varchar(8) DEFAULT NULL,
  `jenis_kelamin` varchar(1) DEFAULT NULL,
  `jenis_kelahiran` varchar(12) DEFAULT NULL,
  `tempat_lahir` varchar(40) DEFAULT NULL,
  `anak_ke` tinyint(4) DEFAULT NULL,
  `berat` smallint(6) DEFAULT NULL,
  `panjang` smallint(6) DEFAULT NULL,
  `penolong_kelahiran` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`kandidat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_pelapor`
--

CREATE TABLE IF NOT EXISTS `tbl_detail_pelapor` (
  `kandidat_id` int(16) NOT NULL,
  `tanggal_lapor` date NOT NULL,
  `hubungan_keluarga` varchar(40) NOT NULL,
  `nik` varchar(16) NOT NULL,
  PRIMARY KEY (`kandidat_id`),
  KEY `fk_nik` (`nik`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_detail_pelapor`
--

INSERT INTO `tbl_detail_pelapor` (`kandidat_id`, `tanggal_lapor`, `hubungan_keluarga`, `nik`) VALUES
(1, '2014-07-06', 'Ayah', '1305010707820001'),
(2, '2014-07-06', 'Ayah', '1305010607810001'),
(3, '2014-07-06', 'Ayah', '1305010607910002'),
(4, '2014-07-06', 'Ayah', '1305100607140001'),
(5, '2014-07-06', 'Ayah', '1305100607150001'),
(6, '2014-07-06', 'Ayah', '1305100607760001'),
(7, '2014-07-06', 'Ayah', '1305100607790001'),
(8, '2014-07-06', 'Ayah', '1305100607880001'),
(9, '2014-07-06', 'Ayah', '1305100707120001'),
(10, '2014-07-06', 'Ayah', '1305010707820001'),
(11, '2014-07-06', 'Ayah', '1305010607810001'),
(12, '2014-07-06', 'Ayah', '1305010607910002'),
(13, '2014-07-06', 'Ayah', '1305100607140001'),
(14, '2014-07-06', 'Ayah', '1305100607150001'),
(15, '2014-07-06', 'Ayah', '1305100607760001'),
(16, '2014-07-23', 'ibu', '1305100607130001'),
(17, '2014-07-23', 'ayah', '1305100607150001'),
(18, '2014-07-23', 'ayah', '1305100707120001'),
(19, '2014-07-23', 'ayah', '1305100607880001');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kandidat_bayi`
--

CREATE TABLE IF NOT EXISTS `tbl_kandidat_bayi` (
  `kandidat_id` int(16) NOT NULL AUTO_INCREMENT,
  `pengajuan_id` int(16) NOT NULL,
  `no_kk` varchar(16) NOT NULL,
  `nik_bayi` varchar(16) DEFAULT NULL,
  `nik_ibu` varchar(16) NOT NULL,
  `nik_ayah` varchar(16) NOT NULL,
  `nik_pelapor` varchar(16) NOT NULL,
  `nik_saksi1` varchar(16) NOT NULL,
  `nik_saksi2` varchar(16) NOT NULL,
  `keterangan` text,
  `approve` int(11) NOT NULL DEFAULT '0',
  `sk_id` int(16) NOT NULL,
  `insert_datetime` date NOT NULL,
  `insert_by` varchar(20) NOT NULL,
  `update_datetime` date DEFAULT NULL,
  `update_by` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`kandidat_id`),
  KEY `fk_pengajuan_id` (`pengajuan_id`),
  KEY `fk_no_kk` (`no_kk`),
  KEY `fk_sk_id` (`sk_id`),
  KEY `fk_nik_bayi` (`nik_bayi`),
  KEY `fk_nik_ibu` (`nik_ibu`),
  KEY `fk_nik_saksi1` (`nik_saksi1`),
  KEY `fk_nik_pelapor` (`nik_pelapor`),
  KEY `fk_nik_ayah` (`nik_ayah`),
  KEY `fk_nik_saksi2` (`nik_saksi2`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `tbl_kandidat_bayi`
--

INSERT INTO `tbl_kandidat_bayi` (`kandidat_id`, `pengajuan_id`, `no_kk`, `nik_bayi`, `nik_ibu`, `nik_ayah`, `nik_pelapor`, `nik_saksi1`, `nik_saksi2`, `keterangan`, `approve`, `sk_id`, `insert_datetime`, `insert_by`, `update_datetime`, `update_by`) VALUES
(1, 1, '1305103007990001', NULL, '1305100607110001', '1305010707820001', '1305010707820001', '1305100707890001', '1305100707780001', NULL, 0, 1, '2014-07-06', 'Yogia Zulfi', NULL, NULL),
(2, 2, '1305103007990002', NULL, '1305100607130001', '1305010607810001', '1305010607810001', '1305100707890001', '1305100707780001', NULL, 0, 1, '2014-07-06', 'Yogia Zulfi', NULL, NULL),
(3, 3, '1305103007990001', NULL, '1305100607640001', '1305010607910002', '1305010607910002', '1305100707890001', '1305100707780001', NULL, 0, 1, '2014-07-06', 'Yogia Zulfi', NULL, NULL),
(4, 4, '1305103007990002', NULL, '1305100607840001', '1305100607140001', '1305100607140001', '1305100707890001', '1305100707780001', NULL, 0, 1, '2014-07-06', 'Yogia Zulfi', NULL, NULL),
(5, 5, '1305103007990001', NULL, '1305100607940001', '1305100607150001', '1305100607150001', '1305100707890001', '1305100707780001', NULL, 0, 1, '2014-07-06', 'Yogia Zulfi', NULL, NULL),
(6, 6, '1305103007990002', NULL, '1305100707740001', '1305100607760001', '1305100607760001', '1305100707890001', '1305100707780001', NULL, 0, 2, '2014-07-06', 'Yogia Zulfi', NULL, NULL),
(7, 7, '1305103007990001', NULL, '1305100707750001', '1305100607790001', '1305100607790001', '1305100707890001', '1305100707780001', NULL, 0, 2, '2014-07-06', 'Yogia Zulfi', NULL, NULL),
(8, 8, '1305103007990002', NULL, '1305100707780001', '1305100607880001', '1305100607880001', '1305100707890001', '1305100707780001', NULL, 0, 2, '2014-07-06', 'Yogia Zulfi', NULL, NULL),
(9, 9, '1305103007990001', NULL, '1305100707890001', '1305100707120001', '1305100707120001', '1305100707890001', '1305100707780001', NULL, 0, 2, '2014-07-06', 'Yogia Zulfi', NULL, NULL),
(10, 10, '1305103007990002', NULL, '1305100607110001', '1305010707820001', '1305010707820001', '1305100707890001', '1305100707780001', NULL, 0, 1, '2014-07-06', 'Yogia Zulfi', NULL, NULL),
(11, 11, '1305103007990001', NULL, '1305100607130001', '1305010607810001', '1305010607810001', '1305100707890001', '1305100707780001', NULL, 0, 1, '2014-07-06', 'Yogia Zulfi', NULL, NULL),
(12, 12, '1305103007990002', NULL, '1305100607840001', '1305010607910002', '1305010607910002', '1305100707890001', '1305100707780001', NULL, 0, 1, '2014-07-06', 'Yogia Zulfi', NULL, NULL),
(13, 13, '1305103007990002', NULL, '1305100607640001', '1305100607140001', '1305100607140001', '1305100707890001', '1305100707780001', NULL, 0, 2, '2014-07-06', 'Yogia Zulfi', NULL, NULL),
(14, 14, '1305103007990001', NULL, '1305100607940001', '1305100607150001', '1305100607150001', '1305100707890001', '1305100707780001', NULL, 0, 1, '2014-07-06', 'Yogia Zulfi', NULL, NULL),
(15, 15, '1305103007990002', NULL, '1305100707740001', '1305100607760001', '1305100607760001', '1305100707890001', '1305100707780001', NULL, 0, 2, '2014-07-06', 'Yogia Zulfi', NULL, NULL),
(16, 21, '1305103007990002', NULL, '1305100607130001', '1305100607150001', '1305100607130001', '1305100607140001', '1305100607840001', 'nama pada KTP ibu salah', 1, 1, '2014-07-23', 'Bidan1', NULL, NULL),
(17, 22, '1305103007990001', NULL, '1305100607110001', '1305100607150001', '1305100607150001', '1305100607760001', '1305100607790001', 'nama ibu tidak sesuai\r\nnama ayah pada KK salah', 0, 1, '2014-07-23', 'Bidan1', NULL, NULL),
(18, 23, '1305103007990002', NULL, '1305100707890001', '1305100707120001', '1305100707120001', '1305100607760001', '1305100607640001', 'ada masalah', 1, 1, '2014-07-23', 'Bidan1', NULL, NULL),
(19, 16, '1305103007990001', NULL, '1305100707890001', '1305100607880001', '1305100607880001', '1305010707820001', '1305100607110001', NULL, 0, 1, '2014-07-23', 'Bidan1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_klinik`
--

CREATE TABLE IF NOT EXISTS `tbl_klinik` (
  `id_klinik` int(11) NOT NULL AUTO_INCREMENT,
  `nama_klinik` varchar(80) NOT NULL,
  `alamat` varchar(128) NOT NULL,
  `no_ho` varchar(40) DEFAULT NULL,
  `tgl_ho` date DEFAULT NULL,
  `penanggung_jawab` varchar(50) DEFAULT NULL,
  `pimpinan` varchar(60) DEFAULT NULL,
  `pekerjaan` varchar(80) DEFAULT NULL,
  `no_izin` varchar(50) DEFAULT NULL,
  `tgl_izin_berlaku` date DEFAULT NULL,
  `tgl_izin_keluar` date DEFAULT NULL,
  `jns_klinik` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_klinik`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_klinik`
--

INSERT INTO `tbl_klinik` (`id_klinik`, `nama_klinik`, `alamat`, `no_ho`, `tgl_ho`, `penanggung_jawab`, `pimpinan`, `pekerjaan`, `no_izin`, `tgl_izin_berlaku`, `tgl_izin_keluar`, `jns_klinik`) VALUES
(1, 'KLINIK F&D MEDICAL SERVICE', 'Korong Padang Karambil Nagari Kuranji Hilir Kec. Sungai Limau\r\n', '134/KEP/HO/BPMPTT-2014', '2014-04-23', 'Dr.Hj.Fatmawati Sridewi', 'Dr.Hj.Fatmawati Sridewi', 'Pegawai Negeri Sipil', 'Yankes/Regdit/01/VII/2014', '2019-07-03', '2014-07-03', NULL),
(2, 'KLINIK SETIA', 'Jl.Raya Padang - Bulkit Tinggi KM.48 Simpang Pariaman Sicincin', '180/KEP/HO/BPMPPT-2014', '2014-05-31', 'dr. Risya Diana Dewi', 'Hj. Nuraini.Amd.Keb.', 'Pegawai Negeri Sipil', 'Yankes/Regdit/02/VII/2014', '2019-07-15', '2014-07-15', 'Klinik Pratama'),
(3, 'KLINIK AISYIAH III', 'Jl.Raya Padang - Bulkit Tinggi Simpang  Lintas Lubuk Alung ', '193/KEP/HO/BPMPPT-2014', '2014-06-18', 'dr.Akhnal Syaputra.S.Ked', 'Dra.Hj.Armainar', 'Pimpinan Daerah Aisyiah Kab.Padang Pariaman', 'Yankes/Regdit/03/VIII/2014', '2019-08-11', '2014-08-11', 'Klinik Pratama'),
(4, 'RINGAN-RINGAN MEDICAL CENTER', 'Korong Ringan-Ringan Nagari Pakandangan Kec.Enam Lingkung', '300/142/BPMPPT-2012', '2012-11-21', 'dr Ferdinal Feri,Sp.OG (K)', 'Rifki Monrizal NP.SH,M.Si', 'Pegawai Negeri Sipil', '441/06/Yankes/V/2013', '2018-05-13', '2013-05-13', NULL),
(5, 'RB & BP " SUMATERA BARAT " Jr.Kp. Ladang Lubuk Alung Kab.Pd. Pariaman\n', 'Jr.Kp. Ladang Lubuk Alung Kab.Pd. Pariaman', NULL, NULL, 'Nurhayati,Amd.Keb, S.SiT', 'Nurhayati,Amd.Keb, S.SiT', 'Pegawai Negeri Sipil', NULL, NULL, NULL, NULL),
(6, 'Klinik Alifha', 'Jl.Raya Padang Bukittinggi KM. 21 Kasai Kec. Batang Anai', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengajuan`
--

CREATE TABLE IF NOT EXISTS `tbl_pengajuan` (
  `pengajuan_id` int(16) NOT NULL AUTO_INCREMENT,
  `ad_id` int(16) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `umur_kehamilan` int(16) NOT NULL,
  `status_id` int(16) NOT NULL,
  `tanggal_pengajuan` date NOT NULL,
  `insert_datetime` date NOT NULL,
  `insert_by` varchar(20) NOT NULL,
  `update_datetime` date DEFAULT NULL,
  `update_by` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`pengajuan_id`),
  KEY `fk_ad_id` (`ad_id`),
  KEY `fk_nik` (`nik`),
  KEY `fk_status_id` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `tbl_pengajuan`
--

INSERT INTO `tbl_pengajuan` (`pengajuan_id`, `ad_id`, `nik`, `umur_kehamilan`, `status_id`, `tanggal_pengajuan`, `insert_datetime`, `insert_by`, `update_datetime`, `update_by`) VALUES
(1, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(2, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(3, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(4, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(5, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(6, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(7, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(8, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(9, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(10, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(11, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(12, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(13, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(14, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(15, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(16, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(17, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(18, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(19, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(20, 1, '1305010607810001', 8, 1, '2014-07-01', '2014-07-01', 'Yogia Zulfi', NULL, NULL),
(21, 1, '1305010607810001', 8, 2, '2014-07-23', '2014-07-23', 'Bidan1', NULL, NULL),
(22, 1, '1305010607810001', 6, 2, '2014-07-23', '2014-07-23', 'Bidan1', NULL, NULL),
(23, 1, '1305100607150001', 7, 2, '2014-07-23', '2014-07-23', 'Bidan1', NULL, NULL),
(25, 1, '1305100707750001', 90, 1, '2014-10-31', '2014-10-31', 'masyarakat', NULL, NULL),
(26, 1, '1305100707750001', 90, 1, '2014-10-31', '2014-10-31', 'masyarakat', NULL, NULL),
(27, 1, '1305100000000006', 90, 2, '2014-11-24', '2014-11-24', 'masyarakat', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_persyaratan`
--

CREATE TABLE IF NOT EXISTS `tbl_persyaratan` (
  `persyaratan_id` int(16) NOT NULL AUTO_INCREMENT,
  `judul` varchar(40) NOT NULL,
  `keterangan` varchar(40) NOT NULL,
  PRIMARY KEY (`persyaratan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_persyaratan`
--

INSERT INTO `tbl_persyaratan` (`persyaratan_id`, `judul`, `keterangan`) VALUES
(1, 'KTP', ''),
(2, 'Kartu Keluarga', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_persyaratan_adduk`
--

CREATE TABLE IF NOT EXISTS `tbl_persyaratan_adduk` (
  `pa_id` int(16) NOT NULL AUTO_INCREMENT,
  `ad_id` int(16) NOT NULL,
  `persyaratan_id` int(16) NOT NULL,
  `kuatitas` int(11) NOT NULL,
  `isWajib` tinyint(1) NOT NULL,
  PRIMARY KEY (`pa_id`),
  KEY `fk_ad_id` (`ad_id`),
  KEY `fk_persyaratan_id` (`persyaratan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_persyaratan_adduk`
--

INSERT INTO `tbl_persyaratan_adduk` (`pa_id`, `ad_id`, `persyaratan_id`, `kuatitas`, `isWajib`) VALUES
(1, 1, 1, 2, 1),
(2, 1, 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_persyaratan_belum_lengkap`
--

CREATE TABLE IF NOT EXISTS `tbl_persyaratan_belum_lengkap` (
  `pengajuan_id` int(16) NOT NULL,
  `jenis` varchar(40) NOT NULL,
  `nama_lengkap` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status_kandidat`
--

CREATE TABLE IF NOT EXISTS `tbl_status_kandidat` (
  `sk_id` int(16) NOT NULL AUTO_INCREMENT,
  `judul` varchar(40) NOT NULL,
  PRIMARY KEY (`sk_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_status_kandidat`
--

INSERT INTO `tbl_status_kandidat` (`sk_id`, `judul`) VALUES
(1, 'Baru Entry'),
(2, 'Mendekati Kelahiran'),
(3, 'Proses Pengajuan'),
(4, 'Sudah Dicetak'),
(5, 'Sudah Dikirim');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status_kandidat_history`
--

CREATE TABLE IF NOT EXISTS `tbl_status_kandidat_history` (
  `kandidat_id` int(16) NOT NULL,
  `sk_id` int(16) NOT NULL,
  `keterangan` varchar(40) NOT NULL,
  `insert_datetime` date NOT NULL,
  `insert_by` varchar(20) NOT NULL,
  PRIMARY KEY (`kandidat_id`,`sk_id`),
  KEY `sk_id` (`sk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_status_kandidat_history`
--

INSERT INTO `tbl_status_kandidat_history` (`kandidat_id`, `sk_id`, `keterangan`, `insert_datetime`, `insert_by`) VALUES
(1, 1, '', '2014-07-06', 'Yogia Zulfi'),
(2, 1, '', '2014-07-06', 'Yogia Zulfi'),
(3, 1, '', '2014-07-06', 'Yogia Zulfi'),
(4, 1, '', '2014-07-06', 'Yogia Zulfi'),
(5, 1, '', '2014-07-06', 'Yogia Zulfi'),
(6, 2, '', '2014-07-06', 'Yogia Zulfi'),
(7, 2, '', '2014-07-06', 'Yogia Zulfi'),
(8, 2, '', '2014-07-06', 'Yogia Zulfi'),
(9, 2, '', '2014-07-06', 'Yogia Zulfi'),
(10, 1, '', '2014-07-06', 'Yogia Zulfi'),
(11, 1, '', '2014-07-06', 'Yogia Zulfi'),
(12, 1, '', '2014-07-06', 'Yogia Zulfi'),
(13, 2, '', '2014-07-06', 'Yogia Zulfi'),
(14, 1, '', '2014-07-06', 'Yogia Zulfi'),
(15, 2, '', '2014-07-06', 'Yogia Zulfi'),
(16, 1, '', '2014-07-23', 'Bidan1'),
(17, 1, '', '2014-07-23', 'Bidan1'),
(18, 1, '', '2014-07-23', 'Bidan1'),
(19, 1, '', '2014-07-23', 'Bidan1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status_pengajuan`
--

CREATE TABLE IF NOT EXISTS `tbl_status_pengajuan` (
  `status_id` int(16) NOT NULL,
  `keterangan` varchar(40) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_status_pengajuan`
--

INSERT INTO `tbl_status_pengajuan` (`status_id`, `keterangan`) VALUES
(1, 'Belum Lengkap'),
(2, 'Sudah Lengkap');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status_pengajuan_history`
--

CREATE TABLE IF NOT EXISTS `tbl_status_pengajuan_history` (
  `pengajuan_id` int(16) NOT NULL,
  `status_id` int(16) NOT NULL,
  `insert_datetime` date NOT NULL,
  `insert_by` varchar(20) NOT NULL,
  PRIMARY KEY (`pengajuan_id`,`status_id`),
  KEY `status_id` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_status_pengajuan_history`
--

INSERT INTO `tbl_status_pengajuan_history` (`pengajuan_id`, `status_id`, `insert_datetime`, `insert_by`) VALUES
(1, 2, '2014-07-01', 'Yogia Zulfi'),
(2, 2, '2014-07-01', 'Yogia Zulfi'),
(3, 2, '2014-07-01', 'Yogia Zulfi'),
(4, 2, '2014-07-01', 'Yogia Zulfi'),
(5, 2, '2014-07-01', 'Yogia Zulfi'),
(6, 2, '2014-07-01', 'Yogia Zulfi'),
(7, 2, '2014-07-01', 'Yogia Zulfi'),
(8, 2, '2014-07-01', 'Yogia Zulfi'),
(9, 2, '2014-07-01', 'Yogia Zulfi'),
(10, 2, '2014-07-01', 'Yogia Zulfi'),
(11, 2, '2014-07-01', 'Yogia Zulfi'),
(12, 2, '2014-07-01', 'Yogia Zulfi'),
(13, 2, '2014-07-01', 'Yogia Zulfi'),
(14, 2, '2014-07-01', 'Yogia Zulfi'),
(15, 2, '2014-07-01', 'Yogia Zulfi'),
(16, 2, '2014-07-01', 'Yogia Zulfi'),
(17, 2, '2014-07-01', 'Yogia Zulfi'),
(18, 2, '2014-07-01', 'Yogia Zulfi'),
(19, 2, '2014-07-01', 'Yogia Zulfi'),
(20, 2, '2014-07-01', 'Yogia Zulfi'),
(21, 2, '2014-07-23', 'Bidan1'),
(22, 2, '2014-07-23', 'Bidan1'),
(23, 2, '2014-07-23', 'Bidan1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(128) NOT NULL,
  `role` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `password`, `role`) VALUES
(1, 'Yogia Zulfi', '$2a$13$lUXX.fd7HWkjZPM47a5DBukm1IcVRcEJb8.xwYlO9IS/d2QcuqWRi', 'pemda'),
(2, 'Bidan1', '$2a$13$z3mvb5LdeJmeo29TxGE3jutjjXsQ/zMrXMw/eff6QYbvUo4kdraWi', 'bidan'),
(3, 'admin', '$2a$13$LbWUgdtO5QL2zYH5MLtlCuSBTKhHfBVQyBo6YxlCLspJJNFY9T1bi', 'admin'),
(4, 'pemda1', '$2a$13$gfZc00witIdEudNZCdxu4OVvZTlDMNtWgKiaJcCZAeziHGzl90hoG', 'pemda'),
(5, 'Nurhayati', '$2a$13$1sdBUC4ONLbcChuf92b/F.WjGrxGX556vifpiqkAQX2jVw.Ut68l2', 'bidan');

-- --------------------------------------------------------

--
-- Structure for view `biodata_wni_view`
--
DROP TABLE IF EXISTS `biodata_wni_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `biodata_wni_view` AS select `tbl_biodata_wni`.`nik` AS `nik`,`tbl_biodata_wni`.`no_ktp` AS `no_ktp`,`tbl_biodata_wni`.`tmpt_sbl` AS `tmpt_sbl`,`tbl_biodata_wni`.`no_paspor` AS `no_paspor`,`tbl_biodata_wni`.`tgl_akh_paspor` AS `tgl_akh_paspor`,`tbl_biodata_wni`.`nama_lgkp` AS `nama_lgkp`,`tbl_biodata_wni`.`jenis_klmin` AS `jenis_klmin`,`tbl_biodata_wni`.`tmpt_lhr` AS `tmpt_lhr`,`tbl_biodata_wni`.`tgl_lhr` AS `tgl_lhr`,`tbl_biodata_wni`.`akta_lhr` AS `akta_lhr`,`tbl_biodata_wni`.`no_akta_lhr` AS `no_akta_lhr`,`tbl_biodata_wni`.`gol_drh` AS `gol_drh`,`tbl_biodata_wni`.`agama` AS `agama`,`tbl_biodata_wni`.`stat_kwn` AS `stat_kwn`,`tbl_biodata_wni`.`akta_kwn` AS `akta_kwn`,`tbl_biodata_wni`.`no_akta_kwn` AS `no_akta_kwn`,`tbl_biodata_wni`.`tgl_kwn` AS `tgl_kwn`,`tbl_biodata_wni`.`akta_crai` AS `akta_crai`,`tbl_biodata_wni`.`no_akta_crai` AS `no_akta_crai`,`tbl_biodata_wni`.`tgl_crai` AS `tgl_crai`,`tbl_biodata_wni`.`stat_hbkel` AS `stat_hbkel`,`tbl_biodata_wni`.`klain_fsk` AS `klain_fsk`,`tbl_biodata_wni`.`pnydng_cct` AS `pnydng_cct`,`tbl_biodata_wni`.`pddk_akh` AS `pddk_akh`,`tbl_biodata_wni`.`jenis_pkrjn` AS `jenis_pkrjn`,`tbl_biodata_wni`.`nik_ibu` AS `nik_ibu`,`tbl_biodata_wni`.`nama_lgkp_ibu` AS `nama_lgkp_ibu`,`tbl_biodata_wni`.`nik_ayah` AS `nik_ayah`,`tbl_biodata_wni`.`nama_lgkp_ayah` AS `nama_lgkp_ayah`,`tbl_biodata_wni`.`nama_ket_rt` AS `nama_ket_rt`,`tbl_biodata_wni`.`nama_ket_rw` AS `nama_ket_rw`,`tbl_biodata_wni`.`nama_pet_reg` AS `nama_pet_reg`,`tbl_biodata_wni`.`nip_pet_reg` AS `nip_pet_reg`,`tbl_biodata_wni`.`nama_pet_entri` AS `nama_pet_entri`,`tbl_biodata_wni`.`nip_pet_entri` AS `nip_pet_entri`,`tbl_biodata_wni`.`tgl_entri` AS `tgl_entri`,`tbl_biodata_wni`.`no_kk` AS `no_kk`,`tbl_biodata_wni`.`jenis_bntu` AS `jenis_bntu`,`tbl_biodata_wni`.`no_prop` AS `no_prop`,`tbl_biodata_wni`.`no_kab` AS `no_kab`,`tbl_biodata_wni`.`no_kec` AS `no_kec`,`tbl_biodata_wni`.`no_kel` AS `no_kel`,`tbl_biodata_wni`.`stat_hidup` AS `stat_hidup`,`tbl_biodata_wni`.`tgl_ubah` AS `tgl_ubah`,`tbl_biodata_wni`.`tgl_cetak_ktp` AS `tgl_cetak_ktp`,`tbl_biodata_wni`.`tgl_ganti_ktp` AS `tgl_ganti_ktp`,`tbl_biodata_wni`.`tgl_pjg_ktp` AS `tgl_pjg_ktp`,`tbl_biodata_wni`.`stat_ktp` AS `stat_ktp`,`tbl_biodata_wni`.`als_numpang` AS `als_numpang`,`tbl_biodata_wni`.`pflag` AS `pflag`,`tbl_biodata_wni`.`cflag` AS `cflag`,`tbl_biodata_wni`.`sync_flag` AS `sync_flag`,`tbl_biodata_wni`.`ket_agama` AS `ket_agama`,`tbl_biodata_wni`.`kebangsaan` AS `kebangsaan`,`tbl_biodata_wni`.`gelar` AS `gelar`,`tbl_biodata_wni`.`ket_pkrjn` AS `ket_pkrjn`,`tbl_biodata_wni`.`glr_agama` AS `glr_agama`,`tbl_biodata_wni`.`glr_akademis` AS `glr_akademis`,`tbl_biodata_wni`.`glr_bangsawan` AS `glr_bangsawan`,`tbl_biodata_wni`.`is_pros_datang` AS `is_pros_datang`,`tbl_biodata_wni`.`desc_pekerjaan` AS `desc_pekerjaan`,`tbl_biodata_wni`.`desc_kepercayaan` AS `desc_kepercayaan`,`tbl_biodata_wni`.`flag_status` AS `flag_status`,`tbl_biodata_wni`.`count_ktp` AS `count_ktp`,`tbl_biodata_wni`.`count_biodata` AS `count_biodata`,`tbl_biodata_wni`.`flagsink` AS `flagsink` from `tbl_biodata_wni`;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_akta_lahir_relation`
--
ALTER TABLE `tbl_akta_lahir_relation`
  ADD CONSTRAINT `tbl_akta_lahir_relation_ibfk_1` FOREIGN KEY (`bayi_no`) REFERENCES `tbl_capil_lahir` (`bayi_no`),
  ADD CONSTRAINT `tbl_akta_lahir_relation_ibfk_2` FOREIGN KEY (`kandidat_id`) REFERENCES `tbl_kandidat_bayi` (`kandidat_id`);

--
-- Constraints for table `tbl_bidan`
--
ALTER TABLE `tbl_bidan`
  ADD CONSTRAINT `tbl_bidan_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `tbl_biodata_wni` (`nik`);

--
-- Constraints for table `tbl_biodata_wni`
--
ALTER TABLE `tbl_biodata_wni`
  ADD CONSTRAINT `tbl_biodata_wni_ibfk_1` FOREIGN KEY (`no_kk`) REFERENCES `tbl_data_keluarga` (`no_kk`);

--
-- Constraints for table `tbl_checklist_persyaratan_pengajuan`
--
ALTER TABLE `tbl_checklist_persyaratan_pengajuan`
  ADD CONSTRAINT `tbl_checklist_persyaratan_pengajuan_ibfk_1` FOREIGN KEY (`pengajuan_id`) REFERENCES `tbl_pengajuan` (`pengajuan_id`),
  ADD CONSTRAINT `tbl_checklist_persyaratan_pengajuan_ibfk_2` FOREIGN KEY (`pa_id`) REFERENCES `tbl_persyaratan_adduk` (`pa_id`);

--
-- Constraints for table `tbl_detail_kelahiran`
--
ALTER TABLE `tbl_detail_kelahiran`
  ADD CONSTRAINT `tbl_detail_kelahiran_ibfk_1` FOREIGN KEY (`kandidat_id`) REFERENCES `tbl_kandidat_bayi` (`kandidat_id`);

--
-- Constraints for table `tbl_detail_pelapor`
--
ALTER TABLE `tbl_detail_pelapor`
  ADD CONSTRAINT `tbl_detail_pelapor_ibfk_1` FOREIGN KEY (`kandidat_id`) REFERENCES `tbl_kandidat_bayi` (`kandidat_id`),
  ADD CONSTRAINT `tbl_detail_pelapor_ibfk_2` FOREIGN KEY (`nik`) REFERENCES `tbl_biodata_wni` (`nik`);

--
-- Constraints for table `tbl_kandidat_bayi`
--
ALTER TABLE `tbl_kandidat_bayi`
  ADD CONSTRAINT `tbl_kandidat_bayi_ibfk_1` FOREIGN KEY (`pengajuan_id`) REFERENCES `tbl_pengajuan` (`pengajuan_id`),
  ADD CONSTRAINT `tbl_kandidat_bayi_ibfk_2` FOREIGN KEY (`no_kk`) REFERENCES `tbl_data_keluarga` (`no_kk`),
  ADD CONSTRAINT `tbl_kandidat_bayi_ibfk_3` FOREIGN KEY (`sk_id`) REFERENCES `tbl_status_kandidat` (`sk_id`),
  ADD CONSTRAINT `tbl_kandidat_bayi_ibfk_4` FOREIGN KEY (`nik_bayi`) REFERENCES `tbl_biodata_wni` (`nik`),
  ADD CONSTRAINT `tbl_kandidat_bayi_ibfk_5` FOREIGN KEY (`nik_ibu`) REFERENCES `tbl_biodata_wni` (`nik`),
  ADD CONSTRAINT `tbl_kandidat_bayi_ibfk_6` FOREIGN KEY (`nik_ayah`) REFERENCES `tbl_biodata_wni` (`nik`),
  ADD CONSTRAINT `tbl_kandidat_bayi_ibfk_7` FOREIGN KEY (`nik_pelapor`) REFERENCES `tbl_biodata_wni` (`nik`),
  ADD CONSTRAINT `tbl_kandidat_bayi_ibfk_8` FOREIGN KEY (`nik_saksi1`) REFERENCES `tbl_biodata_wni` (`nik`),
  ADD CONSTRAINT `tbl_kandidat_bayi_ibfk_9` FOREIGN KEY (`nik_saksi2`) REFERENCES `tbl_biodata_wni` (`nik`);

--
-- Constraints for table `tbl_pengajuan`
--
ALTER TABLE `tbl_pengajuan`
  ADD CONSTRAINT `tbl_pengajuan_ibfk_1` FOREIGN KEY (`ad_id`) REFERENCES `tbl_administrasi_kependudukan` (`ad_id`),
  ADD CONSTRAINT `tbl_pengajuan_ibfk_2` FOREIGN KEY (`nik`) REFERENCES `tbl_biodata_wni` (`nik`),
  ADD CONSTRAINT `tbl_pengajuan_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `tbl_status_pengajuan` (`status_id`);

--
-- Constraints for table `tbl_persyaratan_adduk`
--
ALTER TABLE `tbl_persyaratan_adduk`
  ADD CONSTRAINT `tbl_persyaratan_adduk_ibfk_1` FOREIGN KEY (`ad_id`) REFERENCES `tbl_administrasi_kependudukan` (`ad_id`),
  ADD CONSTRAINT `tbl_persyaratan_adduk_ibfk_2` FOREIGN KEY (`persyaratan_id`) REFERENCES `tbl_persyaratan` (`persyaratan_id`);

--
-- Constraints for table `tbl_status_kandidat_history`
--
ALTER TABLE `tbl_status_kandidat_history`
  ADD CONSTRAINT `tbl_status_kandidat_history_ibfk_1` FOREIGN KEY (`kandidat_id`) REFERENCES `tbl_kandidat_bayi` (`kandidat_id`),
  ADD CONSTRAINT `tbl_status_kandidat_history_ibfk_2` FOREIGN KEY (`sk_id`) REFERENCES `tbl_status_kandidat` (`sk_id`);

--
-- Constraints for table `tbl_status_pengajuan_history`
--
ALTER TABLE `tbl_status_pengajuan_history`
  ADD CONSTRAINT `tbl_status_pengajuan_history_ibfk_1` FOREIGN KEY (`pengajuan_id`) REFERENCES `tbl_pengajuan` (`pengajuan_id`),
  ADD CONSTRAINT `tbl_status_pengajuan_history_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `tbl_status_pengajuan` (`status_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
