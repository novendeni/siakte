window.onload = function()
{
	var tbtarget = 0;
	var sumtarget = $('.thumbnail-slider .item').length;
	var itsize = 185;
	$('.thumbnail-slider').parent().append('<div class="arrow-left"><i class="fa fa-chevron-left"></i></div><div class="arrow-right"><i class="fa fa-chevron-right"></i></div>'); 
	$(document).on('click', '.arrow-right', function() 
	{
		if(tbtarget > -((sumtarget-4)*itsize))
		{
			tbtarget -= itsize;
		}
		else 
		{
			tbtarget = 0;
		}
		$('.thumbnail-slider .wrapper').animate({left: tbtarget+'px'});
	});
	$(document).on('click', '.arrow-left', function() 
	{
		if(tbtarget < 0)
		{
			tbtarget += itsize;
			
		}
		else 
		{
			tbtarget = -itsize*(sumtarget-4);
		}
		$('.thumbnail-slider .wrapper').animate({left: tbtarget+'px'});
	});
	
	
	//--------------------------------------------------------------------------------------------------------------------------
	
	
	
	var frame = 
	{
		state: 1,
		total: $('.slideshow .frame').length,
		time: 7000,
	};
	
	
					
	$('.slideshow').append('<div class="indicator"></div>'); 
	
	for (i=1; i<= frame.total; i++)
	{
		$('.slideshow .indicator').append('<div class="bullet"></div>');
	}
	changeBullet();
	$(document).on('click', '.slideshow .arrow-right', function() 
	{
		nextFrame();
		resetAutoSlide();
	});
	$(document).on('click', '.slideshow .arrow-left', function() 
	{
		prevFrame();
		resetAutoSlide();
	});
	$(document).on('click', '.indicator .bullet', function() 
	{
		gotoFrame($(this).index()+1);
		resetAutoSlide();
	});

	function gotoFrame(to)
	{
		$('.slideshow .frame:nth-child('+ frame.state +')').removeClass('active');
		frame.state = to;
		$('.slideshow .frame:nth-child('+ frame.state +')').addClass('active');
		changeBullet();
	}
	function nextFrame()
	{
		if (frame.state < frame.total)
		{
			$('.slideshow .frame:nth-child('+ frame.state +')').removeClass('active');
			frame.state++;
			$('.slideshow .frame:nth-child('+ frame.state +')').addClass('active');
			changeBullet();
		}
		else
		{
			$('.slideshow .frame:nth-child('+ frame.state +')').removeClass('active');
			frame.state = 1;
			$('.slideshow .frame:nth-child('+ frame.state +')').addClass('active');
			changeBullet();
		}
		
	}
	function prevFrame()
	{
		if (frame.state > 1)
		{
			$('.slideshow .frame:nth-child('+ frame.state +')').removeClass('active');
			frame.state--;
			$('.slideshow .frame:nth-child('+ frame.state +')').addClass('active');
			changeBullet();
		}
		else
		{
			$('.slideshow .frame:nth-child('+ frame.state +')').removeClass('active');
			frame.state = frame.total;
			$('.slideshow .frame:nth-child('+ frame.state +')').addClass('active');
			changeBullet();
		}
		
	}
	function changeBullet()
	{
		$('.indicator .bullet').removeClass('active');
		$('.indicator .bullet:nth-child('+frame.state+')').addClass('active');
	}
	var autoSlide = window.setInterval(function ()
	{
		nextFrame();
	}, frame.time);
	function resetAutoSlide()
	{
		window.clearInterval(autoSlide);
		autoSlide = window.setInterval(function (){ nextFrame();}, frame.time);
	}
	
	$('img[position="center"]').on('load',function()
	{
		var thisWidth = this.width;
		var thisHeigth = this.height;
		$(this).css('left',($(this).parents().width()-thisWidth)/2+'px');
		$(this).css('top',($(this).parents().height()-thisHeigth)/2+'px');
	});
	$('.frame img[position="fill"]').on('load',function()
	{
		var thisHeigth = this.height;
		$(this).css('top',($(this).parents().height()-thisHeigth)/2+'px');
	});
	$(window).resize(function()
	{
		var thisHeigth = this.height;
		$(this).css('top',($(this).parents().height()-thisHeigth)/2+'px');
		
		$('.frame').each(function()
		{
			$(this).children('img:not(.background)').css('left',($(this).width() - $(this).children('img').width())/2);
			
		})
	});
	$('.frame').each(function()
	{
		$(this).children('img:not(.background)').css('left',($(this).width() - $(this).children('img').width())/2);
		if($(this).children('img.background').length > 0)
		{
			$(this).children('img.background').hide();
			a = $(this).children('img.background').attr('src');
			$(this).css('background','url('+a+') center center no-repeat'); 
		}
	});	
}

$(document).ready(function(){
	// update form base on NO KK
	$('#KandidatBayi_no_kk').change(function(){
		var noKK = $(this).val();
		$.ajax({
			url : 'findKK',
			type : 'POST',
			data : $(this).serialize(),
			success : function(data){
				var obj = JSON.parse(data);
				//alert(obj.nama_lgkp);
				
				$('#KandidatBayi_nik_ayah').val(obj.nik);
				$('#nama_ayah').val(obj.nama_lgkp);
				$('#KandidatBayi_nik_pelapor').val(obj.nik);
				$('#nama_pelapor').val(obj.nama_lgkp);
				$('#Pelapor_hubungan_keluarga').val('ayah');
			}
		});
	});
	
	//update hidden field hubungan keluarga
	$('#hub_kel').change(function(){
		$('#Pelapor_hubungan_keluarga').val($(this).val());
	});
        })        