
$(document).ready(function(){

	// date picker
	
	$('.date-picker input').click(function()
	{
		$(this).datepicker('show');
	});
	
	$('.date-picker input').on('changeDate', function(ev){
		if (ev.viewMode === 'days') {
			$(this).datepicker('hide');
		}
	});
	
	// update form base on NO KK
	$('#KandidatBayi_no_kk').change(function(){
		var noKK = $(this).val();
		$.ajax({
			url : 'findKK',
			type : 'POST',
			data : $(this).serialize(),
			success : function(data){
				var obj = JSON.parse(data);
				//alert(obj.nama_lgkp);
				
				$('#KandidatBayi_nik_ayah').val(obj.nik);
				$('#nama_ayah').val(obj.nama_lgkp);
				$('#KandidatBayi_nik_pelapor').val(obj.nik);
				$('#nama_pelapor').val(obj.nama_lgkp);
				$('#Pelapor_hubungan_keluarga').val('ayah');
			}
		});
	});
	
	//update hidden field hubungan keluarga
	$('#hub_kel').change(function(){
		$('#Pelapor_hubungan_keluarga').val($(this).val());
	});
	
	// show tooltip
	$('[data-toggle="tooltip"]').tooltip();
});


function isBreakpoint(alias) 
{
    return $('.device-' + alias).is(':visible');
}