<?php

class DetailKelahiranController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id)
	{
		$model=new DetailKelahiran;
		$model->kandidat_id = $id;
		//$model->waktu_lahir = '23.59';
		
		// model biodata wni
		$biodata = new BiodataWni;
		
		$kandidat = KandidatBayi::model()->findByPk($id);
		$ayah = BiodataWni::model()->findByPk($kandidat->nik_ayah);
		$ibu = BiodataWni::model()->findByPk($kandidat->nik_ibu);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DetailKelahiran']))
		{
			$model->attributes=$_POST['DetailKelahiran'];
			if($model->save()){
				$biodata->nama_lgkp = $model->nama;
				$biodata->jenis_klmin = $model->jenis_kelamin;
				$biodata->tmpt_lhr = $model->tempat_lahir;
				$biodata->tgl_lhr = $model->tanggal_lahir;
				$biodata->gol_drh = '0';
				$biodata->agama = $ayah->agama;
				$biodata->stat_kwn = '0';
				$biodata->nik_ibu = $ibu->nik;
				$biodata->nama_lgkp_ibu = $ibu->nama_lgkp;
				$biodata->nik_ayah = $ayah->nik;
				$biodata->nama_lgkp_ayah = $ayah->nama_lgkp;
				$biodata->no_kk = $kandidat->no_kk;
				$biodata->no_prop = '13';
				$biodata->no_kab = '05';
				$biodata->no_kec = $ayah->no_kec;
				$biodata->no_kel = $ayah->no_kel;
				
				$noKec = $biodata->no_kec;
				if (strlen($noKec) < 2)
					$noKec = '0' . $noKec;
					
				$tgl = substr($model->tanggal_lahir, 8, 2);
				if ($model->jenis_kelamin === 'perempuan'){
					$tgl = $tgl + 40;
				}
				$bln = substr($model->tanggal_lahir, 5, 2);
				$thn = substr($model->tanggal_lahir, 2, 2);
				
				$counter = 1;
				do{
					$biodata->nik = $biodata->no_prop . $biodata->no_kab . $noKec . $tgl . $bln . $thn . '000' . $counter;
					$status = BiodataWni::model()->findByPk($biodata->nik);
					$counter = $counter + 1;
				}while($status !== NULL);
				
				$biodata->save(false);
				
				$kandidat->nik_bayi = $biodata->nik;
				$kandidat->sk_id = 3;
				$kandidat->save();
				
				// send email to admin
				$to = 'novendeni@gmail.com';
				$from = 'System';
				$subject = 'Notifikasi Kelahiran';
				$message = 'Telah lahir bayi dengan nomor kandidat '.$model->kandidat_id;
				$this->mailsend($to, $from, $subject, $message);
				
				$this->redirect(array('KandidatBayi/index'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DetailKelahiran']))
		{
			$model->attributes=$_POST['DetailKelahiran'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->kandidat_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('DetailKelahiran');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new DetailKelahiran('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DetailKelahiran']))
			$model->attributes=$_GET['DetailKelahiran'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return DetailKelahiran the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=DetailKelahiran::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param DetailKelahiran $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='detail-kelahiran-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
