<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		//$this->render('index');
            
                $this->redirect(array('test'));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
				
				$hak = Yii::app()->user->role;
				
				if($hak === 'bidan')
					$this->redirect(array('/bidan'));
				
				if($hak === 'pemda')
					$this->redirect(array('/pemda'));
				
				if($hak === 'admin')
					$this->redirect(array('kandidatbayi/index'));
				
			}
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	/**
	*
	*/
	public function actionFindBidan(){
		$q = new CDbCriteria();
		$request = trim($_GET['term']);
		if($request != ''){
			$q->addSearchCondition('nama', $request);
			$model = Bidan::model()->findAll($q);
			
			$data = array();
                        if (!empty($model)){
                            foreach($model as $get){
                                    $data[] = array(
                                            'nik'=>$get->nik,
                                            'nama'=>$get->nama,
                                            'alamat'=>$get->alamat
                                    );
                            }
                            //echo CJSON::encode($data);
                        } else {
                            $data[] = array(
                                'nik'=>'0',
                                'nama'=>'Data tidak ditemukan,',
                                'alamat'=>'periksa kembali kata kunci yang diinputkan'
                            );
                        }
                        echo CJSON::encode($data);
		}
	}
	
	/**
	*
	*/
	public function actionFindKK(){
		
		if(isset($_POST['KandidatBayi']['no_kk'])){
			$nokk = $_POST['KandidatBayi']['no_kk'];
			$model = DataKeluarga::model()->findByPK($nokk);
			if($model != null){
				$ayah = BiodataWni::model()->findByAttributes(array('no_kk'=>$nokk,'nama_lgkp'=>$model->nama_kep));
				echo json_encode($ayah->attributes);
			} else {
				echo 'data not found';
			}
		} else
			echo 'no data sent';
	}
	
	/**
	*
	*/
	public function actionFindNama(){
		if(isset($_POST['nik'])){
			$nik = $_POST['nik'];
			$model = BiodataWni::model()->findByPK($nik);

			echo $model->nama_lgkp;
		} else{
			echo 'no data send';
		}
	}
	
	/**
	*
	*/
	public function actionDaftar(){
		$this->layout = '//layouts/column2';
		
		$pengajuan = new Pengajuan;
		$bayi = new KandidatBayi;
		$pelapor = new DetailPelapor;
		
		if(isset($_POST['Pengajuan'], $_POST['KandidatBayi'])){
			$pengajuan->ad_id = 1;
			$pengajuan->nik = $_POST['Pengajuan']['nik'];
			$pengajuan->umur_kehamilan = $_POST['Pengajuan']['umur_kehamilan'];
			$pengajuan->tanggal_pengajuan = date('Y-m-d');
			$pengajuan->insert_datetime = date('Y-m-d');
			$pengajuan->insert_by = 'masyarakat';
			$pengajuan->update_datetime = NULL;
			$pengajuan->update_by = NULL;
			$pengajuan->status_id = 1;
            if ($pengajuan->save()){
				// status history object, fill with data and save
				$stathistory = new StatusPengajuanHistory;
				$stathistory->pengajuan_id = $pengajuan->pengajuan_id;
				$stathistory->status_id = $pengajuan->status_id;
				$stathistory->insert_by = $pengajuan->insert_by;
				$stathistory->insert_datetime = $pengajuan->tanggal_pengajuan;
				$stathistory->save(false);
						
				//checklist persyaratan pengajuan, fill with data and save
				$cpp = new ChecklistPersyaratanPengajuan;
				$cpp->pengajuan_id = $pengajuan->pengajuan_id;
				$cpp->pa_id = 1;
				$cpp->kuantitas = 2;
				if ($pengajuan->status_id == 2){
					$cpp->isQualified = 1;
				} else {
					$cpp->isQualified = 0;
				}
				$cpp->save();
				
				$bayi->pengajuan_id = $pengajuan->pengajuan_id;
				$bayi->attributes = $_POST['KandidatBayi'];
				$bayi->nik_bayi = null;
				$bayi->sk_id = 1;
				$bayi->insert_datetime = date('Y-m-d');
				$bayi->insert_by = 'masyarakat';
				if($bayi->save()){
					$kandidatHist = new StatusKandidatHistory;
					$kandidatHist->kandidat_id = $bayi->kandidat_id;
					$kandidatHist->sk_id = $bayi->sk_id;
					$kandidatHist->insert_datetime = $bayi->insert_datetime;
					$kandidatHist->insert_by = $bayi->insert_by;
					$kandidatHist->save(false);
					
					$pelapor->kandidat_id = $bayi->kandidat_id;
					$pelapor->nik = $bayi->nik_pelapor;
					$pelapor->tanggal_lapor = date('Y-m-d');
					$pelapor->hubungan_keluarga = $_POST['DetailPelapor']['hubungan_keluarga'];
					var_dump($pelapor->attributes);
					$pelapor->save(false);
					
					Yii::app()->user->setFlash('success','Data Telah terkirim, Silahkan kunjungi bidan anda untuk informasi lebih lanjut. Terima Kasih');
				}
			}
		}
		
		$this->render('daftar',array('pengajuan'=>$pengajuan,'bayi'=>$bayi,'pelapor'=>$pelapor));
	}
	
	public function actionTest(){
		$this->layout = '//layouts/column';
                $this->render('index_new');
	}
        
        
}