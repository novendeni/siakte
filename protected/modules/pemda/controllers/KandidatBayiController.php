<?php

class KandidatBayiController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'create', 'approve', 'update', 'list', 'copy', 'review', 'admin', 'rekap'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id_pengajuan)
	{
		$model=new KandidatBayi;
		$model->pengajuan_id = $_GET['id_pengajuan'];
		
		// create new detail pelapor object
		$modelPelapor = new DetailPelapor;
		
		// kandidat history
		$kandidatHist = new StatusKandidatHistory;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KandidatBayi']))
		{
			$model->attributes=$_POST['KandidatBayi'];
			if ($model->keterangan === NULL){
				$model->approve = 0;
			} else {
				$model->approve = 1;
			}
			$model->sk_id = 1;
			$model->insert_datetime = date('Y-m-d');
			$model->insert_by = Yii::app()->user->name;
			
			if($model->save()){
				// fill all attributes model
				$modelPelapor->attributes=$_POST['DetailPelapor'];
				$modelPelapor->kandidat_id = $model->kandidat_id;
				$modelPelapor->tanggal_lapor = $model->insert_datetime;
				$modelPelapor->nik = $model->nik_pelapor;
				
				$kandidatHist->kandidat_id = $model->kandidat_id;
				$kandidatHist->sk_id = $model->sk_id;
				$kandidatHist->insert_datetime = $model->insert_datetime;
				$kandidatHist->insert_by = $model->insert_by;
				
				// save model
				$modelPelapor->save();
				$kandidatHist->save(false);
				
				$this->redirect(array('/bidan/kandidatbayi/sukses'));
			}
		}

		$this->render('create',array(
			'model'=>$model, 
			'modelPelapor'=>$modelPelapor,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		$modelPelapor = new DetailPelapor;
		$modelPelapor = DetailPelapor::model()->findByPk($model->kandidat_id);
		
		// kandidat history
		$kandidatHist = new StatusKandidatHistory;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KandidatBayi']))
		{
			$model->attributes=$_POST['KandidatBayi'];
			$model->update_datetime = date('Y-m-d');
			$model->update_by = Yii::app()->user->name;
			
			$modelPelapor->attributes=$_POST['DetailPelapor'];
			
			if($model->save()){
				$modelPelapor->save();
				
				$kandidatHist->kandidat_id = $model->kandidat_id;
				$kandidatHist->sk_id = $model->sk_id;
				$kandidatHist->insert_datetime = $model->update_datetime;
				$kandidatHist->insert_by = $model->update_by;
				$kandidatHist->save(false);
				
				$this->redirect(array('index'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'modelPelapor'=>$modelPelapor,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $this->layout = '/layouts/sidebar';
            $model=new KandidatBayi('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['KandidatBayi'])){
                    $model->attributes=$_GET['KandidatBayi'];
            } 
            $this->render('index',array(
                    'model'=>$model,
            ));
	}
	
	/**
	*
	*/
	public function actionApprove($id){
		$model=$this->loadModel($id);
		$model->approve = 0;
		$model->save();
		
		Yii::app()->user->setFlash('success','Normal - Deleted Successfully');
		
		$this->redirect(array('/pemda/kandidatbayi/index'));
	}
	
	/**
	 * Lists all models.
	 */
	public function actionList()
	{
            $this->layout = '/layouts/sidebar';
            $model=new KandidatBayi('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['KandidatBayi'])){
                    $model->attributes=$_GET['KandidatBayi'];
            } 
            $this->render('list',array(
                    'model'=>$model,
            ));
	}
	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new KandidatBayi('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['KandidatBayi']))
			$model->attributes=$_GET['KandidatBayi'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return KandidatBayi the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=KandidatBayi::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param KandidatBayi $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='kandidat-bayi-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	/**
	* Lists all models with filter
	*
	*/
	public function actionRekap(){
            $this->layout = '/layouts/sidebar';
            $model=new KandidatBayi('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['KandidatBayi']))
                $model->attributes=$_GET['KandidatBayi'];

            $this->render('rekap',array(
                'model'=>$model,
            ));
	}
	
	/**
	* copy model to another database
	*
	*/
	public function actionCopy($id){
		$model=$this->loadModel($id);
		$detail = DetailKelahiran::model()->findByPk($model->kandidat_id);
		$kk = DataKeluarga::model()->findByPK($model->no_kk);
		$ibu = BiodataWni::model()->findByPk($model->nik_ibu);
		$ayah = BiodataWni::model()->findByPk($model->nik_ayah);
		$pelapor = BiodataWni::model()->findByPk($model->nik_pelapor);
		$kkPelapor = DataKeluarga::model()->findByPK($pelapor->no_kk);
		$detPelapor = DetailPelapor::model()->findByPk($model->kandidat_id);
		$saksi1 = BiodataWni::model()->findByPk($model->nik_saksi1);
		$kkSaksi1 = DataKeluarga::model()->findByPK($saksi1->no_kk);
		$saksi2 = BiodataWni::model()->findByPk($model->nik_saksi2);
		$kkSaksi2 = DataKeluarga::model()->findByPK($saksi2->no_kk);
		
		//sementara copy ke table capil lahir pada database siakte dengan format yg sama dengan oracle
		$capil = new CapilLahir;
		
		$capil->bayi_org_asing = '0';
		$capil->bayi_nik = $model->nik_bayi;
		$capil->bayi_nama_lgkp = $detail->nama;
		$capil->bayi_tmpt_lahir = $detail->tempat_lahir;
		$capil->bayi_tgl_lahir = $detail->tanggal_lahir;
		$capil->bayi_wkt_lahir = $detail->waktu_lahir;
		$capil->bayi_jns_kelamin = $detail->jenis_kelamin;
		$capil->bayi_jns_klhr = $detail->jenis_kelahiran;
		$capil->bayi_tmpt_klhr = ''; //$detail->tempat_lahir
		$capil->bayi_anak_ke = $detail->anak_ke;
		$capil->bayi_berat = $detail->berat;
		$capil->bayi_panjang = $detail->panjang;
		$capil->bayi_pnlg_klhr = $detail->penolong_kelahiran;
		$capil->bayi_dom_lahir = '1';
		$capil->bayi_ln_lahir = '0';
		$capil->no_kk = $model->no_kk;
		$capil->nama_kk = $kk->nama_kep;
		$capil->ibu_nik = $ibu->nik;
		$capil->ibu_nama_lgkp = $ibu->nama_lgkp;
		$capil->ibu_tgl_lahir = $ibu->tgl_lhr;
		$capil->ibu_tgl_kawin = $ibu->tgl_kwn;
		$capil->ibu_wrg_ngr = $ibu->kebangsaan;
		$capil->ibu_pekerjaan = $ibu->ket_pkrjn;
		$capil->ibu_kebangsaan = $ibu->kebangsaan;
		$capil->ibu_alamat = $kk->alamat;
		$capil->ibu_no_rt = $kk->no_rt;
		$capil->ibu_no_rw = $kk->no_rw;
		$capil->ibu_no_prov = $kk->no_prop;
		$capil->ibu_no_kab = $kk->no_kab;
		$capil->ibu_no_kec = $kk->no_kec;
		$capil->ibu_no_kel = $kk->no_kel;
		$capil->ayah_nik = $ayah->nik;
		$capil->ayah_nama_lgkp = $ayah->nama_lgkp;
		$capil->ayah_tgl_lahir = $ayah->tgl_lhr;
		$capil->ayah_wrg_ngr = $ayah->kebangsaan;
		$capil->ayah_pekerjaan = $ayah->ket_pkrjn;
		$capil->ayah_kebangsaan = $ayah->kebangsaan;
		$capil->ayah_alamat = $kk->alamat;
		$capil->ayah_no_rt = $kk->no_rt;
		$capil->ayah_no_rw = $kk->no_rw;
		$capil->ayah_no_prov = $kk->no_prop;
		$capil->ayah_no_kab = $kk->no_kab;
		$capil->ayah_no_kec = $kk->no_kec;
		$capil->ayah_no_kel = $kk->no_kel;
		$capil->plpr_nik = $pelapor->nik;
		$capil->plpr_tgl_lapor = $model->insert_datetime;
		$capil->plpr_umur = ''; //$pelapor->
		$capil->plpr_nama_lgkp = $pelapor->nama_lgkp;
		$capil->plpr_kelamin = $pelapor->jenis_klmin;
		$capil->plpr_pekerjaan = $pelapor->ket_pkrjn;
		$capil->plpr_alamat = $kkPelapor->alamat;
		$capil->plpr_no_rt = $kkPelapor->no_rt;
		$capil->plpr_no_rw = $kkPelapor->no_rw;
		$capil->plpr_no_prov = $kkPelapor->no_prop;
		$capil->plpr_no_kab = $kkPelapor->no_kab;
		$capil->plpr_no_kec = $kkPelapor->no_kec;
		$capil->plpr_no_kel = $kkPelapor->no_kel;
		$capil->plpr_hub_kel = $detPelapor->hubungan_keluarga;
		$capil->saksi1_nik = $saksi1->nik;
		$capil->saksi1_nama_lgkp = $saksi1->nama_lgkp;
		$capil->saksi1_umur = '';
		$capil->saksi1_kelamin = $saksi1->jenis_klmin;
		$capil->saksi1_pekerjaan = $saksi1->ket_pkrjn;
		$capil->saksi1_alamat = $kkSaksi1->alamat;
		$capil->saksi1_no_rt = $kkSaksi1->no_rt;
		$capil->saksi1_no_rw = $kkSaksi1->no_rw;
		$capil->saksi1_no_prov = $kkSaksi1->no_prop;
		$capil->saksi1_no_kab = $kkSaksi1->no_kab;
		$capil->saksi1_no_kec = $kkSaksi1->no_kec;
		$capil->saksi1_no_kel = $kkSaksi1->no_kel;
		$capil->saksi2_nik = $saksi2->nik;
		$capil->saksi2_nama_lgkp = $saksi2->nama_lgkp;
		$capil->saksi2_umur = '';
		$capil->saksi2_kelamin = $saksi2->jenis_klmin;
		$capil->saksi2_pekerjaan = $saksi2->ket_pkrjn;
		$capil->saksi2_alamat = $kkSaksi2->alamat;
		$capil->saksi2_no_rt = $kkSaksi2->no_rt;
		$capil->saksi2_no_rw = $kkSaksi2->no_rw;
		$capil->saksi2_kode_pos  = $kkSaksi2->kode_pos;
		$capil->saksi2_no_prov  = $kkSaksi2->no_prop;
		$capil->saksi2_no_kab = $kkSaksi2->no_kab;
		$capil->saksi2_no_kec = $kkSaksi2->no_kec;
		$capil->saksi2_no_kel = $kkSaksi2->no_kel;
		$capil->adm_no_kedubes = '';
		$capil->adm_no_konjen = '';
		$capil->adm_no_konsul = '';
		$capil->adm_no_prov = '';
		$capil->adm_no_kab = '';
		$capil->adm_no_kec = '';
		$capil->adm_no_kel = '';
		$capil->adm_akta_no = '';
		$capil->adm_dokumen = '';
		$capil->adm_sp_no = '';
		$capil->adm_sp_tgl = '';
		$capil->adm_sp_nama = '';
		$capil->adm_akta_tgl = '';
		$capil->adm_akta_ktp_tgl = '';
		$capil->adm_kades_nama = '';
		$capil->adm_kades_nip = '';
		$capil->adm_regs_nama = '';
		$capil->adm_regs_nip = '';
		$capil->adm_tgl_entry = '';
		$capil->adm_tgl_update = '';
		$capil->user_id = '';
		$capil->flag_update = '';
		$capil->flag_ctk_akta = '';
		$capil->flag_ctk_kakta = '';
		$capil->flag_ctk_sk = '';
		$capil->flag_status = ''; 
		
		$capil->save(false);
		
		$model->sk_id=3;
		$model->save();
		
		$aktaRelation = new AktaLahirRelation;
		$aktaRelation->bayi_no = $capil->bayi_no;
		$aktaRelation->kandidat_id = $model->kandidat_id;
		$aktaRelation->insert_datetime = date('Y-m-d');
		$aktaRelation->insert_by = Yii::app()->user->name;
		$aktaRelation->save(false);
		
		$this->render(array('success'));
	}
	
	/**
	* render new view with data from model
	* 
	*/
	public function actionReview($id){
		$model=$this->loadModel($id);
		$this->render('review', array(  //file blum ada
			'model'=>$model
		));
	}
}
