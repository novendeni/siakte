<?php

class DefaultController extends Controller
{  
	public function actionIndex()
	{
		$this->layout = '/layout/column2';
		$this->render('index');
	}
        
        /* show statistic / chart for spesific request
         *  
         * 
         */
        public function actionStatistic(){
        
            $jenis = ''; $data = ''; $tabel = ''; $kategori = ''; $result = ''; $categories=array(); $values=array(); $title='';
            
            if(isset($_POST['jenis'],$_POST['data'])){
                $jenis = $_POST['jenis'];
                $data = $_POST['data'];
                
                if($_POST['jenis'] === 'pengajuan')
                    $table = 'tbl_pengajuan';
                if($_POST['jenis'] === 'kelahiran')
                    $table = 'AktaLahir';
                if($_POST['data'] === 'kecamatan')
                    $kategori = 'Kecamatan';
                if($_POST['data'] === 'tahun')
                    $kategori = 'bulan';
                
                if($kategori === 'bulan'){
                    $result = Yii::app()->db->createCommand()
                            ->select('MONTHNAME(tanggal_pengajuan) AS bulan , COUNT(*) AS jml')
                            ->from($table)
                            ->group('MONTH(tanggal_pengajuan)')
                            ->queryAll();
          
                    foreach ($result as $item) {
                        $categories[] = $item['bulan'];
                        $values[] = (int)$item['jml'];
                        //$values = $values .','. $item['jml'];
                    }
                    
                }
                $title = json_encode('Statistik Kelahiran');
                //$values = json_encode(substr($values, 1));
                //$values = json_encode($values);
            }           
            $this->render('statistic',array('jenis'=>$jenis,'data'=>$data,'title'=>$title,'categories'=>$categories,'values'=>$values));
        }
}