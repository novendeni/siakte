<?php
    $this->layout = '/layouts/sidebar';
?>

<div class="page-app">
    <div class="section white">
        <div class="container">
            <div class="page-title">
                <div class="title">
                    Statistik
                </div>
                <div class="line">
                </div>
            </div>
        </div>
    </div>
    <div class="section gray">
        <div class="container">
            <?php echo CHtml::beginForm(); ?>
             <table class="table form-table">
                <tr>
                    <td>Pilih Data</td>
                    <td><?php echo CHtml::dropDownList('jenis',$jenis,array('pengajuan'=>'Pengajuan','kelahiran'=>'Kelahiran'),array('empty'=>'-- Pilih --','class'=>'form-control'))?></td>
                </tr>
                <tr>
                    <td>Kategori</td>
                    <td><?php echo CHtml::dropDownList('data',$data,array('tahun'=>'Tahun','kecamatan'=>'Kecamatan'),array('empty'=>'-- Pilih --','class'=>'form-control'))?></td>
                </tr>
                <tr>
                    <td><?php echo CHtml::submitButton('Show',array('class'=>'btn btn-primary')); ?></td>
                </tr>
            <?php echo CHtml::endForm(); ?>
            </table>
        </div>
    </div>
    
    <div class="section white"></div>
    
    <div class="section gray">
        <div class="container">
            <?php 
                if(isset($title) && $title !== ''){
                    $this->Widget('ext.highcharts.HighchartsWidget', array(
                        'options'=>array(
                           'chart'=>array(
                               'type'=>'column',
                           ),
                          'title' => array('text' => $title),
                          'credits' => array('enabled' => false),
                          'xAxis' => array(
                             'categories' => $categories
                          ),
                          'yAxis' => array(
                             'title' => array('text' => 'Jumlah'),
                             'min' => 0
                          ),
                          'series' => array(
                             array(
                                 'name'=>'Kelahiran',
                                 'data'=>$values //[1,4,62,45]
                             )
                          ),
                            'plotOptions' => array(
                                'series' => array(
                                    'colorByPoint' => true
                                )
                            )
                        )
                    ));
                } else { ?>
                <div style="width:50%; float:left; margin-bottom: 10px;">
                    <?php
                    $this->Widget('ext.highcharts.HighchartsWidget', array(
                        'options'=>array(
                            'chart'=>array(
                                'type'=>'column',
                                'plotBackgroundColor' => '#ffffff',
                                'plotBorderWidth' => null,
                                'plotShadow' => false,
                                'height' => 400,
                                'width' => 400,
                            ),
                            'title' => array('text' => 'Pengajuan Tahun 2014'),
                            'credits' => array('enabled' => false),
                            //'colors'=>array('#0563FE', '#6AC36A', '#FFD148', '#FF2F2F'),
                            'xAxis' => array(
                                'categories' => array('Jan','Feb','Maret','April','Mei')
                            ),
                            'yAxis' => array(
                                'title' => array('text' => 'Jumlah'),
                                'min' => 0
                            ),
                            'series' => array(
                                 array(
                                    'name'=>'Kelahiran',
                                    'data'=>[11,21,32,25,18]
                                )
                            ),
                            'plotOptions' => array(
                                'series' => array(
                                    'colorByPoint' => true
                                )
                            )
                        )
                    ));
                 ?>
            </div>

            <div style="width:50%; float:left; margin-bottom: 10px;">
                <?php
            $this->Widget('ext.highcharts.HighchartsWidget', array(
               'options'=>array(
                   'chart'=>array(
                        'type'=>'column',
                        'plotBackgroundColor' => '#ffffff',
                        'plotBorderWidth' => null,
                        'plotShadow' => false,
                        'height' => 400,
                        'width' => 400,
                    ),
                  'title' => array('text' => 'Kelahiran Tahun 2014'),
                  'credits' => array('enabled' => false),
                  'xAxis' => array(
                     'categories' => array('Jan','Feb','Maret','April','Mei')
                  ),
                  'yAxis' => array(
                     'title' => array('text' => 'Jumlah'),
                     'min' => 0
                  ),
                  'series' => array(
                     array(
                         'name'=>'Kelahiran',
                         'data'=>[10,14,32,45,22]
                     )
                  ),
                    'plotOptions' => array(
                        'series' => array(
                            'colorByPoint' => true
                        )
                    )
               )
            ));
            ?>
            </div>

            <div style="width:50%; float:left;">
                <?php
            $this->Widget('ext.highcharts.HighchartsWidget', array(
               'options'=>array(
                   'chart'=>array(
                        'type'=>'column',
                        'plotBackgroundColor' => '#ffffff',
                        'plotBorderWidth' => null,
                        'plotShadow' => false,
                        'height' => 400,
                        'width' => 400,
                    ),
                  'title' => array('text' => 'Kelahiran Per Kecamatan'),
                  'credits' => array('enabled' => false),
                  'xAxis' => array(
                     'categories' => array('Kec1','Kec2','Kec3','Kec4','Kec5')
                  ),
                  'yAxis' => array(
                     'title' => array('text' => 'Jumlah'),
                     'min' => 0
                  ),
                  'series' => array(
                     array(
                         'name'=>'Kelahiran',
                         'data'=>[21,14,32,45,28]
                     )
                  ),
                    'plotOptions' => array(
                        'series' => array(
                            'colorByPoint' => true
                        )
                    )
               )
            ));
            ?>
            </div>

            <div style="width:50%; float:left;">
                <?php
            $this->Widget('ext.highcharts.HighchartsWidget', array(
               'options'=>array(
                   'chart'=>array(
                        'type'=>'column',
                        'plotBackgroundColor' => '#ffffff',
                        'plotBorderWidth' => null,
                        'plotShadow' => false,
                        'height' => 400,
                        'width' => 400,
                    ),
                  'title' => array('text' => 'Pengajuan Per Kecamatan'),
                  'credits' => array('enabled' => false),
                  'xAxis' => array(
                     'categories' => array('Kec1','Kec2','Kec3','Kec4','Kec5')
                  ),
                  'yAxis' => array(
                     'title' => array('text' => 'Jumlah'),
                     'min' => 0
                  ),
                  'series' => array(
                     array(
                         'name'=>'Kelahiran',
                         'data'=>[18,14,42,35,27]
                     )
                  ),
                    'plotOptions' => array(
                        'series' => array(
                            'colorByPoint' => true
                        )
                    )
               )
            ));
            ?>
            </div>
      <?php  } //end else ?>
        </div>
    </div>
</div>



