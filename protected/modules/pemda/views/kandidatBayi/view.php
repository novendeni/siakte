<?php
/* @var $this KandidatBayiController */
/* @var $model KandidatBayi */
?>

<?php
$this->breadcrumbs=array(
	'Kandidat Bayis'=>array('index'),
	$model->kandidat_id,
);

$this->menu=array(
	array('label'=>'List KandidatBayi', 'url'=>array('index')),
	array('label'=>'Create KandidatBayi', 'url'=>array('create')),
	array('label'=>'Update KandidatBayi', 'url'=>array('update', 'id'=>$model->kandidat_id)),
	array('label'=>'Delete KandidatBayi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->kandidat_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage KandidatBayi', 'url'=>array('admin')),
);
?>

<h1>View KandidatBayi #<?php echo $model->kandidat_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'kandidat_id',
		'pengajuan_id',
		'no_kk',
		'nik_bayi',
		'nik_ibu',
		'nik_ayah',
		'nik_pelapor',
		'nik_saksi1',
		'nik_saksi2',
		'sk_id',
		'insert_datetime',
		'insert_by',
		'update_datetime',
		'update_by',
	),
)); ?>