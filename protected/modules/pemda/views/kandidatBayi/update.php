<?php
/* @var $this KandidatBayiController */
/* @var $model KandidatBayi */
?>

<?php
$this->breadcrumbs=array(
	'Kandidat Bayis'=>array('index'),
	$model->kandidat_id=>array('view','id'=>$model->kandidat_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List KandidatBayi', 'url'=>array('index')),
	array('label'=>'Create KandidatBayi', 'url'=>array('create')),
	array('label'=>'View KandidatBayi', 'url'=>array('view', 'id'=>$model->kandidat_id)),
	array('label'=>'Manage KandidatBayi', 'url'=>array('admin')),
);
?>

<h1 align='center'>Update Kandidat Bayi <?php echo $model->kandidat_id; ?></h1>

<?php $this->renderPartial('_formUpdate', array('model'=>$model, 'modelPelapor'=>$modelPelapor)); ?>