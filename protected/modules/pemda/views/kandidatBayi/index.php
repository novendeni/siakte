<?php
/* @var $this KandidatBayiController */
/* @var $dataProvider CActiveDataProvider */

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'setuju',
    'options'=>array(
        //'title'=>'Disetujui',
        'autoOpen'=>false,
    ),
));

echo 'Data telah disetujui';

$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<div class="page-app">
    <div class="section white">
        <div class="container">
            <div class="page-title">
                <div class="title">
                    Perbaikan Data Pengajuan
                </div>
                <div class="line">
                </div>
            </div>
        </div>
    </div>
    
    <div class="section gray">
        <div class="container">
            <?php $this->widget('bootstrap.widgets.TbGridView',array(
                'type' => TbHtml::GRID_TYPE_BORDERED,
                'pager'=>array(),
                'dataProvider'=>$model->search(array('condition'=>'approve=1')),
                'columns'=>array(
                    //'kandidat_id',
                    //'pengajuan_id',
                    //'no_kk',
                    array(
			'name'=>'noKk',
			'type'=>'raw',
                        'header'=>'KK',
                        'value'=>'$data->noKk->nama_kep . "<br/>" . $data->no_kk',
                    ),
                    //'nik_bayi',
                    /*array(
                        'name'=>'nikBayi',
			'type'=>'raw',
                        'header'=>'Bayi',
                        'value'=>'$data->nik_bayi', //$data->nikBayi->nama_lgkp . "<br/>" . 
                    ),*/
                    //'nik_ibu',
                    array(
			'name'=>'nikIbu',
			'type'=>'raw',
                        'header'=>'Ibu',
                        'value'=>'$data->nikIbu->nama_lgkp . "<br/>" . $data->nik_ibu',
                    ),
                    //'nik_ayah',
                    array(
			'name'=>'nikAyah',
			'type'=>'raw',
                        'header'=>'Ayah',
                        'value'=>'$data->nikAyah->nama_lgkp. "<br/>" . $data->nik_ayah',
                    ),
                    //'nik_pelapor',
                    array(
			'name'=>'nikPelapor',
			'type'=>'raw',
                        'header'=>'Pelapor',
                        'value'=>'$data->nikPelapor->nama_lgkp. "<br/>" . $data->nik_pelapor',
                    ),
                    'keterangan',
                    //'nik_saksi1',
                    /*array(
			'name'=>'nikSaksi1',
			'type'=>'raw',
                        'header'=>'Saksi 1',
                        'value'=>'$data->nikSaksi1->nama_lgkp . "<br/>" . $data->nik_saksi1',
                    ), */
                    //'nik_saksi2',
                    /*array(
			'name'=>'nikSaksi2',
			'type'=>'raw',
                        'header'=>'Saksi 2',
                        'value'=>'$data->nikSaksi2->nama_lgkp . "<br/>" . $data->nik_saksi2',
                    ),*/
                    /*
                    'sk_id',
                    'insert_datetime',
                    'insert_by',
                    'update_datetime',
                    'update_by',
                    */
                    array(
                        'name'=>'bidan',
                        'type'=>'raw',
                        'header'=>'Bidan',
                        //'value'=>'$data->pengajuan->nik0->nama_lgkp',
                        'value'=>function($data){
                            $bidan = Bidan::model()->find(array('select'=>'alamat_praktek','condition'=>'nik=:nikBidan','params'=>array(':nikBidan'=>$data->pengajuan->nik)));
                            echo $data->pengajuan->nik0->nama_lgkp. '<br />' .$bidan['alamat_praktek'];
                        }
                    ),
                    array(
			'class'=>'CButtonColumn',
			'template'=>'{setujui}', //{review}
			'buttons'=>array(
                            'setujui'=>array(
                                'label'=>'Telah diperbaiki',
                                'onClick'=>'$("#setuju").dialog("open"); return false;',
                                'imageUrl'=>Yii::app()->request->baseUrl.'/images/setujui.png',
                                'url'=>'Yii::app()->createUrl("/pemda/kandidatbayi/approve", array("id"=>$data->kandidat_id))',
                            ),
			)
                    ),
                ),
            )); 
        ?>
        </div>
    </div>
</div>