<?php
/* @var $this KandidatBayiController */
/* @var $data KandidatBayi */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('kandidat_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kandidat_id),array('view','id'=>$data->kandidat_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pengajuan_id')); ?>:</b>
	<?php echo CHtml::encode($data->pengajuan_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_kk')); ?>:</b>
	<?php echo CHtml::encode($data->no_kk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nik_bayi')); ?>:</b>
	<?php echo CHtml::encode($data->nik_bayi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nik_ibu')); ?>:</b>
	<?php echo CHtml::encode($data->nik_ibu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nik_ayah')); ?>:</b>
	<?php echo CHtml::encode($data->nik_ayah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nik_pelapor')); ?>:</b>
	<?php echo CHtml::encode($data->nik_pelapor); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('nik_saksi1')); ?>:</b>
	<?php echo CHtml::encode($data->nik_saksi1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nik_saksi2')); ?>:</b>
	<?php echo CHtml::encode($data->nik_saksi2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sk_id')); ?>:</b>
	<?php echo CHtml::encode($data->sk_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insert_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->insert_datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insert_by')); ?>:</b>
	<?php echo CHtml::encode($data->insert_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->update_datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_by')); ?>:</b>
	<?php echo CHtml::encode($data->update_by); ?>
	<br />

	*/ ?>

</div>