<?php
/* @var $this KandidatBayiController */
/* @var $dataProvider CActiveDataProvider */

Yii::app()->clientScript->registerScript('search', "

$('.search-form form').submit(function(){
	$('#kandidat-bayi-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="page-app">
    <div class="section white">
        <div class="container">
            <div class="page-title">
                <div class="title">
                    Rekapitulasi Calon Bayi
                </div>
                <div class="line">
                </div>
            </div>
        </div>
    </div>
    
    <div class="section gray">
        <div class="container">
            <?php echo CHtml::beginForm(Yii::app()->createUrl($this->route),'GET');?>
            <table class="table form-table">
                <tr>
                    <td>NIK Saksi</td>
                    <td><?php echo CHtml::activeTextField($model,'nik_saksi1', array('class'=>'form-control','placeholder'=>'NIK Saksi')); ?></td>
                </tr>
                <tr>
                    <td>Insert By</td>
                    <td><?php echo CHtml::activeTextField($model,'insert_by', array('class'=>'form-control','placeholder'=>'NIK Saksi')); ?></td>
                </tr>
                <tr>
                    <td>Kecamatan</td>
                    <td>
                        <?php 
                            echo CHtml::activeDropDownList($model,'kecamatanSearch',
                                array(
                                    '1'=>'LUBUK ALUNG',
                                    '2'=>'BATANG ANAI',
                                    '3'=>'NAN SABARIS',
                                    '4'=>'2 X 11 KAYU TANAM',
                                    '5'=>'VII KOTO SUNGAI SARIK',
                                    '6'=>'V KOTO KP DALAM',
                                    '7'=>'SUNGAI GARINGGING',
                                    '8'=>'SUNGAI LIMAU',
                                    '9'=>'IV KOTO AUR MALINTANG',
                                    '10'=>'ULAKAN TAPAKIS',
                                    '11'=>'SINTUAK TOBOH GADANG',
                                    '12'=>'PADANG SAGO',
                                    '13'=>'BATANG GASAN',
                                    '14'=>'V KOTO TIMUR',
                                    '15'=>'2X11 ENAM LINGKUNG',
                                    '16'=>'PATAMUAN',
                                    '17'=>'ENAM LINGKUNG'
                                ),
                                array(
                                    'empty'=>'-- Pilih --','class'=>'form-control'
                                )
                            )
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><?php echo CHtml::submitButton('Search',array('class'=>'btn btn-primary')); ?></td>
                </tr>
            </table>
            <?php echo CHtml::endForm();?>
        </div>
    </div>
    
    <div class="section white">
        <div class="container">
            <?php 
                $this->widget('bootstrap.widgets.TbGridView',array(
                    'id'=>'kandidat-bayi-grid',
                    'type' => TbHtml::GRID_TYPE_BORDERED,
                    'pager'=>array(),
                    'dataProvider'=>$model->search(),
                    //'filter'=>$model,
                    'columns'=>array(
                        //'kandidat_id',
                        //'pengajuan_id',
                        //'no_kk',
                        array(
                            'name'=>'noKk',
                            'type'=>'raw',
                            'header'=>'KK',
                            'value'=>'$data->noKk->nama_kep . "<br/>" . $data->no_kk',
                        ),
                        //'nik_bayi',
                        /*array(
                            'name'=>'nikBayi',
                            'type'=>'raw',
                            'header'=>'Bayi',
                            'value'=>'$data->nikBayi->nama_lgkp . "<br/>" . $data->nik_bayi',
                        ),*/
                        //'nik_ibu',
                        array(
                            'name'=>'nikIbu',
                            'type'=>'raw',
                            'header'=>'Ibu',
                            'value'=>'$data->nikIbu->nama_lgkp . "<br/>" . $data->nik_ibu',
                        ),
                        //'nik_ayah',
                        array(
                            'name'=>'nikAyah',
                            'type'=>'raw',
                            'header'=>'Ayah',
                            'value'=>'$data->nikAyah->nama_lgkp. "<br/>" . $data->nik_ayah',
                        ),
                        //'nik_pelapor',
                        array(
                            'name'=>'nikPelapor',
                            'type'=>'raw',
                            'header'=>'Pelapor',
                            'value'=>'$data->nikPelapor->nama_lgkp. "<br/>" . $data->nik_pelapor',
                        ),
                        //'nik_saksi1',
                        array(
                            'name'=>'nikSaksi1',
                            'type'=>'raw',
                            'header'=>'Saksi 1',
                            'value'=>'$data->nikSaksi1->nama_lgkp . "<br/>" . $data->nik_saksi1',
                        ),
                        //'nik_saksi2',
                        array(
                            'name'=>'nikSaksi2',
                            'type'=>'raw',
                            'header'=>'Saksi 2',
                            'value'=>'$data->nikSaksi2->nama_lgkp . "<br/>" . $data->nik_saksi2',
                        ),
                        /*
                        'sk_id',
                        'insert_datetime',
                        'insert_by',
                        'update_datetime',
                        'update_by',
                        */
                        array(
                            'class'=>'CButtonColumn',
                            'template'=>'{view}',
                        ),
                    ),
                    
                )); 
            ?>
        </div>
    </div>
</div>