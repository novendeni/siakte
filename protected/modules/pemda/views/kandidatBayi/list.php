<?php
/* @var $this KandidatBayiController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
	Yii::app()->clientScript->registerScript('search', "
		$('.search-form form').submit(function(){
			$('#kandidat-bayi-grid').yiiGridView('update', {
				data: $(this).serialize()
			});
			return false;
		});
	");
?>

<div class="page-app">
    <div class="section white">
        <div class="container">
            <div class="page-title">
                <div class="title">
                    List Calon Bayi
                </div>
                <div class="line">
                </div>
            </div>
        </div>
    </div>
    
    <div class="section gray">
        <div class="container">
            <div class="form-inline">
                <?php echo CHtml::beginForm(Yii::app()->createUrl($this->route),'GET');?>
                    <table class="table form-table">
                        <tr>
                            <td>Status</td>
                            <td>
                                <label class="radio-inline">
                                    <input id="KandidatBayi_sk_id_1" type="radio" name="KandidatBayi[sk_id]" value="1">Baru Entry
                                </label>
                                <label class="radio-inline">
                                    <input id="KandidatBayi_sk_id_2" type="radio" name="KandidatBayi[sk_id]" value="2">Mendekati Kelahiran
                                </label>
                                <label class="radio-inline">
                                    <input id="KandidatBayi_sk_id_3" type="radio" name="KandidatBayi[sk_id]" value="3">Proses Pengajuan
                                </label>
                                <label class="radio-inline">
                                    <input id="KandidatBayi_sk_id_4" type="radio" name="KandidatBayi[sk_id]" value="4">Sudah Dicetak
                                </label>
                                <label class="radio-inline">
                                    <input id="KandidatBayi_sk_id_5" type="radio" name="KandidatBayi[sk_id]" value="5">Sudah Dikirim
                                </label>
                            </td>
                        </tr>
                        <tr><td><?php echo CHtml::submitButton('Search',array('class'=>'btn btn-primary'));?></td></tr>
                    </table>
                <?php echo CHtml::endForm(); ?>
            </div> <!-- form -->
        </div>
    </div>
    
    <div class="section white">
        <div class="container">
            <?php $this->widget('bootstrap.widgets.TbGridView',array(
                'type' => TbHtml::GRID_TYPE_BORDERED,
                'dataProvider'=>$model->search(),
                'pager'=>array(),
                'columns'=>array(
                    //'kandidat_id',
                    //'pengajuan_id',
                    //'no_kk',
                    array(
                        'name'=>'noKk',
                        'type'=>'raw',
                        'header'=>'KK',
                        'value'=>'$data->noKk->nama_kep . "<br/>" . $data->no_kk',
                    ),
                    //'nik_bayi',
                    array(
			'name'=>'nikBayi',
			'type'=>'raw',
                        'header'=>'Bayi',
                        'value'=>'$data->nik_bayi', //$data->nikBayi->nama_lgkp . "<br/>" . 
                    ),
                    //'nik_ibu',
                    array(
			'name'=>'nikIbu',
			'type'=>'raw',
                        'header'=>'Ibu',
                        'value'=>'$data->nikIbu->nama_lgkp . "<br/>" . $data->nik_ibu',
                    ),
                    //'nik_ayah',
                    array(
                        'name'=>'nikAyah',
			'type'=>'raw',
                        'header'=>'Ayah',
                        'value'=>'$data->nikAyah->nama_lgkp. "<br/>" . $data->nik_ayah',
                    ),
                    //'nik_pelapor',
                    array(
                        'name'=>'nikPelapor',
			'type'=>'raw',
                        'header'=>'Pelapor',
                        'value'=>'$data->nikPelapor->nama_lgkp. "<br/>" . $data->nik_pelapor',
                    ),
                    //'nik_saksi1',
                    /*array(
                        'name'=>'nikSaksi1',
			'type'=>'raw',
                        'header'=>'Saksi 1',
                        'value'=>'$data->nikSaksi1->nama_lgkp . "<br/>" . $data->nik_saksi1',
                    ), */
                    //'nik_saksi2',
                    /*array(
			'name'=>'nikSaksi2',
			'type'=>'raw',
                        'header'=>'Saksi 2',
                        'value'=>'$data->nikSaksi2->nama_lgkp . "<br/>" . $data->nik_saksi2',
                    ),*/
                    /*
                    'sk_id',
                    'insert_datetime',
                    'insert_by',
                    'update_datetime',
                    'update_by',
                    */
                    array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}{copy}', //{review}
			'buttons'=>array(
                            /*'review'=>array(
                                'label'=>'lihat data dan copy ke database',
                                'imageUrl'=>Yii::app()->request->baseUrl.'/images/review.png',
                                'url'=>'Yii::app()->createUrl("")',
                            ),*/
                            'copy'=>array(
                                'label'=>'copy ke database',
                                'imageUrl'=>Yii::app()->request->baseUrl.'/images/copy.png',
                                'url'=>'Yii::app()->createUrl("KandidatBayi/copy", array("id"=>$data->kandidat_id))',
                            ),
                        )
                    ),
                ),
            )); ?>
        </div>
    </div>
</div>