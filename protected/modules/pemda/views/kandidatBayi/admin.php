<?php
/* @var $this KandidatBayiController */
/* @var $model KandidatBayi */


$this->breadcrumbs=array(
	'Kandidat Bayis'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List KandidatBayi', 'url'=>array('index')),
	array('label'=>'Create KandidatBayi', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#kandidat-bayi-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Kandidat Bayis</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'kandidat-bayi-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'kandidat_id',
		'pengajuan_id',
		'no_kk',
		'nik_bayi',
		'nik_ibu',
		'nik_ayah',
		/*
		'nik_pelapor',
		'nik_saksi1',
		'nik_saksi2',
		'sk_id',
		'insert_datetime',
		'insert_by',
		'update_datetime',
		'update_by',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>