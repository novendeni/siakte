<?php
    /*
     * 
     */
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css" rel="stylesheet" >
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <link href="<?php echo Yii::app()->request->baseUrl;?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->request->baseUrl;?>/css/siakcapil_new.css" rel="stylesheet">
    </head>
    <body>
	<!---------------[ header ]---------------------------------------------------------------------------------------------->
	<header class="header2">
            <div class="container2">
                <a href="<?php echo Yii::app()->createUrl('/bidan/site/index');?>" class="logo">
                    <div class="image">
                        <img src="<?php echo Yii::app()->request->baseUrl;?>/images/logo.png" />
                    </div>
                    <div class="text">
                        <div class="title">ACE</div>
                        <div class="sub-title">Administrasi Cepat Elektronik</div>
                    </div>
                </a>
                <div class="right-group">
                    <div class="dropdown">
                        <a href="" class="user-info" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="image"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/photo1.png"/></div>
                            <div class="info">
                                <div class="name"><?php echo Yii::app()->user->name;?></div>
                                <div class="sub"><!--User Sub-title--></div>
                            </div>
                        </a>
                        <div class="dropdown-menu user pull-right" role="menu" aria-labelledby="dLabel">
                        <div class="user-info details" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="image"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/photo1.png"/></div>
                            <div class="info">
                                <div class="name"><?php echo Yii::app()->user->name;?></div>
                                <div class="sub">Klinik</div>
                                <div class="sub"><!--lorem ipsum dolor--></div>
                                <br/>
                                <div>
                                    <a href="#" class="btn btn-primary">Profile</a> &nbsp;
                                    <a href="<?php echo Yii::app()->createUrl('/site/logout');?>" class="btn btn-default">Logout <i class="fa fa-sign-out"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>	
                <span class="hidden-xs hidden-sm">
                    <a href="#"><span class="round-icon"><i class="fa fa-twitter"></i></span></a>&nbsp;
                    <a href="#"><span class="round-icon"><i class="fa fa-facebook"></i></span></a>
                </span>
                <span class="hidden-xs hidden-sm" ><i class="fa fa-phone"></i> <span style="color: #ff8b00;">Call Center</span>  021-8093008
                    &nbsp;&nbsp;&nbsp;&nbsp;|
                </span>
            </div>	
	</header>
        
        <?php echo $content; ?>

        <!--------------------[ Footer ]----------------------------------------------------------------------------------------->
	<footer class="footer-app">
            <div class="copyright">Hak cipta &copy; 2014 Dukcapil. Semua Hak Dilindungi</div>
	</footer>
	
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.10.2.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/siakcapil_new.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/New folder/siakte.js"></script>
    </body>
</html>
	
