<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>

<div class="content">
	<div id='container'>
		<?php echo $content; ?>
	</div>
</div><!-- content -->

<?php $this->endContent(); ?>