<?php

?>

<?php $this->beginContent('/layouts/main_new');?>
    <!----------------------[ Main Body ]--------------------------------------------------------------------------------------->
    <div>
        <div class="row app-body">
            <div class="col-md-3 hero-left col-sm-3">
                <ul class="list-menu app-list">
                    <li class="main-header">
                        <i class="fa fa-th-list"></i>
                        Menu
                    </li>
                    <li class="header">Action</li>
                    <li><a href="<?php echo Yii::app()->createUrl('/bidan/pengajuan/list');?>"><i class="fa fa-check-square-o"></i>List Pengajuan</a></li>
                    <li><a href="<?php echo Yii::app()->createUrl('/bidan/pengajuan/daftar');?>"><i class="fa fa-pencil-square-o"></i>Pendaftaran Baru</a></li>
                    <li><a href="<?php echo Yii::app()->createUrl('/bidan/detailKelahiran/lahir');?>"><i class="fa fa-plus"></i>Input Data Kelahiran</a></li>
                    <li class="separator"></li> <!--
                    <li class="header">Menu 2</li>
                    <li><a href="#"><i class="fa fa-check-square-o"></i>List Pengajuan</a></li>
                    <li><a href="#"><i class="fa fa-pencil-square-o"></i>Pendaftaran Baru</a></li>
                    <li><a href="#"><i class="fa fa-plus"></i>Input Data Kelahiran</a></li> -->
                </ul>
            </div>
            <div class="col-md-9 hero-right col-sm-9">
                <?php echo $content;?>
            </div>
        </div>
    </div>
    
<?php $this->endContent();?>