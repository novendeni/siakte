<?php
/* @var $this CustomerController */
/* @var $model Customer */
/* @var $form CActiveForm */
?>

<?php $this->pageTitle=Yii::app()->name . ' - Approve Pengajuan'; ?>

<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<div class="page-title">
	<div class="title"><h3>Approve Pengajuan</h3></div>
</div>

<table class="table">
	<tr>
		<th style="width:20%">No Pengajuan</th>
		<th style="width:30%">Umur Kehamilan</th>
		<th>Tgl Pengajuan</th>
		<th style="width:22%">Action</th>
	</tr>
	<?php if($models != null): ?>
		<?php foreach($models as $model): ?>
			<tr>
				<td><?php echo $model->pengajuan_id;?></td>
				<td><?php echo $model->umur_kehamilan; ?></td>
				<td><?php echo date('d-m-Y',strtotime($model->tanggal_pengajuan));?></td>
				<td>
					<span title="Approve"><a href="<?php echo Yii::app()->createUrl("operator/customer/approve/id/".$model->pengajuan_id); ?>" class="btn btn-1">Setujui</a></span>
					<span title="Reject"><a href="<?php echo Yii::app()->createUrl("operator/customer/remove/id/".$model->pengajuan_id); ?>" class="btn btn-1">Batal</i></a></span>
				</td>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>
</table>

<div class='row'>
<?php 
	$this->widget('CLinkPager', array(
		'pages' => $pages,
		'header'=>'',
		'nextPageLabel'=>'&raquo;',
		'prevPageLabel'=>'&laquo;',
		'selectedPageCssClass'=>'active',
		'hiddenPageCssClass'=>'disabled',
		'htmlOptions'=>array(
			'class'=>'pagination',
		),
		)
	) 
?>
</div>