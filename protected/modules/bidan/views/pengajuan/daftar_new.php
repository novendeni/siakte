<?php
    $this->layout = '/layouts/sidebar';
?>

<div class="top-bar">
    <!--<form class="search-box-1">
        <input type="text" placeholder="Search..." />
        <button type="submit"><i class="fa fa-search"></i></button> 
    </form> -->
</div>
<div class="page-app">
    <form method="POST" action="<?php echo Yii::app()->createUrl('bidan/default/daftar');?>">
        <div class="section white">
            <div class="container">
                <div class="page-title">
                    <div class="title">FORM PENDAFTARAN</div>
                    <div class="line"></div>
                </div>
            </div>
        </div>
        <div class="section gray">
            <div class="container">
                <div class="section-title-1">Data Calon Bayi</div>
                <table class="table form-table">
                    <tr>
                        <td>No. KK</td>
                        <!--<td><input class="form-control" type="text" placeholder="No. KK" /></td>-->
                        <td><?php echo CHtml::activeTextField($bayi,'no_kk',array('class'=>'form-control',));?></td>
                    </tr>
                    <tr>
                        <td>Umur Kehamilan</td>
                        <!--<td><input class="form-control" type="text" placeholder="Umur Kehamilan" /></td>-->
                        <td><?php echo CHtml::activeNumberField($pengajuan,'umur_kehamilan',array('class'=>'form-control'));?></td>
                    </tr>
                    <tr>
                        <td>NIK Ayah</td>
                        <!--<td><input class="form-control" type="text" placeholder="NIK Ayah" /></td>-->
                        <td><?php echo CHtml::activeTextField($bayi,'nik_ayah',array('class'=>'form-control col-xs-5','readOnly'=>true));?></td>
                        <td><?php echo CHtml::textField('nama_ayah','',array('class'=>'form-control col-xs-5','disabled'=>true,'style'=>'margin-left:10px;'));?></td>
                    </tr>
                    <tr>
                        <td>NIK Ibu</td>
                        <!--<td><input class="form-control" type="text" placeholder="NIK Ibu" /></td>-->
                        <td>
                            <?php 
                                echo CHtml::activeTextField($bayi,'nik_ibu',array(
                                    'class'=>'form-control col-xs-5',
                                    'ajax'=>array(
                                        'type'=>'POST',
                                        'data'=>array('nik'=>'js:this.value'),
                                        'url'=>Yii::app()->createUrl('/site/findNama'),
                                        'success'=>'js:function(data){$("#nama_ibu").val(data);}',
                                    )
                                ));
                            ?>
                        </td>
                        <td><?php echo CHtml::textField('nama_ibu','',array('class'=>'form-control col-xs-5','disabled'=>true,'style'=>'margin-left:10px;'));?></td>
                    </tr>
                    <tr>
                        <td>NIK Pelapor</td>
                        <!--<td><input class="form-control" type="text" placeholder="NIK Pelapor" /></td>-->
                        <td>
                            <?php 
                                echo CHtml::activeTextField($bayi,'nik_pelapor',array(
                                    'class'=>'form-control col-xs-5',
                                    'ajax'=>array(
                                        'type'=>'POST',
                                        'data'=>array('nik'=>'js:this.value'),
                                        'url'=>Yii::app()->createUrl('/site/findNama'),
                                        'success'=>'js:function(data){$("#nama_pelapor").val(data);}',
                                    )
                                ));
                            ?>
                        </td>
                        <td><?php echo CHtml::textField('nama_pelapor','',array('class'=>'form-control col-xs-5','disabled'=>true,'style'=>'margin-left:10px;'));?></td>
                    </tr>
                    <tr>
                        <td>Hubungan Keluarga</td>
                        <!--<td>
                            <select class="form-control">
                                <option>Pilih ... </option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </td>-->
                        <td>
                            <?php echo CHtml::activeDropDownList($pelapor,'hubungan_keluarga',array('ayah'=>'Ayah','ibu'=>'Ibu','paman'=>'Paman','bibi'=>'Bibi','kakek'=>'Kakek','nenek'=>'Nenek'),array('empty'=>'-- Pilih --','class'=>'form-control'));?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    
        <div class="section white">
            <div class="container">
                <div class="section-title-1">Data Saksi</div>
                <table class="table form-table">
                    <tr>
                        <td>Saksi 1</td>
                        <!--<td><input class="form-control" type="text" placeholder="No. KK" /></td>-->
                        <td>
                            <?php 
                                echo CHtml::activeTextField($bayi,'nik_saksi1',array('class'=>'form-control col-xs-5','ajax'=>array(
                                        'type'=>'POST',
                                        'data'=>array('nik'=>'js:this.value'),
                                        'url'=>Yii::app()->createUrl('/site/findNama'),
                                        'success'=>'js:function(data){$("#nama_saksi1").val(data);}',
                                )));
                            ?>
                        </td>
                        <td>
                            <?php echo CHtml::textField('nama_saksi1','',array('class'=>'form-control col-xs-5','disabled'=>true,'style'=>'margin-left:10px;'));?>
                        </td>
                    </tr>
                    <tr>
                        <td>Saksi 2</td>
                        <!--<td><input class="form-control" type="text" placeholder="Umur Kehamilan" /></td>-->
                        <td>
                            <?php 
							echo CHtml::activeTextField($bayi,'nik_saksi2',array('class'=>'form-control col-xs-5','ajax'=>array(
								'type'=>'POST',
								'data'=>array('nik'=>'js:this.value'),
								'url'=>Yii::app()->createUrl('/site/findNama'),
								'success'=>'js:function(data){$("#nama_saksi2").val(data);}',
							)));
						?>
                        </td>
                        <td>
                            <?php echo CHtml::textField('nama_saksi2','',array('class'=>'form-control col-xs-5','disabled'=>true,'style'=>'margin-left:10px;'));?>
                        </td>
                    </tr>
                </table>	
            </div>
        </div>

        <div class="section gray">
            <div class="container">
                <div class="section-title-1">Persyaratan</div>
                <table class="table form-table">
                    <tr>
                        <td>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="persyaratan[]" value="KTPA"> KTP Ayah
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="persyaratan[]" value="KTPI"> KTP Ibu
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="persyaratan[]" value="KK"> Kartu Keluarga
                            </label>
                        </td>
                    </tr>
                </table>	
            </div>
        </div>

        <div class="section white">
            <div class="container">
                <div class="section-title-1">Data Bermasalah</div>
                <table class="table form-table">
                    <tr>
                        <td>Permasalahan</td>
                        <td>
                            <!--<textarea class="form-control" rows="3"></textarea>-->
                            <?php echo CHtml::activeTextArea($bayi,'keterangan',array('cols'=>50));?><br/>
                           <span style="font-size: 12px;">*di isi hanya jika data persyaratan bermasalah!</span>
                        </td>
                    </tr>
                </table>
                <br/>
                <input class="btn btn-primary" type="submit" value=" Save ">
            </div>
        </div>
    </form>
</div>