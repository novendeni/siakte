<?php
/* @var $this PengajuanController */
/* @var $model Pengajuan */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <?php echo $form->textFieldControlGroup($model,'pengajuan_id',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'ad_id',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'nik',array('span'=>5,'maxlength'=>16)); ?>

                    <?php echo $form->textFieldControlGroup($model,'umur_kehamilan',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'status_id',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'tanggal_pengajuan',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'insert_datetime',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'insert_by',array('span'=>5,'maxlength'=>20)); ?>

                    <?php echo $form->textFieldControlGroup($model,'update_datetime',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'update_by',array('span'=>5,'maxlength'=>20)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton('Search',  array('color' => TbHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->