<?php
/* @var $this PengajuanController */
/* @var $model Pengajuan */
/* @var $form CActiveForm */
?>

<?php $this->pageTitle=Yii::app()->name . ' - List Pengajuan'; ?>

<div class="page-title">
	<div class="title"><h3>List Pengajuan</h3></div>
</div>

<?php if(Yii::app()->user->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<table class="table">
	<tr>
            <th>Nomor</th>
		<th>Nomor KK</th>
		<th>Nama Ayah</th>
		<th>Nama Ibu</th>
		<th>Tanggal Pengajuan</th>
		<th>Umur Kehamilan</th>
        <th>status</th>
		<th>Action</th>
	</tr>
	<?php if($models != null): ?>
		<?php foreach($models as $model): ?>
            <?php $kandidat = KandidatBayi::model()->findByAttributes(array('pengajuan_id'=>$model->pengajuan_id));?>
			<?php if($kandidat != NULL) : ?>	
				<tr>
					<td><?php echo $model->pengajuan_id;?></td>
					<td><?php echo $kandidat->no_kk;?></td>
					<td><?php echo $kandidat->nikAyah->nama_lgkp;?></td>
					<td><?php echo $kandidat->nikIbu->nama_lgkp;?></td>
					<td><?php echo $model->tanggal_pengajuan; ?></td>
					<td><?php echo $model->umur_kehamilan; ?></td>
					<td><?php echo $model->status->keterangan; ?></td>
					<td>
						<span title="Review Pengajuan"><a href="<?php echo Yii::app()->createUrl("bidan/pengajuan/edit/id/".$model->pengajuan_id); ?>"><i class="fa fa-pencil"></i></a></span>
					</td>
				</tr>
			<?php endif;?>
		<?php endforeach; ?>
	<?php endif; ?>
</table>

<div class='row'>
<?php 
	$this->widget('CLinkPager', array(
		'pages' => $pages,
		'header'=>'',
		'nextPageLabel'=>'&raquo;',
		'prevPageLabel'=>'&laquo;',
		'selectedPageCssClass'=>'active',
		'hiddenPageCssClass'=>'disabled',
		'htmlOptions'=>array(
			'class'=>'pagination',
		),
		)
	) 
?>
</div>

