<?php
	//@var SiteController
?>
<h2>Form Pendaftaran</h2>

<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?php echo CHtml::beginForm(Yii::app()->createUrl('/bidan/pengajuan/edit',array('id'=>$pengajuan->pengajuan_id)),'POST',array('class'=>'form-horizontal'));?>
	<?php echo CHtml::errorSummary(array($bayi,$pengajuan,$pelapor),'','',array('class'=>'alert alert-danger'));?>
	<div class="panel-group" id="accrodion">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>
					<a data-toggle="collapse" data-parent="accordion" href="#kandidat-bayi">Data Calon Bayi</a>
				</h4>
			</div>
			<div id="kandidat-bayi" class="panel-collapse collapse in">
				<div class="panel-body">
					<div class="form-group form-inline">
						<?php echo CHtml::activeLabel($bayi,'no_kk',array('class'=>'control-label col-xs-3'));?>
						<?php echo CHtml::activeTextField($bayi,'no_kk',array('class'=>'form-control col-xs-5',));?>
					</div>
					<div class="form-group form-inline">
						<?php echo CHtml::activeLabel($pengajuan,'umur_kehamilan',array('label'=>'Umur Kehamilan','class'=>'control-label col-xs-3'));?>
						<div class="input-group col-xs-4">
							<?php echo CHtml::activeNumberField($pengajuan,'umur_kehamilan',array('class'=>'form-control'));?>
							<span class="input-group-addon">Hari</span>
						</div>
					</div>
					<div class="form-group form-inline">
						<?php echo CHtml::activeLabel($bayi,'nik_ayah',array('class'=>'control-label col-xs-3'));?>
						<?php echo CHtml::activeTextField($bayi,'nik_ayah',array('class'=>'form-control col-xs-5','readOnly'=>true));?>
						&nbsp;
						<?php echo CHtml::textField('nama_ayah','',array('class'=>'form-control col-xs-5','disabled'=>true,'style'=>'margin-left:10px;'));?>
					</div>
					<div class="form-group form-inline">
						<?php echo CHtml::activeLabel($bayi,'nik_ibu',array('class'=>'control-label col-xs-3'));?>
						<?php 
							echo CHtml::activeTextField($bayi,'nik_ibu',array('class'=>'form-control col-xs-5','ajax'=>array(
								'type'=>'POST',
								'data'=>array('nik'=>'js:this.value'),
								'url'=>Yii::app()->createUrl('/bidan/default/findNama'),
								'success'=>'js:function(data){$("#nama_ibu").val(data);}',
							)));
						?>
						&nbsp;
						<?php echo CHtml::textField('nama_ibu','',array('class'=>'form-control col-xs-5','disabled'=>true,'style'=>'margin-left:10px;'));?>
					</div>
					<div class="form-group form-inline">
						<?php echo CHtml::activeLabel($bayi,'nik_pelapor',array('class'=>'control-label col-xs-3'));?>
						<?php 
							echo CHtml::activeTextField($bayi,'nik_pelapor',array('class'=>'form-control col-xs-5','ajax'=>array(
								'type'=>'POST',
								'data'=>array('nik'=>'js:this.value'),
								'url'=>Yii::app()->createUrl('/site/findNama'),
								'success'=>'js:function(data){$("#nama_pelapor").val(data);}',
							)));
						?>
						&nbsp;
						<?php echo CHtml::textField('nama_pelapor','',array('class'=>'form-control col-xs-5','disabled'=>true,'style'=>'margin-left:10px;'));?>
					</div>
					<div class="form-group form-inline">
						<?php echo CHtml::activeLabel($pelapor,'hubungan_keluarga',array('class'=>'control-label col-xs-3'));?>
						<?php //echo CHtml::activehiddenField($pelapor,'hubungan_keluarga',array('class'=>'form-control col-xs-5'));?>
						<?php //echo CHtml::dropDownList('hub_kel',$pelapor->hubungan_keluarga,array('ayah'=>'Ayah','ibu'=>'Ibu','paman'=>'Paman','bibi'=>'Bibi','kakek'=>'Kakek','nenek'=>'Nenek'),array('empty'=>'-- Pilih --','class'=>'form-control','style'=>'width: 195px;'));?>
						<?php echo CHtml::activeDropDownList($pelapor,'hubungan_keluarga',array('ayah'=>'Ayah','ibu'=>'Ibu','paman'=>'Paman','bibi'=>'Bibi','kakek'=>'Kakek','nenek'=>'Nenek'),array('empty'=>'-- Pilih --','class'=>'form-control','style'=>'width: 195px;'));?>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>
					<a data-toggle="collapse" data-parent="accordion" href="#saksi">Data Saksi</a>
				</h4>
			</div>
			<div id="saksi" class="panel-collapse collapse in">
				<div class="panel-body">
					<div class="form-group form-inline">
						<?php echo CHtml::activeLabel($bayi,'nik_saksi1',array('class'=>'control-label col-xs-3'));?>
						<?php 
							echo CHtml::activeTextField($bayi,'nik_saksi1',array('class'=>'form-control col-xs-5','ajax'=>array(
								'type'=>'POST',
								'data'=>array('nik'=>'js:this.value'),
								'url'=>Yii::app()->createUrl('/site/findNama'),
								'success'=>'js:function(data){$("#nama_saksi1").val(data);}',
							)));
						?>
						&nbsp;
						<?php echo CHtml::textField('nama_saksi1','',array('class'=>'form-control col-xs-5','disabled'=>true,'style'=>'margin-left:10px;'));?>
					</div>
					<div class="form-group form-inline">
						<?php echo CHtml::activeLabel($bayi,'nik_saksi2',array('class'=>'control-label col-xs-3'));?>
						<?php 
							echo CHtml::activeTextField($bayi,'nik_saksi2',array('class'=>'form-control col-xs-5','ajax'=>array(
								'type'=>'POST',
								'data'=>array('nik'=>'js:this.value'),
								'url'=>Yii::app()->createUrl('/site/findNama'),
								'success'=>'js:function(data){$("#nama_saksi2").val(data);}',
							)));
						?>
						&nbsp;
						<?php echo CHtml::textField('nama_saksi2','',array('class'=>'form-control col-xs-5','disabled'=>true,'style'=>'margin-left:10px;'));?>
					</div>
					
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>
					<a data-toggle="collapse" data-parent="accordion" href="#persyaratan">Persyaratan</a>
				</h4>
			</div>
			<div id="persyaratan" class="panel-collapse collapse in">
				<div class="panel-body">
					<div class="form-group">
					<table>
						<tr>
							<td>
								<?php echo CHtml::label('persyaratan','',array('class'=>'control-label col-xs-3'));?>
							</td>
							<td>
								<input type="checkbox" name="persyaratan[]" value="KTPA">&nbsp; KTP Ayah<br>
                                <input type="checkbox" name="persyaratan[]" value="KTPI">&nbsp; KTP Ibu<br>
                                <input type="checkbox" name="persyaratan[]" value="KK">&nbsp; Kartu Keluarga
							</td>
						</tr>
					</table>
					</div>			
				</div>
			</div>
		</div>
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>
					<a data-toggle="collapse" data-parent="accordion" href="#Data Bermasalah">Data Bermasalah</a>
				</h4>
			</div>
			<div id="persyaratan" class="panel-collapse collapse in">
				<div class="panel-body">
					*di isi hanya jika data persyaratan bermasalah!
					<div class="form-group">
					<table>
						<tr>
							<td>
								<?php echo CHtml::label('Permasalahan','',array('class'=>'control-label col-xs-3'));?>
							</td>
							<td>
								<?php echo CHtml::activeTextArea($bayi,'keterangan',array('cols'=>50));?>
							</td>
						</tr>
					</table>
					</div>			
				</div>
			</div>
		</div>
		<br/>
		<?php echo CHtml::submitButton('Simpan',array('class'=>'btn btn-primary')); ?>
	</div>
<?php echo CHtml::endForm();?>

<?php
	Yii::app()->clientScript->registerScript('autocomplete',"
		jQuery('#nama_bidan').data('uiAutocomplete')._renderItem = function(ul,item) {
			return $('<li></li>')
			.data('ui-autocomplete-item',item)
			.append('<a>' + item.nama + '<br><i>' + item.alamat + '</i></a>')
			.appendTo(ul);
		};",
		CClientScript::POS_READY
	);
?>