<?php
/* @var $this DetailKelahiranController */
/* @var $model DetailKelahiran */
?>

<?php
$this->breadcrumbs=array(
	'Detail Kelahirans'=>array('index'),
	$model->kandidat_id,
);

$this->menu=array(
	array('label'=>'List DetailKelahiran', 'url'=>array('index')),
	array('label'=>'Create DetailKelahiran', 'url'=>array('create')),
	array('label'=>'Update DetailKelahiran', 'url'=>array('update', 'id'=>$model->kandidat_id)),
	array('label'=>'Delete DetailKelahiran', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->kandidat_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DetailKelahiran', 'url'=>array('admin')),
);
?>

<h1>View DetailKelahiran #<?php echo $model->kandidat_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'kandidat_id',
		'nama',
		'tanggal_lahir',
		'waktu_lahir',
		'jenis_kelamin',
		'jenis_kelahiran',
		'tempat_lahir',
		'anak_ke',
		'berat',
		'panjang',
		'penolong_kelahiran',
	),
)); ?>