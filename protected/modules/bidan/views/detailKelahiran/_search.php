<?php
/* @var $this DetailKelahiranController */
/* @var $model DetailKelahiran */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <?php echo $form->textFieldControlGroup($model,'kandidat_id',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'nama',array('span'=>5,'maxlength'=>40)); ?>

                    <?php echo $form->textFieldControlGroup($model,'tanggal_lahir',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'waktu_lahir',array('span'=>5,'maxlength'=>8)); ?>

                    <?php echo $form->textFieldControlGroup($model,'jenis_kelamin',array('span'=>5,'maxlength'=>1)); ?>

                    <?php echo $form->textFieldControlGroup($model,'jenis_kelahiran',array('span'=>5,'maxlength'=>12)); ?>

                    <?php echo $form->textFieldControlGroup($model,'tempat_lahir',array('span'=>5,'maxlength'=>40)); ?>

                    <?php echo $form->textFieldControlGroup($model,'anak_ke',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'berat',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'panjang',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'penolong_kelahiran',array('span'=>5,'maxlength'=>20)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton('Search',  array('color' => TbHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->