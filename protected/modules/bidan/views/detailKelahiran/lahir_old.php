<?php
/* @var $this DetailKelahiranController */
/* @var $model KDetail Kelahiran */
/* @var $form TbActiveForm */

?>

<h1>Input Data Kelahiran</h1>

<br/>

<div class="form">
    <?php echo CHtml::beginForm(); ?>
    <div class="form-group form-inline">
        <?php echo CHtml::activeLabel($model,'pengajuan_id',array('label'=>'Nomor Pengajuan','class'=>'control-label col-xs-3'));?>
        <?php echo CHtml::activeNumberField($model,'pengajuan_id', array('class'=>'form-control','placeholder'=>'Nomor Pengajuan')); ?>
    </div>
    <div class="form-group form-inline">
        <?php echo CHtml::label('Jumlah Bayi','',array('label'=>'Jumlah bayi','class'=>'control-label col-xs-3'));?>
        <?php echo CHtml::numberField('jml_bayi',$jml, array('class'=>'form-control','min'=>1)); ?>
    </div>
    <?php echo CHtml::submitButton('Input',array('class'=>'btn btn-primary')); ?>
    <?php echo CHtml::endForm(); ?>
</div>
    
<br />

<?php if(isset($kandidat) && $kandidat != NULL): ?>
<div class="form">
    <?php echo CHtml::beginForm(); ?>
    <div class="form-group form-inline">
        <?php echo CHtml::activeLabel($kandidat,'nik_ayah',array('label'=>'Nama Ayah','class'=>'control-label col-xs-3'));?>
        <?php echo CHtml::activeTextField($kandidat,'nik_ayah', array('class'=>'form-control','disabled'=>true,'value'=>$kandidat->nikAyah->nama_lgkp)); ?>
    </div>
    <div class="form-group form-inline">
        <?php echo CHtml::activeLabel($kandidat,'nik_ibu',array('label'=>'Nama Ibu','class'=>'control-label col-xs-3'));?>
        <?php echo CHtml::activeTextField($kandidat,'nik_ibu', array('class'=>'form-control','disabled'=>true,'value'=>$kandidat->nikIbu->nama_lgkp)); ?>
    </div>
     <?php echo CHtml::endForm(); ?>
</div>
<?php endif; ?>

<div class="panel-group" id="accrodion">
    <?php echo CHtml::beginForm(); ?>
        <?php foreach($models as $i=>$item): ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Data Bayi <?php echo $i+1;?></h4>
                </div>
                <div id="pengajuan" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="form-group form-inline">
                            <?php echo CHtml::activeLabel($item,"[$i]nama",array('label'=>'Nama Bayi','class'=>'control-label col-xs-3'));?>
                            <?php echo CHtml::activeTextField($item,"[$i]nama",array('class'=>'form-control','placeholder'=>'Nama'));?>
                        </div>
                        <div class="form-group form-inline">
                            <?php echo CHtml::activeLabel($item,"[$i]tanggal_lahir",array('label'=>'Tanggal Lahir','class'=>'control-label col-xs-3'));?>
                            <div class="input-append date date-picker inline">
                                <?php echo CHtml::activeTextField($item,"[$i]tanggal_lahir", array('class'=>'form-control','placeholder'=>'dd-mm-yyyy', 'data-date'=>'12-02-2012', 'data-date-format'=>'dd-mm-yyyy', 'data-date-viewmode'=>'years')); ?>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <?php echo CHtml::activeLabel($item,"[$i]waktu_lahir",array('label'=>'Waktu Lahir','class'=>'control-label col-xs-3'));?>
                            <?php echo CHtml::activeTextField($item,"[$i]waktu_lahir",array('class'=>'form-control','placeholder'=>'jam:menit (21:59)'));?>
                        </div>
                        <div class="form-group form-inline">
                            <?php echo CHtml::activeLabel($item,"[$i]jenis_kelamin",array('label'=>'Jenis Kelamin','class'=>'control-label col-xs-3'));?>
                            <?php echo CHtml::activeDropDownList($item,"[$i]jenis_kelamin",array('L'=>'Laki - Laki', 'P'=>'Perempuan'),array('class'=>'form-control','empty'=>'Pilih'));?>
                        </div>
                        <div class="form-group form-inline">
                            <?php echo CHtml::activeLabel($item,"[$i]jenis_kelahiran",array('label'=>'Jenis Kelahiran','class'=>'control-label col-xs-3'));?>
                            <?php echo CHtml::activeDropDownList($item,"[$i]jenis_kelahiran",array('normal'=>'Normal', 'prematur'=>'Premature'),array('class'=>'form-control','empty'=>'Pilih'));?>
                        </div>
                        <div class="form-group form-inline">
                            <?php echo CHtml::activeLabel($item,"[$i]tempat_lahir",array('label'=>'Tempat Lahir','class'=>'control-label col-xs-3'));?>
                            <?php echo CHtml::activeTextField($item,"[$i]tempat_lahir",array('class'=>'form-control','placeholder'=>'Tempat Lahir'));?>
                        </div>
                        <div class="form-group form-inline">
                            <?php echo CHtml::activeLabel($item,"[$i]anak_ke",array('label'=>'Anak Ke-','class'=>'control-label col-xs-3'));?>
                            <?php echo CHtml::activeNumberField($item,"[$i]anak_ke",array('class'=>'form-control','placeholder'=>'Anak ke-','min'=>1));?>
                        </div>
                        <div class="form-group form-inline">
                            <?php echo CHtml::activeLabel($item,"[$i]berat",array('label'=>'Berat Bayi','class'=>'control-label col-xs-3'));?>
                            <div class="input-group col-xs-4">
                                <?php echo CHtml::activeTextField($item,"[$i]berat",array('class'=>'form-control','placeholder'=>'Berat'));?>
                                <span class="input-group-addon">gram</span>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <?php echo CHtml::activeLabel($item,"[$i]panjang",array('label'=>'Panjang Bayi','class'=>'control-label col-xs-3'));?>
                            <div class="input-group col-xs-4">
                                <?php echo CHtml::activeTextField($item,"[$i]panjang",array('class'=>'form-control','placeholder'=>'Panjang'));?>
                                <span class="input-group-addon">cm</span>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <?php echo CHtml::activeLabel($item,"[$i]penolong_kelahiran",array('label'=>'Penolong Kelahiran','class'=>'control-label col-xs-3'));?>
                            <?php echo CHtml::activeDropDownList($item,"[$i]penolong_kelahiran",array('bidan'=>'Bidan', 'dokter'=>'Dokter'),array('class'=>'form-control','empty'=>'Pilih'));?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    <br />
    <?php echo CHtml::submitButton('Simpan',array('class'=>'btn btn-primary')); ?>
</div>