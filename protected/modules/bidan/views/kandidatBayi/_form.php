<?php
/* @var $this KandidatBayiController */
/* @var $model KandidatBayi */
/* @var $form TbActiveForm */

	Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
			$('.search-form').toggle();
			return false;
		});
	
	
	$('.toggle-text').change(function(){
			if ($('.toggle-text').val() == 2){
				$('.search-form').toggle()
			}
		});
	");
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
	'id'=>'kandidat-bayi-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Kolom dengan tanda <span class="required">*</span> WAJIB diisi !</p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldControlGroup($model,'pengajuan_id',array('disabled' => true, 'span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'no_kk',array('span'=>5,'maxlength'=>16)); ?>

            <?php //echo $form->textFieldControlGroup($model,'nik_bayi',array('span'=>5,'maxlength'=>16)); ?>

            <?php echo $form->textFieldControlGroup($model,'nik_ibu',array('span'=>5,'maxlength'=>16)); ?>

            <?php echo $form->textFieldControlGroup($model,'nik_ayah',array('span'=>5,'maxlength'=>16)); ?>

            <?php echo $form->textFieldControlGroup($model,'nik_pelapor',array('span'=>5,'maxlength'=>16)); ?>
			
			<?php echo $form->dropDownListControlGroup($modelPelapor,'hubungan_keluarga',array('ayah'=>'Ayah', 'ibu'=>'Ibu', 'paman'=>'Paman', 'bibi'=>'Bibi', 'dll'=>'Dll'), array('empty'=>'Pilih')); ?>

            <?php echo $form->textFieldControlGroup($model,'nik_saksi1',array('span'=>5,'maxlength'=>16)); ?>

            <?php echo $form->textFieldControlGroup($model,'nik_saksi2',array('span'=>5,'maxlength'=>16)); ?>
			
			<?php echo TbHtml::dropDownListControlGroup('Status', '',array('1'=>'Tidak Bermasalah', '2'=>'Bermasalah'), array(
					'empty' => 'Pilih',
					'class' => 'toggle-text', 
					'label' => 'Status Data',
					));
			?>
			
			<div class="search-form" style="display:none">
				<?php echo $form->textAreaControlGroup($model,'keterangan',array('span' => 5, 'rows' => 5)); ?>
			</div>
			
            <?php 
				//$status = CHtml::listData(StatusKandidat::model()->findAll(array('order' => 'sk_id')), 'sk_id', 'judul');
				//echo $form->dropDownListControlGroup($model,'sk_id',$status); 
			?>

            <?php //echo $form->textFieldControlGroup($model,'insert_datetime',array('span'=>5)); ?>

            <?php //echo $form->textFieldControlGroup($model,'insert_by',array('span'=>5,'maxlength'=>20)); ?>

            <?php //echo $form->textFieldControlGroup($model,'update_datetime',array('span'=>5)); ?>

            <?php //echo $form->textFieldControlGroup($model,'update_by',array('span'=>5,'maxlength'=>20)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Perbaharui',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    //'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->