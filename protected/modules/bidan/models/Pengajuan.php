<?php

/**
 * This is the model class for table "tbl_pengajuan".
 *
 * The followings are the available columns in table 'tbl_pengajuan':
 * @property integer $pengajuan_id
 * @property integer $ad_id
 * @property string $nik
 * @property integer $umur_kehamilan
 * @property integer $status_id
 * @property string $tanggal_pengajuan
 * @property string $insert_datetime
 * @property string $insert_by
 * @property string $update_datetime
 * @property string $update_by
 *
 * The followings are the available model relations:
 * @property PersyaratanAdduk[] $tblPersyaratanAdduks
 * @property KandidatBayi[] $kandidatBayis
 * @property AdministrasiKependudukan $ad
 * @property BiodataWni $nik0
 * @property StatusPengajuan $status
 * @property StatusPengajuan[] $tblStatusPengajuans
 */
class Pengajuan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_pengajuan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ad_id, umur_kehamilan, status_id, tanggal_pengajuan, insert_datetime, insert_by', 'required','message'=>'{attribute} tidak boleh kosong.'),
			array('ad_id, umur_kehamilan, status_id, nik', 'numerical', 'integerOnly'=>true),
			array('nik', 'length', 'max'=>16),
			array('insert_by, update_by', 'length', 'max'=>20),
			array('update_datetime', 'safe'),
			array('nik', 'exist', 'allowEmpty'=>false, 'attributeName'=>'nik', 'className'=>'BiodataWni', 'message'=>'{attribute} tidak ditemukan.'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pengajuan_id, ad_id, nik, umur_kehamilan, status_id, tanggal_pengajuan, insert_datetime, insert_by, update_datetime, update_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblPersyaratanAdduks' => array(self::MANY_MANY, 'PersyaratanAdduk', 'tbl_checklist_persyaratan_pengajuan(pengajuan_id, pa_id)'),
			'kandidatBayis' => array(self::HAS_MANY, 'KandidatBayi', 'pengajuan_id'),
			'ad' => array(self::BELONGS_TO, 'AdministrasiKependudukan', 'ad_id'),
			'nik0' => array(self::BELONGS_TO, 'BiodataWni', 'nik'),
                        'nik1' => array(self::BELONGS_TO, 'Bidan', 'nik'),
			'status' => array(self::BELONGS_TO, 'StatusPengajuan', 'status_id'),
			'tblStatusPengajuans' => array(self::MANY_MANY, 'StatusPengajuan', 'tbl_status_pengajuan_history(pengajuan_id, status_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pengajuan_id' => 'Nomor Pengajuan',
			'ad_id' => 'Jenis Pengajuan',
			'nik' => 'NIK Bidan/Dokter',
			'umur_kehamilan' => 'Umur Kehamilan',
			'status_id' => 'Status Pengajuan',
			'tanggal_pengajuan' => 'Tanggal Pengajuan',
			'insert_datetime' => 'Insert Datetime',
			'insert_by' => 'Insert By',
			'update_datetime' => 'Update Datetime',
			'update_by' => 'Update By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pengajuan_id',$this->pengajuan_id);
		$criteria->compare('ad_id',$this->ad_id);
		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('umur_kehamilan',$this->umur_kehamilan);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('tanggal_pengajuan',$this->tanggal_pengajuan,true);
		$criteria->compare('insert_datetime',$this->insert_datetime,true);
		$criteria->compare('insert_by',$this->insert_by,true);
		$criteria->compare('update_datetime',$this->update_datetime,true);
		$criteria->compare('update_by',$this->update_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pengajuan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
