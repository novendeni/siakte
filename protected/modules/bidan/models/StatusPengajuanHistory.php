<?php

/**
 * This is the model class for table "tbl_status_pengajuan_history".
 *
 * The followings are the available columns in table 'tbl_status_pengajuan_history':
 * @property integer $pengajuan_id
 * @property integer $status_id
 * @property string $insert_datetime
 * @property string $insert_by
 */
class StatusPengajuanHistory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_status_pengajuan_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pengajuan_id, status_id, insert_datetime, insert_by', 'required'),
			array('pengajuan_id, status_id', 'numerical', 'integerOnly'=>true),
			array('insert_by', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pengajuan_id, status_id, insert_datetime, insert_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'pengajuan'=>array(self::BELONGS_TO, 'Pengajuan', 'pengajuan_id'),
		'status'=>array(self::BELONGS_TO, 'StatusPengajuan', 'status_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pengajuan_id' => 'Nomor Pengajuan',
			'status_id' => 'Status',
			'insert_datetime' => 'Insert Datetime',
			'insert_by' => 'Insert By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pengajuan_id',$this->pengajuan_id);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('insert_datetime',$this->insert_datetime,true);
		$criteria->compare('insert_by',$this->insert_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StatusPengajuanHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	*
	*/
	public function primaryKey(){
		return array('pengajuan_id', 'status_id');
	}
}