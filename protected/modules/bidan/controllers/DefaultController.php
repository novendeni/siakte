<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		//$this->layout = '/layouts/sidebar';
		//$this->render('index');
            $this->redirect(array('pengajuan/list'));
	}
	
	/**
	*
	*/
	public function actionFindBidan(){
		$q = new CDbCriteria();
		$request = trim($_GET['term']);
		if($request != ''){
			$q->addSearchCondition('nama', $request);
			$model = Bidan::model()->findAll($q);
			
			$data = array();
                        if (!empty($model)){
                            foreach($model as $get){
                                    $data[] = array(
                                            'nik'=>$get->nik,
                                            'nama'=>$get->nama,
                                            'alamat'=>$get->alamat
                                    );
                            }
                            //echo CJSON::encode($data);
                        } else {
                            $data[] = array(
                                'nik'=>'0',
                                'nama'=>'Data tidak ditemukan,',
                                'alamat'=>'periksa kembali kata kunci yang diinputkan'
                            );
                        }
                        echo CJSON::encode($data);
		}
	}
	
	/**
	*
	*/
	public function actionFindKK(){
		
		if(isset($_POST['KandidatBayi']['no_kk'])){
			$nokk = $_POST['KandidatBayi']['no_kk'];
			$model = DataKeluarga::model()->findByPK($nokk);
			if($model != null){
				$ayah = BiodataWni::model()->findByAttributes(array('no_kk'=>$nokk,'nama_lgkp'=>$model->nama_kep));
				echo json_encode($ayah->attributes);
			} else {
				echo 'data not found';
			}
		} else
			echo 'no data sent';
	}
	
	/**
	*
	*/
	public function actionFindNama(){
		if(isset($_POST['nik'])){
			$nik = $_POST['nik'];
			$model = BiodataWni::model()->findByPK($nik);

			echo $model->nama_lgkp;
		} else{
			echo 'no data send';
		}
	}
	
	/**
	*
	*/
	public function actionDaftar(){
		$this->layout = '/layouts/column2';
		
		$pengajuan = new Pengajuan;
		$bayi = new KandidatBayi;
		$pelapor = new DetailPelapor;
                
		if(isset($_POST['Pengajuan'], $_POST['KandidatBayi'])){ 
                    $pengajuan->ad_id = 1;
                    $pengajuan->nik = Yii::app()->user->nik;
                    $pengajuan->umur_kehamilan = $_POST['Pengajuan']['umur_kehamilan'];
                    $pengajuan->tanggal_pengajuan = date('Y-m-d');
                    $pengajuan->insert_datetime = date('Y-m-d');
                    $pengajuan->insert_by = Yii::app()->user->name;
                    $pengajuan->update_datetime = NULL;
                    $pengajuan->update_by = NULL;
                    if(isset($_POST['persyaratan'])){
                        if (count($_POST['persyaratan']) == 3)
                            $pengajuan->status_id = 2;
                        else
                            $pengajuan->status_id = 1;
                    } else {
                        $pengajuan->status_id = 1;
                    }
			
                    if($pengajuan->save()){
                    // status history object, fill with data and save
                        $stathistory = new StatusPengajuanHistory;
                        $stathistory->pengajuan_id = $pengajuan->pengajuan_id;
                        $stathistory->status_id = $pengajuan->status_id;
                        $stathistory->insert_by = $pengajuan->insert_by;
                        $stathistory->insert_datetime = $pengajuan->tanggal_pengajuan;
                        $stathistory->save(false);
                
                        //checklist persyaratan pengajuan, fill with data and save
                        $cpp = new ChecklistPersyaratanPengajuan;
                        $cpp->pengajuan_id = $pengajuan->pengajuan_id;
                        $cpp->pa_id = 1;
                        $cpp->kuantitas = 2;
                        if ($pengajuan->status_id == 2){
                            $cpp->isQualified = 1;
                        } else {
                            $cpp->isQualified = 0;
                        }
                        $cpp->save();
                    
                        $bayi->pengajuan_id = $pengajuan->pengajuan_id;
                        $bayi->attributes = $_POST['KandidatBayi'];
                        $bayi->nik_bayi = null;
                        $bayi->sk_id = 1;
                        $bayi->insert_datetime = date('Y-m-d');
                        $bayi->insert_by = Yii::app()->user->name;
                        if ($bayi->save()){		
                            $kandidatHist = new StatusKandidatHistory;
                            $kandidatHist->kandidat_id = $bayi->kandidat_id;
                            $kandidatHist->sk_id = $bayi->sk_id;
                            $kandidatHist->insert_datetime = $bayi->insert_datetime;
                            $kandidatHist->insert_by = $bayi->insert_by;
                            $kandidatHist->save(false);
                    
                            $pelapor->kandidat_id = $bayi->kandidat_id;
                            $pelapor->nik = $bayi->nik_pelapor;
                            $pelapor->tanggal_lapor = date('Y-m-d');
                            $pelapor->hubungan_keluarga = $_POST['DetailPelapor']['hubungan_keluarga'];
                            $pelapor->save();
				
                            Yii::app()->user->setFlash('success','Data Telah terkirim, Silahkan kunjungi bidan anda untuk informasi lebih lanjut. Terima Kasih');
                        }
                    }
		}
		$this->render('daftar_new',array('pengajuan'=>$pengajuan,'bayi'=>$bayi,'pelapor'=>$pelapor));
	}
        
        public function actionTest(){
            $this->layout = '/layouts/sidebar';
            $this->render('daftar_new');
        }
}