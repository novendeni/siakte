<?php

class PengajuanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','approve','list','lahir','edit','daftar'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Pengajuan;
		$stathistory = new StatusPengajuanHistory;
		$cpp = new ChecklistPersyaratanPengajuan;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pengajuan']))
		{
			$model->attributes=$_POST['Pengajuan'];
			$model->tanggal_pengajuan = date('Y-m-d');
			$model->insert_by = Yii::app()->user->name;
			$model->insert_datetime = date('Y-m-d');
			
			// status history object, fill with data and save
			$stathistory->status_id = $model->status_id;
			$stathistory->insert_by = $model->insert_by;
			$stathistory->insert_datetime = $model->tanggal_pengajuan;
			
			//checklist persyaratan pengajuan, fill with data and save
			
			$cpp->pa_id = 1;
			$cpp->kuantitas = 2;
			if ($model->status_id == 1){
				$cpp->isQualified = 1;
			} else {
				$cpp->isQualified = 0;
			}
					
			if($model->save()){
				//$this->redirect(array('view','id'=>$model->pengajuan_id));
				
				$stathistory->pengajuan_id = $model->pengajuan_id;
				$cpp->pengajuan_id = $model->pengajuan_id;
				$stathistory->save(false);
				$cpp->save();
				
				//redirect sesuai jenis pengajuan
				if ($model->ad_id == 1){
					$this->redirect(array('/bidan/KandidatBayi/create', 'id_pengajuan'=>$model->pengajuan_id));
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pengajuan']))
		{
			$model->attributes=$_POST['Pengajuan'];
			if($model->save()){
				//update table status pengajuan history
				$stathistory = new StatusPengajuanHistory;
				$stathistory->pa_id = $model->pa_id;
				$stathistory->status_id = $model->status_id;
				$stathistory->insert_datetime = $model->update_datetime;
				$stathistory->save(false);
			}
			$this->redirect(array('view','id'=>$model->pengajuan_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Pengajuan');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Pengajuan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pengajuan']))
			$model->attributes=$_GET['Pengajuan'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Pengajuan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Pengajuan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Pengajuan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pengajuan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
        /**
         * list all pengajuan 
         */
        public function actionList(){
            $this->layout = '/layouts/sidebar';
            
            $nik = Yii::app()->user->nik;
            $models = array();
            
            $criteria = new CDbCriteria;
            $criteria->addSearchCondition('nik',$nik);
            
            //pagination
            $count = Pengajuan::model()->count($criteria); 
            $pages = new CPagination($count);
            // results per page
            $pages->pageSize=20;
            $pages->applyLimit($criteria);
            
            $models = Pengajuan::model()->findAll($criteria);
            
            $this->render('list',array('models'=>$models, 'pages'=>$pages));
        }
	/**
	*
	*/
	public function actionApprove(){
		
		$nik = Yii::app()->user->nik;
		
		$criteria = new CDbCriteria;
		$criteria->addSearchCondition('nik',$nik);
		
		$count = Pengajuan::model()->count($criteria);
		$pages = new CPagination($count);
		// results per page
		$pages->pageSize=10;
		$pages->applyLimit($criteria);
		
		$models = Pengajuan::model()->findAll($criteria);
		
		$this->render('approve',array('models'=>$models,'pages'=>$pages));
	}
    /*
	*
	*/
	public function actionEdit($id){
		$pengajuan = Pengajuan::model()->findByPK($id);
                $cpp = ChecklistPersyaratanPengajuan::model()->findByAttributes(array('pengajuan_id'=>$pengajuan->pengajuan_id,'pa_id'=>1));
                $stathistory = StatusPengajuanHistory::model()->findByAttributes(array('pengajuan_id'=>$pengajuan->pengajuan_id,'status_id'=>$pengajuan->status_id));
		$bayi = KandidatBayi::model()->findByAttributes(array('pengajuan_id'=>$pengajuan->pengajuan_id));
		$kandidatHist = StatusKandidatHistory::model()->findByAttributes(array('kandidat_id'=>$bayi->kandidat_id,'sk_id'=>$bayi->sk_id));
                $pelapor = DetailPelapor::model()->findByAttributes(array('kandidat_id'=>$bayi->kandidat_id));
		
		if(isset($_POST['Pengajuan'], $_POST['KandidatBayi'])){ 
            $pengajuan->ad_id = 1;
			$pengajuan->nik = Yii::app()->user->nik;
			$pengajuan->umur_kehamilan = $_POST['Pengajuan']['umur_kehamilan'];
			$pengajuan->tanggal_pengajuan = date('Y-m-d');
			$pengajuan->insert_datetime = date('Y-m-d');
			$pengajuan->insert_by = Yii::app()->user->name;
			$pengajuan->update_datetime = NULL;
			$pengajuan->update_by = NULL;
			if(isset($_POST['persyaratan'])){
				if (count($_POST['persyaratan']) == 3)
				$pengajuan->status_id = 2;
                else
					$pengajuan->status_id = 1;
            } else {
				$pengajuan->status_id = 1;
			}
                        //var_dump($pengajuan->attributes);
			$pengajuan->save();
                        
            // status history object, fill with data and save
            $stathistory->pengajuan_id = $pengajuan->pengajuan_id;
			$stathistory->status_id = $pengajuan->status_id;
			$stathistory->insert_by = $pengajuan->insert_by;
			$stathistory->insert_datetime = $pengajuan->tanggal_pengajuan;
			$stathistory->save(false);
					
			//checklist persyaratan pengajuan, fill with data and save
			
            $cpp->pengajuan_id = $pengajuan->pengajuan_id;
			$cpp->pa_id = 1;
			$cpp->kuantitas = 2;
			if ($pengajuan->status_id == 2){
				$cpp->isQualified = 1;
			} else {
				$cpp->isQualified = 0;
			}
            $cpp->save();
			
			$bayi->pengajuan_id = $pengajuan->pengajuan_id;
			$bayi->attributes = $_POST['KandidatBayi'];
			$bayi->nik_bayi = null;
            $bayi->sk_id = 1;
            if(strlen($bayi->keterangan) > 5){
				$bayi->approve = 1;
			}
			$bayi->insert_datetime = date('Y-m-d');
			$bayi->insert_by = Yii::app()->user->name;
			$bayi->save();
                        
            $kandidatHist->kandidat_id = $bayi->kandidat_id;
			$kandidatHist->sk_id = $bayi->sk_id;
			$kandidatHist->insert_datetime = $bayi->insert_datetime;
			$kandidatHist->insert_by = $bayi->insert_by;
            $kandidatHist->save(false);
			
			$pelapor->kandidat_id = $bayi->kandidat_id;
			$pelapor->nik = $bayi->nik_pelapor;
			$pelapor->tanggal_lapor = date('Y-m-d');
			$pelapor->hubungan_keluarga = $_POST['DetailPelapor']['hubungan_keluarga'];
			$pelapor->save();
			
			Yii::app()->user->setFlash('success','Data Telah terkirim, Silahkan kunjungi bidan anda untuk informasi lebih lanjut. Terima Kasih');
		}
		$this->render('edit',array('pengajuan'=>$pengajuan,'bayi'=>$bayi,'pelapor'=>$pelapor));
	}
        
        /**
	*
	*/
	public function actionDaftar(){
		$this->layout = '/layouts/sidebar';
		
		$pengajuan = new Pengajuan;
		$bayi = new KandidatBayi;
		$pelapor = new DetailPelapor;
                
		if(isset($_POST['Pengajuan'], $_POST['KandidatBayi'])){ 
                    $pengajuan->ad_id = 1;
                    $pengajuan->nik = Yii::app()->user->nik;
                    $pengajuan->umur_kehamilan = $_POST['Pengajuan']['umur_kehamilan'];
                    $pengajuan->tanggal_pengajuan = date('Y-m-d');
                    $pengajuan->insert_datetime = date('Y-m-d');
                    $pengajuan->insert_by = Yii::app()->user->name;
                    $pengajuan->update_datetime = NULL;
                    $pengajuan->update_by = NULL;
                    if(isset($_POST['persyaratan'])){
                        if (count($_POST['persyaratan']) == 3)
                            $pengajuan->status_id = 2;
                        else
                            $pengajuan->status_id = 1;
                    } else {
                        $pengajuan->status_id = 1;
                    }
			
                    if($pengajuan->save()){
                    // status history object, fill with data and save
                        $stathistory = new StatusPengajuanHistory;
                        $stathistory->pengajuan_id = $pengajuan->pengajuan_id;
                        $stathistory->status_id = $pengajuan->status_id;
                        $stathistory->insert_by = $pengajuan->insert_by;
                        $stathistory->insert_datetime = $pengajuan->tanggal_pengajuan;
                        $stathistory->save(false);
                
                        //checklist persyaratan pengajuan, fill with data and save
                        $cpp = new ChecklistPersyaratanPengajuan;
                        $cpp->pengajuan_id = $pengajuan->pengajuan_id;
                        $cpp->pa_id = 1;
                        $cpp->kuantitas = 2;
                        if ($pengajuan->status_id == 2){
                            $cpp->isQualified = 1;
                        } else {
                            $cpp->isQualified = 0;
                        }
                        $cpp->save();
                    
                        $bayi->pengajuan_id = $pengajuan->pengajuan_id;
                        $bayi->attributes = $_POST['KandidatBayi'];
                        $bayi->nik_bayi = null;
                        $bayi->sk_id = 1;
                        $bayi->insert_datetime = date('Y-m-d');
                        $bayi->insert_by = Yii::app()->user->name;
                        if ($bayi->save()){		
                            $kandidatHist = new StatusKandidatHistory;
                            $kandidatHist->kandidat_id = $bayi->kandidat_id;
                            $kandidatHist->sk_id = $bayi->sk_id;
                            $kandidatHist->insert_datetime = $bayi->insert_datetime;
                            $kandidatHist->insert_by = $bayi->insert_by;
                            $kandidatHist->save(false);
                    
                            $pelapor->kandidat_id = $bayi->kandidat_id;
                            $pelapor->nik = $bayi->nik_pelapor;
                            $pelapor->tanggal_lapor = date('Y-m-d');
                            $pelapor->hubungan_keluarga = $_POST['DetailPelapor']['hubungan_keluarga'];
                            $pelapor->save();
				
                            Yii::app()->user->setFlash('success','Data Telah terkirim, Silahkan kunjungi bidan anda untuk informasi lebih lanjut. Terima Kasih');
                        }
                    }
		}
		$this->render('daftar_new',array('pengajuan'=>$pengajuan,'bayi'=>$bayi,'pelapor'=>$pelapor));
	}
}