<?php
/* @var $this DetailPelaporController */
/* @var $model DetailPelapor */

$this->breadcrumbs=array(
	'Detail Pelapors'=>array('index'),
	$model->kandidat_id,
);

$this->menu=array(
	array('label'=>'List DetailPelapor', 'url'=>array('index')),
	array('label'=>'Create DetailPelapor', 'url'=>array('create')),
	array('label'=>'Update DetailPelapor', 'url'=>array('update', 'id'=>$model->kandidat_id)),
	array('label'=>'Delete DetailPelapor', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->kandidat_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DetailPelapor', 'url'=>array('admin')),
);
?>

<h1>View DetailPelapor #<?php echo $model->kandidat_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'kandidat_id',
		'tanggal_lapor',
		'hubungan_keluarga',
		'nik',
		'insert_datetime',
		'insert_by',
		'update_datetime',
		'update_by',
	),
)); ?>
