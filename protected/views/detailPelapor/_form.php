<?php
/* @var $this DetailPelaporController */
/* @var $model DetailPelapor */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'detail-pelapor-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'kandidat_id'); ?>
		<?php echo $form->textField($model,'kandidat_id'); ?>
		<?php echo $form->error($model,'kandidat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tanggal_lapor'); ?>
		<?php echo $form->textField($model,'tanggal_lapor'); ?>
		<?php echo $form->error($model,'tanggal_lapor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hubungan_keluarga'); ?>
		<?php echo $form->textField($model,'hubungan_keluarga',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'hubungan_keluarga'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nik'); ?>
		<?php echo $form->textField($model,'nik'); ?>
		<?php echo $form->error($model,'nik'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'insert_datetime'); ?>
		<?php echo $form->textField($model,'insert_datetime'); ?>
		<?php echo $form->error($model,'insert_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'insert_by'); ?>
		<?php echo $form->textField($model,'insert_by',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'insert_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_datetime'); ?>
		<?php echo $form->textField($model,'update_datetime'); ?>
		<?php echo $form->error($model,'update_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_by'); ?>
		<?php echo $form->textField($model,'update_by',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'update_by'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->