<?php
/* @var $this DetailPelaporController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Detail Pelapors',
);

$this->menu=array(
	array('label'=>'Create DetailPelapor', 'url'=>array('create')),
	array('label'=>'Manage DetailPelapor', 'url'=>array('admin')),
);
?>

<h1>Detail Pelapors</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
