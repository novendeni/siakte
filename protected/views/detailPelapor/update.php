<?php
/* @var $this DetailPelaporController */
/* @var $model DetailPelapor */

$this->breadcrumbs=array(
	'Detail Pelapors'=>array('index'),
	$model->kandidat_id=>array('view','id'=>$model->kandidat_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DetailPelapor', 'url'=>array('index')),
	array('label'=>'Create DetailPelapor', 'url'=>array('create')),
	array('label'=>'View DetailPelapor', 'url'=>array('view', 'id'=>$model->kandidat_id)),
	array('label'=>'Manage DetailPelapor', 'url'=>array('admin')),
);
?>

<h1>Update DetailPelapor <?php echo $model->kandidat_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>