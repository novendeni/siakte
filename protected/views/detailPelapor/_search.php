<?php
/* @var $this DetailPelaporController */
/* @var $model DetailPelapor */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'kandidat_id'); ?>
		<?php echo $form->textField($model,'kandidat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tanggal_lapor'); ?>
		<?php echo $form->textField($model,'tanggal_lapor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hubungan_keluarga'); ?>
		<?php echo $form->textField($model,'hubungan_keluarga',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nik'); ?>
		<?php echo $form->textField($model,'nik'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'insert_datetime'); ?>
		<?php echo $form->textField($model,'insert_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'insert_by'); ?>
		<?php echo $form->textField($model,'insert_by',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'update_datetime'); ?>
		<?php echo $form->textField($model,'update_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'update_by'); ?>
		<?php echo $form->textField($model,'update_by',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->