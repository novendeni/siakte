<?php
/* @var $this DetailPelaporController */
/* @var $data DetailPelapor */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kandidat_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kandidat_id), array('view', 'id'=>$data->kandidat_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_lapor')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_lapor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hubungan_keluarga')); ?>:</b>
	<?php echo CHtml::encode($data->hubungan_keluarga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nik')); ?>:</b>
	<?php echo CHtml::encode($data->nik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insert_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->insert_datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insert_by')); ?>:</b>
	<?php echo CHtml::encode($data->insert_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->update_datetime); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('update_by')); ?>:</b>
	<?php echo CHtml::encode($data->update_by); ?>
	<br />

	*/ ?>

</div>