<?php
/* @var $this DetailPelaporController */
/* @var $model DetailPelapor */

$this->breadcrumbs=array(
	'Detail Pelapors'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DetailPelapor', 'url'=>array('index')),
	array('label'=>'Manage DetailPelapor', 'url'=>array('admin')),
);
?>

<h1>Create DetailPelapor</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>