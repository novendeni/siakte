<?php
/* @var $this DetailKelahiranController */
/* @var $model DetailKelahiran */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout'=>TbHtml::FORM_LAYOUT_HORIZONTAL,
	'id'=>'detail-kelahiran-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Kolom dengan tanda<span class="required">*</span> WAJIB diisi !</p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldControlGroup($model,'kandidat_id',array('disabled' => true, 'span'=>3)); ?>

            <?php echo $form->textFieldControlGroup($model,'nama',array('span'=>3,'maxlength'=>40)); ?>

            <?php //echo $form->textFieldControlGroup($model,'tanggal_lahir',array('span'=>5)); 
				echo $form->dateFieldControlGroup($model,'tanggal_lahir',array());
			?>

            <?php //echo $form->textFieldControlGroup($model,'waktu_lahir',array('span'=>5,'maxlength'=>8)); 
				$timePicker = $this->widget('yiiwheels.widgets.timepicker.WhTimePicker',array(
					'model'=>$model,
					'name' => 'DetailKelahiran[waktu_lahir]',
					'attribute'=>'waktu_lahir',
					'pluginOptions'=>array(
						'minuteStep'=>1,
						'showMeridian'=>false,
						'value'=>'waktu_lahir',
					),
				),true);
				echo TbHtml::customActiveControlGroup($timePicker, $model, 'waktu_lahir');
				
				/*
				echo TbHtml::labelTb('Waktu Lahir');
				echo TbHtml::dropDownList('jam', '', array(
					'00'=>'00', '01'=>'01', '02'=>'02', '03'=>'03', '04'=>'04', '05'=>'05',
					'06'=>'06', '07'=>'07', '08'=>'08', '09'=>'09', '10'=>'10',	'11'=>'11', 
					'12'=>'12', '13'=>'13', '14'=>'14', '15'=>'15', '16'=>'16', '17'=>'17', 
					'18'=>'18', '19'=>'19', '20'=>'20', '21'=>'21', '22'=>'22', '23'=>'23',
				));
				echo TbHtml::dropDownList('jam', '', array(
					'00'=>'00', '01'=>'01', '02'=>'02', '03'=>'03', '04'=>'04', '05'=>'05',
				'06'=>'06', '07'=>'07', '08'=>'08', '09'=>'09', '10'=>'10',	'11'=>'11', 
				'12'=>'12', '13'=>'13', '14'=>'14', '15'=>'15', '16'=>'16', '17'=>'17', 
				'18'=>'18', '19'=>'19', '20'=>'20', '21'=>'21', '22'=>'22', '23'=>'23',
				'24'=>'24', '25'=>'25', '26'=>'26', '27'=>'27', '28'=>'28', '29'=>'29',
				'30'=>'30', '31'=>'31', '32'=>'32', '33'=>'33', '34'=>'34', '35'=>'35', 
				'36'=>'36', '37'=>'37', '38'=>'38', '39'=>'39', '40'=>'40', '41'=>'41',
				'42'=>'42', '43'=>'43', '44'=>'44', '45'=>'45', '46'=>'46', '47'=>'47', 
				'48'=>'48', '49'=>'49', '50'=>'50', '51'=>'51', '52'=>'52', '53'=>'53',
				'54'=>'54', '55'=>'55', '56'=>'56', '57'=>'57', '58'=>'58', '59'=>'59'
				));*/
			?>

            <?php echo $form->dropDownListControlGroup($model,'jenis_kelamin',array('L'=>'Laki-Laki', 'P'=>'Perempuan')); ?>

            <?php echo $form->dropDownListControlGroup($model,'jenis_kelahiran',array('normal'=>'Normal', 'prematur'=>'Prematur')); ?>

            <?php echo $form->textFieldControlGroup($model,'tempat_lahir',array('span'=>3,'maxlength'=>40)); ?>

            <?php echo $form->textFieldControlGroup($model,'anak_ke',array('span'=>3)); ?>

            <?php echo $form->textFieldControlGroup($model,'berat',array('span'=>3)); ?>

            <?php echo $form->textFieldControlGroup($model,'panjang',array('span'=>3)); ?>

            <?php echo $form->dropDownListControlGroup($model,'penolong_kelahiran',array('Bidan'=>'Bidan', 'Dokter'=>'Dokter')); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Perbaharui',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    //'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->