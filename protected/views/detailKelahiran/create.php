<?php
/* @var $this DetailKelahiranController */
/* @var $model DetailKelahiran */
?>

<?php
$this->breadcrumbs=array(
	'Detail Kelahirans'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DetailKelahiran', 'url'=>array('index')),
	array('label'=>'Manage DetailKelahiran', 'url'=>array('admin')),
);
?>

<h1 align='center'>Form Detail Kelahiran</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>