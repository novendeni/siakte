<?php
/* @var $this DetailKelahiranController */
/* @var $model DetailKelahiran */


$this->breadcrumbs=array(
	'Detail Kelahirans'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List DetailKelahiran', 'url'=>array('index')),
	array('label'=>'Create DetailKelahiran', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#detail-kelahiran-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Detail Kelahirans</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'detail-kelahiran-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'kandidat_id',
		'nama',
		'tanggal_lahir',
		'waktu_lahir',
		'jenis_kelamin',
		'jenis_kelahiran',
		/*
		'tempat_lahir',
		'anak_ke',
		'berat',
		'panjang',
		'penolong_kelahiran',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>