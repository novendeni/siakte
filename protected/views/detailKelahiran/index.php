<?php
/* @var $this DetailKelahiranController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Detail Kelahirans',
);

$this->menu=array(
	array('label'=>'Create DetailKelahiran','url'=>array('create')),
	array('label'=>'Manage DetailKelahiran','url'=>array('admin')),
);
?>

<h1>Detail Kelahirans</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>