<?php
/* @var $this DetailKelahiranController */
/* @var $model DetailKelahiran */
?>

<?php
$this->breadcrumbs=array(
	'Detail Kelahirans'=>array('index'),
	$model->kandidat_id=>array('view','id'=>$model->kandidat_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DetailKelahiran', 'url'=>array('index')),
	array('label'=>'Create DetailKelahiran', 'url'=>array('create')),
	array('label'=>'View DetailKelahiran', 'url'=>array('view', 'id'=>$model->kandidat_id)),
	array('label'=>'Manage DetailKelahiran', 'url'=>array('admin')),
);
?>

    <h1>Update DetailKelahiran <?php echo $model->kandidat_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>