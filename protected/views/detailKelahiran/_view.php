<?php
/* @var $this DetailKelahiranController */
/* @var $data DetailKelahiran */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('kandidat_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kandidat_id),array('view','id'=>$data->kandidat_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('waktu_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->waktu_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_kelamin')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_kelamin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_kelahiran')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_kelahiran); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tempat_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tempat_lahir); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('anak_ke')); ?>:</b>
	<?php echo CHtml::encode($data->anak_ke); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('berat')); ?>:</b>
	<?php echo CHtml::encode($data->berat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('panjang')); ?>:</b>
	<?php echo CHtml::encode($data->panjang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penolong_kelahiran')); ?>:</b>
	<?php echo CHtml::encode($data->penolong_kelahiran); ?>
	<br />

	*/ ?>

</div>