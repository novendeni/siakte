<?php $this->beginContent('//layouts/main_new');?>	
	<!----------------------[ Main Body ]------------------------------------------------------------------------------------------------------>
	<div class="main1"></div>
	<div class="main2">
		<div class="container"> 
			<div class="hero">
				<a class="btn nav-toggle collapsed" data-toggle="collapse" data-target="#tes"><br/>
					<i class="fa fa-reorder"></i> Menu Utama 
				</a>
				<div class="row">
					<div class="col-md-3 hero-left col-sm-3">
						
						<ul class="list-menu nav-list collapse navbar-collapse" id="tes">
							<li class="main-header hidden-xs">Menu Utama</li>
							<li><a href="#">Beranda</a></li>
							<li><a href="#">Profil</a></li>
							<li><a href="#">Produk</a></li>
							<li><a href="#">Kantor Regional</a></li>
							<li><a href="#">Berita</a></li>
							<li><a href="#">Video</a></li>
							<li><a href="#">Regulasi</a></li>
							<li><a href="#">SJDI</a></li>
							<li><a href="#">Publikasi</a></li>
							<li><a href="#">LPID</a></li>
							<li><a href="#">Blog Kepegawaian</a></li>
							<li><a href="#">Portal Lama</a></li>
							<li><a href="#">Hubungi Kami</a></li>
						</ul>
					</div>
					
					<div class="col-md-9 hero-right col-sm-9">
						<!----------------------[ Main Slider ]---------------------------->
						<div class="slideshow"> 
							<div class="frame active">
								<img height="418" width="780" class="floor" src="<?php echo Yii::app()->request->baseUrl;?>/images/image_1.jpg"/>
								<p>Pellentesque habitant morbi tristique</p>
							</div>
							<div class="frame">
								<img height="418" width="780" class="floor" src="<?php echo Yii::app()->request->baseUrl;?>/images/image_2.jpg"/>
							</div>
							<div class="frame">
								<img height="418" width="780" src="<?php echo Yii::app()->request->baseUrl;?>/images/image_3.jpg"/>
								<p>Donec gravida, ante vel ornare lacinia</p>
							</div>
                                                        <div class="frame">
								<img height="418" width="780" class="floor" src="<?php echo Yii::app()->request->baseUrl;?>/images/image_4.jpg"/>
							</div>
                                                        <div class="frame">
								<img height="418" width="780" class="floor" src="<?php echo Yii::app()->request->baseUrl;?>/images/image_5.jpg"/>
							</div>
                                                        <div class="frame">
								<img height="418" width="780" class="floor" src="<?php echo Yii::app()->request->baseUrl;?>/images/image_6.jpg"/>
							</div>
                                                        <div class="frame">
								<img height="418" width="780" class="floor" src="<?php echo Yii::app()->request->baseUrl;?>/images/image_7.jpg"/>
							</div>
                                                        <div class="frame">
								<img height="418" width="780" class="floor" src="<?php echo Yii::app()->request->baseUrl;?>/images/image_8.jpg"/>
							</div>
                                                        <div class="frame">
								<img height="418" width="780" class="floor" src="<?php echo Yii::app()->request->baseUrl;?>/images/img2.jpg"/>
							</div>
						</div>
						
						<!----------------------[ Thumbnail Slider ]---------------------------->
						<div class="wrapper">
							<div class="thumbnail-slider">
								<div class="wrapper">
									<div class="item">
										<img src="<?php echo Yii::app()->request->baseUrl;?>/images/image_1.jpg" />
										<a href="#" class="detail">
											<p>Quisque malesuada nulla sed pede volutpat pulvinar.</p>
											<i class="fa fa-search"></i>
											<span>Read More</span>
										</a>
									</div>
									<div class="item">
										<img src="<?php echo Yii::app()->request->baseUrl;?>/images/image_2.jpg" />
										<a href="#" class="detail">
											<p>Quisque malesuada nulla sed pede volutpat pulvinar.</p>
											<i class="fa fa-search"></i>
											<span>Read More</span>
										</a>
									</div>
									<div class="item">
										<img src="<?php echo Yii::app()->request->baseUrl;?>/images/image_3.jpg" />
										<a href="#" class="detail">
											<p>Quisque malesuada nulla sed pede volutpat pulvinar.</p>
											<i class="fa fa-search"></i>
											<span>Read More</span>
										</a>
									</div>
									<div class="item">
										<img src="<?php echo Yii::app()->request->baseUrl;?>/images/image_4.jpg" />
										<a href="#" class="detail">
											<p>Quisque malesuada nulla sed pede volutpat pulvinar.</p>
											<i class="fa fa-search"></i>
											<span>Read More</span>
										</a>
									</div>
									<div class="item">
										<img src="<?php echo Yii::app()->request->baseUrl;?>/images/image_5.jpg" />
										<a href="#" class="detail">
											<p>Quisque malesuada nulla sed pede volutpat pulvinar.</p>
											<i class="fa fa-search"></i>
											<span>Read More</span>
										</a>
									</div>
									<div class="item">
										<img src="<?php echo Yii::app()->request->baseUrl;?>/images/image_6.jpg"></i>
										<a href="#" class="detail">
											<p>Quisque malesuada nulla sed pede volutpat pulvinar.</p>
											<i class="fa fa-search"></i>
											<span>Read More</span>
										</a>
										</a>
									</div>
									<div class="item">
										<img src="<?php echo Yii::app()->request->baseUrl;?>/images/img2.jpg" />
										<a href="#" class="detail">
											<p>Quisque malesuada nulla sed pede volutpat pulvinar.</p>
											<i class="fa fa-search"></i>
											<span>Read More</span>
										</a>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>	
			
			<!--
			<div class="section-title white center">
				<div class="title">Quisque Aliquam</div>
				<div class="line"></div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="entry"> 
						
						<div class="image">
							<a href="#"><img src="<?php //echo Yii::app()->request->baseUrl;?>/images/logo.png"/></a>
						</div>
						<div class="body">
							<div class="title"><a href="#">Vestibulum ante ipsum</a></div>
							<p>Aliquam velit dui, commodo quis, porttitor eget, convallis et, nisi. 
							Aliquam justo lectus, iaculis a, auctor sed. </p>
							<a class="more" href="#">Read More <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="entry"> 
						
						<div class="image">
							<a href="#"><img src="<?php //echo Yii::app()->request->baseUrl;?>/images/logo.png"/></a>
						</div>
						<div class="body">
							<div class="title"><a href="#">Proin lectus orci, venenatis pharetra</a></div>
							<p>In consectetuer, lorem eu lobortis egestas, velit odio imperdiet eros, sit amet 
							sagittis nunc mi ac neque.  </p>
							<a class="more" href="#">Read More <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="entry"> 
						
						<div class="image">
							<a href="#"><img src="<?php //echo Yii::app()->request->baseUrl;?>/images/logo.png"/></a>
						</div>
						<div class="body">
							<div class="title"><a href="#">Fusce venenatis ligula in ped</a></div>
							<p>Quisque malesuada nulla sed pede volutpat pulvinar. 
							Phasellus nisi metus, tempus sit amet, ultrices ac.</p>
							<a class="more" href="#">Read More <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div><br/><br/><br/><br/><br/><br/>
			</div>-->
		</div>
	</div>
<?php $this->endContent();?>