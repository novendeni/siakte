<?php
/* @var $this SiteController */

	$this->pageTitle=Yii::app()->name;
	$this->layout = '//layouts/column2';
?>

<!--
<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p>Congratulations! You have successfully created your Yii application.</p>

<p>You may change the content of this page by modifying the following two files:</p>
<ul>
	<li>View file: <code><?php echo __FILE__; ?></code></li>
	<li>Layout file: <code><?php echo $this->getLayoutFile('main'); ?></code></li>
</ul>

<p>For more details on how to further develop this application, please read
the <a href="http://www.yiiframework.com/doc/">documentation</a>.
Feel free to ask in the <a href="http://www.yiiframework.com/forum/">forum</a>,
should you have any questions.</p> -->

<h1 align="center">Selamat Datang di Website Disdukcapil</h1>
<br/>
<br/>

<h2 align="center">Cara Pendaftaran</h2>

<ol>
    <li>Klik menu Pendaftaran yang terdapat pada kolom sebelah kiri</li>
    <li>Ketikan nama bidan tempat anda akan melahirkan pada kolom "nama bidan", pada saat anda mengetikan nama bidan akan muncul pilihan bidan dengan nama beserta alamat nya, klik pada pilihan yang muncul.</li>
    <li>Masukan umur kehamilan anda dalam satuan Hari</li>
    <li>Pada kolom Nomor KK, ketikkan nomor Kartu Keluarga anda kemudian klik pada bagian luar kolom maka NIK ayah dan nama ayah akan terisi secara otomatis</li>
    <li>Ketikan NIK Ibu pada kolom NIK Ibu</li>
    <li>Pada saat anda memasukan Nomor KK maka NIK Pelapor akan otomatis terisi dengan NIK Ayah, apabila Pelapor bukan Ayah maka hapus nik yang terdapat pada kolom NIK Pelapor dengan NIK yang seharusnya.</li>
    <li>Ganti Pilihan Hubungan Pelapor dengan yang semestinya, jika tidak erdapat pada pilihan makan pilih "Lainnya"</li>
    <li>Masukkan NIK dari Saksi 1 dan Saksi 2 pada kolom NIK saksi</li>
    <li>Checklist pada persyaratan yang telah dipenuhi dengan melakukan klik pada kotak yang terdapat pada sisi kiri persyaratan</li>
    <li>Periksa kembali data yang dimasukkan, jika telah yakin benar maka tekan tombol Simpan.</li>
</ol>


<?php
	//$this->redirect('kandidatbayi/index');
       
?>