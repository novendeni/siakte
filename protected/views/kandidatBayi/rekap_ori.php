<?php
/* @var $this KandidatBayiController */
/* @var $dataProvider CActiveDataProvider */

?>

<h1 align='center'>Rekapitulasi Calon Bayi</h1>

<div class='filter-form'>
	<div class='wide-form'>
		<?php
			$form = $this->beginWidget('CActiveForm', array(
				'action'=>Yii::app()->createUrl($this->route),
				'method'=>'get',
			));
		?>
	
		<div class='row'>
			<?php
				echo $form->label($model,'Group By : ');
				$criteria = 'insert_by';
				$types = array('insert_by'=>'Petugas', 'a'=>'Nagari', 'b'=>'Kecamatan', 'nik_saksi1'=>'Saksi');
			?>
			
			<?php
				echo CHtml::radioButtonList('sk_id','', $types, array(
					'template'=>'{input}{label}',
					'separator'=>'',
					'labelOptions'=>array('style'=>'display: inline; margin-right: 10px; font-weight: normal;'),
					'onchange'=>"$criteria=$('.this).val()",
				));
			?>
		</div>
		
		<div class='row'>
			<?php echo $form->label($model,'Nama : '); ?>
			<?php
				echo $form->textField($model,$criteria,array(
					'onchange'=>CHtml::ajax(array(
						'success'=>"$('.filter-form form').submit()",
					))
				));
			?>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'kandidat-bayi-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		//'kandidat_id',
		//'pengajuan_id',
		//'no_kk',
		array(
			'name'=>'noKk',
			'type'=>'raw',
            'header'=>'KK',
            'value'=>'$data->noKk->nama_kep . "<br/>" . $data->no_kk',
        ),
		'nik_bayi',
		/*array(
			'name'=>'nikBayi',
			'type'=>'raw',
            'header'=>'Bayi',
            'value'=>'$data->nikBayi->nama_lgkp . "<br/>" . $data->nik_bayi',
        ),*/
		//'nik_ibu',
		array(
			'name'=>'nikIbu',
			'type'=>'raw',
            'header'=>'Ibu',
            'value'=>'$data->nikIbu->nama_lgkp . "<br/>" . $data->nik_ibu',
        ),
		//'nik_ayah',
		array(
			'name'=>'nikAyah',
			'type'=>'raw',
            'header'=>'Ayah',
            'value'=>'$data->nikAyah->nama_lgkp. "<br/>" . $data->nik_ayah',
        ),
		//'nik_pelapor',
		array(
			'name'=>'nikPelapor',
			'type'=>'raw',
            'header'=>'Pelapor',
            'value'=>'$data->nikPelapor->nama_lgkp. "<br/>" . $data->nik_pelapor',
        ),
		//'nik_saksi1',
		array(
			'name'=>'nikSaksi1',
			'type'=>'raw',
            'header'=>'Saksi 1',
            'value'=>'$data->nikSaksi1->nama_lgkp . "<br/>" . $data->nik_saksi1',
        ),
		//'nik_saksi2',
		array(
			'name'=>'nikSaksi2',
			'type'=>'raw',
            'header'=>'Saksi 2',
            'value'=>'$data->nikSaksi2->nama_lgkp . "<br/>" . $data->nik_saksi2',
        ),
		/*
		'sk_id',
		'insert_datetime',
		'insert_by',
		'update_datetime',
		'update_by',
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{review}{copy}',
			'buttons'=>array(
				'review'=>array(
					'label'=>'lihat data dan copy ke database',
					'imageUrl'=>Yii::app()->request->baseUrl.'/images/review.png',
					'url'=>'Yii::app()->createUrl("")',
				),
				'copy'=>array(
					'label'=>'copy ke database',
					'imageUrl'=>Yii::app()->request->baseUrl.'/images/tambah.jpg',
					'url'=>'Yii::app()->createUrl("")',
				),
			)
		), */
	),
)); ?>