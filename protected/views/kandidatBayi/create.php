<?php
/* @var $this KandidatBayiController */
/* @var $model KandidatBayi */
?>

<?php
$this->breadcrumbs=array(
	'Kandidat Bayis'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List KandidatBayi', 'url'=>array('index')),
	array('label'=>'Manage KandidatBayi', 'url'=>array('admin')),
);
?>

<h1 align='center'>Form Kandidat Bayi</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'modelPelapor'=>$modelPelapor)); ?>