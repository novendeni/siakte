<?php /* @var $this Controller */ ?>
<?php 
	$this->beginContent('//layouts/main'); 
	if(!isset($model)){ $model=new LoginForm; }
?>

<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-3">
				<div id="side-menu" class="list-group">
                                    <a class="list-group-item header" href="#login">
                                        <center>&nbsp;Menu</center>
                                    </a>
                                        <a class="list-group-item" href="<?php echo Yii::app()->createUrl('/site/daftar');?>">
						<i class="fa fa-bars"></i> &nbsp;Pendaftaran
					</a>
					<a class="list-group-item header" data-toggle="collapse" data-parent="#accordion" href="#login">
						<i class="fa fa-user"></i> &nbsp;Login
					</a>
					<div id="login" class="panel-collapse collapse in">
						<div class="list-group-item">
							<div class="form">
								<?php echo CHtml::beginForm(Yii::app()->createUrl('/site/login'),'POST');?>
									<div class="form-group">
										<?php echo CHtml::activeLabel($model,'username',array('label'=>'Username'));?>
										<?php echo CHtml::activeTextField($model,'username',array('class'=>'form-control','placeholder'=>'username'));?>
									</div>
									<div class="form-group">
										<?php echo CHtml::activeLabel($model,'password',array('label'=>'Password'));?>
										<?php echo CHtml::activePasswordField($model,'password',array('class'=>'form-control','placeholder'=>'Password'));?>
									</div>
									<div class="checkbox">
										<label>
											<?php echo CHtml::activeCheckBox($model,'rememberMe'); ?>
											<?php echo CHtml::activeLabel($model,'rememberMe'); ?>
										</label>
									</div>
									<?php echo CHtml::submitButton('Login',array('class'=>'btn btn-primary')); ?>
								<?php echo CHtml::endForm(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-9 col-sm-9 left">
				<?php echo $content; ?>
			</div>
		</div>
	</div>
</section>

<?php $this->endContent(); ?>