<?php /* @var $this Controller */ ?>

<?php 
	$activePage = $this->getAction()->controller->action->id;
?>

<?php if(!isset($model)){ $model=new LoginForm; }?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>
		<link href="<?php echo Yii::app()->request->baseUrl;?>/css/font-awesome.min.css" rel="stylesheet" >
		<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/datepicker.css" rel="stylesheet">
		<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/siakte.css" rel="stylesheet">
	</head>
	
	<body>
		<div id="wrapper">
			<header>
				<div id="line-header"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							Header
						</div>
					</div>
				</div>
			</header>
			
			<?php echo $content; ?>
			<!-- Content -->
			
			<footer>
				<div class="container">
					<div class="row">
						<div class="col-md-* col-sm-* no-padding">
							<center>&copy; Copyright 2014 - 2020 Disduk Capil. <br /> All Rights Reserved</center>
						</div>
					</div>
				</div>
			</footer>
		</div>
		
		<div class="device-xs visible-xs"></div>
		<div class="device-sm visible-sm"></div>
		<div class="device-md visible-md"></div>
		
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.10.2.min.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/siakte.js"></script>
	</body>
</html>
