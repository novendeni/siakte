<?php
    
    
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link href="<?php echo Yii::app()->request->baseUrl;?>/css/font-awesome.min.css" rel="stylesheet" >
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <link href="<?php echo Yii::app()->request->baseUrl;?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->request->baseUrl;?>/css/siakcapil_new.css" rel="stylesheet">
    </head>

    <body>
	<!---------------[ header ]---------------------------------------------------------------------------------------------->
	<header>
            <div class="container">
                <a href="index.php" class="logo">
                    <div class="image">
                        <img src="<?php echo Yii::app()->request->baseUrl;?>/images/logo.png" />
                    </div>
                    <div class="text">
                        <div class="title">ACE</div>
                        <div class="sub-title">Administrasi Cepat Elektronik</div>
                    </div>
                </a>
                <div class="right-group">
                    <div class="xrow">
                        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#LoginModal"><i class="fa fa-lock"></i> Login</button>
                        <span class="hidden-xs"><i class="fa fa-phone"></i> <span style="color: #ff8b00;">Call Center</span>  021-8093008</span>
                    </div>
                    <div class="xrow hidden-xs">
                        <span>
                            <a href="#"><span class="round-icon"><i class="fa fa-twitter"></i></span></a>&nbsp;
                            <a href="#"><span class="round-icon"><i class="fa fa-facebook"></i></span></a>
                        </span>
                        <form class="search-box-1">
                            <input type="text" placeholder="Search..." />
                            <button type="submit"><i class="fa fa-search"></i></button> 
                        </form>
                    </div>
                </div>
            </div>
	</header>
	
	<!----------------------[ Login Modal ]------------------------------------------------------------------------------------------------------>
	<div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <center><h4 class="modal-title"><i class="fa fa-sign-in"></i> Login</h4></center>
                    </div>
                    <div class="modal-body">
                        <form class="form-group login-form" action="<?php echo Yii::app()->createUrl('/site/login')?>" method="POST">
                            <div class="icon-input">
                                <i class="fa fa-user"></i>
                                <input class="form-control" type="text" placeholder="Username" name="LoginForm[username]"/>
                            </div>		
                            <div class="icon-input">
                                <input class="form-control" type="password" placeholder="Password" name="LoginForm[password]"/>
                                <i class="fa fa-lock"></i>
                            </div>
                            <br/>
                            <center><button type="submit" class="btn btn-primary">Log In</button></center>
                        </form>
                        <center><a class="forgot" href="#">Lupa Password?</a></center>
                    </div>
                    <div class="modal-footer">
                        <center><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></center>
                    </div>
                </div>
            </div>
	</div>
	
        <?php echo $content;?>
        
        <!--------------------[ Footer ]------------------------------------------------------------------------------------->
	<footer>
            <div class="container">
                <br/><br/><br/>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <h4><b>Menu</b></h4>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <ul class="footer-menu">
                                    <li><a href="#">Beranda</a></li>
                                    <li><a href="#">Profil</a></li>
                                    <li><a href="#">Produk</a></li>
                                    <li><a href="#">Kantor Regional</a></li>
                                    <li><a href="#">Berita</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <ul class="footer-menu">
                                    <li><a href="#">Video</a></li>
                                    <li><a href="#">Regulasi</a></li>
                                    <li><a href="#">SJDI</a></li>
                                    <li><a href="#">Publikasi</a></li>
                                    <li><a href="#">Blog Kepegawaian</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <ul class="footer-menu">
                                    <li><a href="#">LPID</a></li>
                                    <li><a href="#">Portal</a></li>
                                    <li><a href="#">Hubungi Kami</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 sep">
                        <h4><b>Call Center</b></h4>
                        <span class="round-icon2"><i class="fa fa-phone"></i></span><span>&nbsp;&nbsp;021-8093008</span>
                        <br/><br/>
                        <h4><b>Kantor Pusat</b></h4>
                        <p>Jl. Letjen Sutoyo No. 12 Telp 021-8093008 Jakarta Timur 13640</p>
                        <br/>
                    </div>
                    <div class="col-md-4 col-sm-4 sep">
                        <h4><b>Follow Kami</b></h4>
                        <span class="round-icon2"><i class="fa fa-facebook"></i></span>&nbsp;&nbsp;<a href="#">malesuada facebook</a><br/>
                        <span class="round-icon2"><i class="fa fa-twitter"></i></span>&nbsp;&nbsp;<a href="#">@olutpatpulvinar_twitter</a>
                        <br/><br/>
                        <h4><b>Subscribe</b></h4>
                        <form class="subscribe form-inline">
                            <input class="form-control input-sm" type="text" placeholder="Email" /><i class="fa fa-envelope-o"></i>
                            <button class="btn btn-black btn-sm" type="submit">Subscribe</button> <br/>
                            <span>Dapatkan Kabar Terbaru dari Kami</span>
                        </form>
                    </div>
                </div>
                <br/><br/><br/>
            </div>
            <div class="copyright">Hak cipta &copy; 2014 Dukcapil. Semua Hak Dilindungi</div>
	</footer>
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.10.2.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/bootstrap.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/siakcapil_new.js"></script>
    </body>
</html>