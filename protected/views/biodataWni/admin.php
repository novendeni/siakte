<?php
/* @var $this BiodataWniController */
/* @var $model BiodataWni */

$this->breadcrumbs=array(
	'Biodata Wnis'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List BiodataWni', 'url'=>array('index')),
	array('label'=>'Create BiodataWni', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#biodata-wni-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Biodata Wnis</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'biodata-wni-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nik',
		'no_ktp',
		'tmpt_sbl',
		'no_paspor',
		'tgl_akh_paspor',
		'nama_lgkp',
		/*
		'jenis_klmin',
		'tmpt_lhr',
		'tgl_lhr',
		'akta_lhr',
		'no_akta_lhr',
		'gol_drh',
		'agama',
		'stat_kwn',
		'akta_kwn',
		'no_akta_kwn',
		'tgl_kwn',
		'akta_crai',
		'no_akta_crai',
		'tgl_crai',
		'stat_hbkel',
		'klain_fsk',
		'pnydng_cct',
		'pddk_akh',
		'jenis_pkrjn',
		'nik_ibu',
		'nama_lgkp_ibu',
		'nik_ayah',
		'nama_lgkp_ayah',
		'nama_ket_rt',
		'nama_ket_rw',
		'nama_pet_reg',
		'nip_pet_reg',
		'nama_pet_entri',
		'nip_pet_entri',
		'tgl_entri',
		'no_kk',
		'jenis_bntu',
		'no_prop',
		'no_kab',
		'no_kec',
		'no_kel',
		'stat_hidup',
		'tgl_ubah',
		'tgl_cetak_ktp',
		'tgl_ganti_ktp',
		'tgl_pjg_ktp',
		'stat_ktp',
		'als_numpang',
		'pflag',
		'cflag',
		'sync_flag',
		'ket_agama',
		'kebangsaan',
		'gelar',
		'ket_pkrjn',
		'glr_agama',
		'glr_akademis',
		'glr_bangsawan',
		'is_pros_datang',
		'desc_pekerjaan',
		'desc_kepercayaan',
		'flag_status',
		'count_ktp',
		'count_biodata',
		'flagsink',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
