<?php
/* @var $this BiodataWniController */
/* @var $model BiodataWni */

$this->breadcrumbs=array(
	'Biodata Wnis'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BiodataWni', 'url'=>array('index')),
	array('label'=>'Manage BiodataWni', 'url'=>array('admin')),
);
?>

<h1>Create BiodataWni</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>