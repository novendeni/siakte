<?php
/* @var $this BiodataWniController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Biodata Wnis',
);

$this->menu=array(
	array('label'=>'Create BiodataWni', 'url'=>array('create')),
	array('label'=>'Manage BiodataWni', 'url'=>array('admin')),
);
?>

<h1>Biodata Wnis</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
