<?php
/* @var $this BiodataWniController */
/* @var $model BiodataWni */

$this->breadcrumbs=array(
	'Biodata Wnis'=>array('index'),
	$model->nik=>array('view','id'=>$model->nik),
	'Update',
);

$this->menu=array(
	array('label'=>'List BiodataWni', 'url'=>array('index')),
	array('label'=>'Create BiodataWni', 'url'=>array('create')),
	array('label'=>'View BiodataWni', 'url'=>array('view', 'id'=>$model->nik)),
	array('label'=>'Manage BiodataWni', 'url'=>array('admin')),
);
?>

<h1>Update BiodataWni <?php echo $model->nik; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>