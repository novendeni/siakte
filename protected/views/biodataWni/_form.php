<?php
/* @var $this BiodataWniController */
/* @var $model BiodataWni */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'biodata-wni-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<!-- generate by system 
	<div class="row">
		<?php echo $form->labelEx($model,'nik'); ?>
		<?php echo $form->textField($model,'nik',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'nik'); ?>
	</div>
	--->
	<!--
	<div class="row">
		<?php echo $form->labelEx($model,'no_ktp'); ?>
		<?php echo $form->textField($model,'no_ktp',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'no_ktp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tmpt_sbl'); ?>
		<?php echo $form->textField($model,'tmpt_sbl',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'tmpt_sbl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_paspor'); ?>
		<?php echo $form->textField($model,'no_paspor',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'no_paspor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgl_akh_paspor'); ?>
		<?php echo $form->textField($model,'tgl_akh_paspor'); ?>
		<?php echo $form->error($model,'tgl_akh_paspor'); ?>
	</div>
	--->
	
	<div class="row">
		<?php echo $form->labelEx($model,'nama_lgkp'); ?>
		<?php echo $form->textField($model,'nama_lgkp',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'nama_lgkp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jenis_klmin'); ?>
		<?php echo $form->textField($model,'jenis_klmin'); ?>
		<?php echo $form->error($model,'jenis_klmin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tmpt_lhr'); ?>
		<?php echo $form->textField($model,'tmpt_lhr',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'tmpt_lhr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgl_lhr'); ?>
		<?php //echo $form->textField($model,'tgl_lhr'); 
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'name'=>'BiodataWni[tgl_lhr]',
				'id'=>'tgl_lhr',
			    'value'=>$model->tgl_lhr,
				'options'=>array(
    				'dateFormat'=>'yy-mm-dd',
					'showAnim'=>'fold',
				),
				'htmlOptions'=>array(
    				'style'=>'height:20px;'
				),
			));
		?>
		<?php echo $form->error($model,'tgl_lhr'); ?>
	</div>
	
	<!--
	<div class="row">
		<?php echo $form->labelEx($model,'akta_lhr'); ?>
		<?php echo $form->textField($model,'akta_lhr'); ?>
		<?php echo $form->error($model,'akta_lhr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_akta_lhr'); ?>
		<?php echo $form->textField($model,'no_akta_lhr',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'no_akta_lhr'); ?>
	</div>
	-->
	
	<div class="row">
		<?php echo $form->labelEx($model,'gol_drh'); ?>
		<?php echo $form->textField($model,'gol_drh'); ?>
		<?php echo $form->error($model,'gol_drh'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'agama'); ?>
		<?php echo $form->textField($model,'agama',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'agama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stat_kwn'); ?>
		<?php echo $form->textField($model,'stat_kwn'); ?>
		<?php echo $form->error($model,'stat_kwn'); ?>
	</div>
	
	<!--
	<div class="row">
		<?php echo $form->labelEx($model,'akta_kwn'); ?>
		<?php echo $form->textField($model,'akta_kwn'); ?>
		<?php echo $form->error($model,'akta_kwn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_akta_kwn'); ?>
		<?php echo $form->textField($model,'no_akta_kwn',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'no_akta_kwn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgl_kwn'); ?>
		<?php echo $form->textField($model,'tgl_kwn'); ?>
		<?php echo $form->error($model,'tgl_kwn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'akta_crai'); ?>
		<?php echo $form->textField($model,'akta_crai'); ?>
		<?php echo $form->error($model,'akta_crai'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_akta_crai'); ?>
		<?php echo $form->textField($model,'no_akta_crai',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'no_akta_crai'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgl_crai'); ?>
		<?php echo $form->textField($model,'tgl_crai'); ?>
		<?php echo $form->error($model,'tgl_crai'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stat_hbkel'); ?>
		<?php echo $form->textField($model,'stat_hbkel'); ?>
		<?php echo $form->error($model,'stat_hbkel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'klain_fsk'); ?>
		<?php echo $form->textField($model,'klain_fsk'); ?>
		<?php echo $form->error($model,'klain_fsk'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pnydng_cct'); ?>
		<?php echo $form->textField($model,'pnydng_cct'); ?>
		<?php echo $form->error($model,'pnydng_cct'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pddk_akh'); ?>
		<?php echo $form->textField($model,'pddk_akh'); ?>
		<?php echo $form->error($model,'pddk_akh'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jenis_pkrjn'); ?>
		<?php echo $form->textField($model,'jenis_pkrjn'); ?>
		<?php echo $form->error($model,'jenis_pkrjn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nik_ibu'); ?>
		<?php echo $form->textField($model,'nik_ibu',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'nik_ibu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_lgkp_ibu'); ?>
		<?php echo $form->textField($model,'nama_lgkp_ibu',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'nama_lgkp_ibu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nik_ayah'); ?>
		<?php echo $form->textField($model,'nik_ayah',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'nik_ayah'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_lgkp_ayah'); ?>
		<?php echo $form->textField($model,'nama_lgkp_ayah',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'nama_lgkp_ayah'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_ket_rt'); ?>
		<?php echo $form->textField($model,'nama_ket_rt',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'nama_ket_rt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_ket_rw'); ?>
		<?php echo $form->textField($model,'nama_ket_rw',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'nama_ket_rw'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_pet_reg'); ?>
		<?php echo $form->textField($model,'nama_pet_reg',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'nama_pet_reg'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nip_pet_reg'); ?>
		<?php echo $form->textField($model,'nip_pet_reg'); ?>
		<?php echo $form->error($model,'nip_pet_reg'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_pet_entri'); ?>
		<?php echo $form->textField($model,'nama_pet_entri',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'nama_pet_entri'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nip_pet_entri'); ?>
		<?php echo $form->textField($model,'nip_pet_entri'); ?>
		<?php echo $form->error($model,'nip_pet_entri'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgl_entri'); ?>
		<?php echo $form->textField($model,'tgl_entri'); ?>
		<?php echo $form->error($model,'tgl_entri'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_kk'); ?>
		<?php echo $form->textField($model,'no_kk'); ?>
		<?php echo $form->error($model,'no_kk'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jenis_bntu'); ?>
		<?php echo $form->textField($model,'jenis_bntu'); ?>
		<?php echo $form->error($model,'jenis_bntu'); ?>
	</div>
	-->
	
	<div class="row">
		<?php echo $form->labelEx($model,'no_prop'); ?>
		<?php echo $form->textField($model,'no_prop'); ?>
		<?php echo $form->error($model,'no_prop'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_kab'); ?>
		<?php echo $form->textField($model,'no_kab'); ?>
		<?php echo $form->error($model,'no_kab'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_kec'); ?>
		<?php echo $form->textField($model,'no_kec'); ?>
		<?php echo $form->error($model,'no_kec'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_kel'); ?>
		<?php echo $form->textField($model,'no_kel'); ?>
		<?php echo $form->error($model,'no_kel'); ?>
	</div>

	<!--
	<div class="row">
		<?php echo $form->labelEx($model,'stat_hidup'); ?>
		<?php echo $form->textField($model,'stat_hidup'); ?>
		<?php echo $form->error($model,'stat_hidup'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'tgl_ubah'); ?>
		<?php echo $form->textField($model,'tgl_ubah'); ?>
		<?php echo $form->error($model,'tgl_ubah'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgl_cetak_ktp'); ?>
		<?php echo $form->textField($model,'tgl_cetak_ktp'); ?>
		<?php echo $form->error($model,'tgl_cetak_ktp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgl_ganti_ktp'); ?>
		<?php echo $form->textField($model,'tgl_ganti_ktp'); ?>
		<?php echo $form->error($model,'tgl_ganti_ktp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgl_pjg_ktp'); ?>
		<?php echo $form->textField($model,'tgl_pjg_ktp'); ?>
		<?php echo $form->error($model,'tgl_pjg_ktp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stat_ktp'); ?>
		<?php echo $form->textField($model,'stat_ktp'); ?>
		<?php echo $form->error($model,'stat_ktp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'als_numpang'); ?>
		<?php echo $form->textField($model,'als_numpang'); ?>
		<?php echo $form->error($model,'als_numpang'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pflag'); ?>
		<?php echo $form->textField($model,'pflag',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'pflag'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cflag'); ?>
		<?php echo $form->textField($model,'cflag',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'cflag'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sync_flag'); ?>
		<?php echo $form->textField($model,'sync_flag'); ?>
		<?php echo $form->error($model,'sync_flag'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ket_agama'); ?>
		<?php echo $form->textField($model,'ket_agama',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'ket_agama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kebangsaan'); ?>
		<?php echo $form->textField($model,'kebangsaan',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'kebangsaan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gelar'); ?>
		<?php echo $form->textField($model,'gelar',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'gelar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ket_pkrjn'); ?>
		<?php echo $form->textField($model,'ket_pkrjn',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'ket_pkrjn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'glr_agama'); ?>
		<?php echo $form->textField($model,'glr_agama',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'glr_agama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'glr_akademis'); ?>
		<?php echo $form->textField($model,'glr_akademis',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'glr_akademis'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'glr_bangsawan'); ?>
		<?php echo $form->textField($model,'glr_bangsawan',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'glr_bangsawan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_pros_datang'); ?>
		<?php echo $form->textField($model,'is_pros_datang',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'is_pros_datang'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'desc_pekerjaan'); ?>
		<?php echo $form->textField($model,'desc_pekerjaan',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'desc_pekerjaan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'desc_kepercayaan'); ?>
		<?php echo $form->textField($model,'desc_kepercayaan',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'desc_kepercayaan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'flag_status'); ?>
		<?php echo $form->textField($model,'flag_status',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'flag_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'count_ktp'); ?>
		<?php echo $form->textField($model,'count_ktp'); ?>
		<?php echo $form->error($model,'count_ktp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'count_biodata'); ?>
		<?php echo $form->textField($model,'count_biodata'); ?>
		<?php echo $form->error($model,'count_biodata'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'flagsink'); ?>
		<?php echo $form->textField($model,'flagsink',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'flagsink'); ?>
	</div>
	-->
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->