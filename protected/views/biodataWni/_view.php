<?php
/* @var $this BiodataWniController */
/* @var $data BiodataWni */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('nik')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->nik), array('view', 'id'=>$data->nik)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_ktp')); ?>:</b>
	<?php echo CHtml::encode($data->no_ktp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tmpt_sbl')); ?>:</b>
	<?php echo CHtml::encode($data->tmpt_sbl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_paspor')); ?>:</b>
	<?php echo CHtml::encode($data->no_paspor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_akh_paspor')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_akh_paspor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_lgkp')); ?>:</b>
	<?php echo CHtml::encode($data->nama_lgkp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_klmin')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_klmin); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tmpt_lhr')); ?>:</b>
	<?php echo CHtml::encode($data->tmpt_lhr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_lhr')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_lhr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('akta_lhr')); ?>:</b>
	<?php echo CHtml::encode($data->akta_lhr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_akta_lhr')); ?>:</b>
	<?php echo CHtml::encode($data->no_akta_lhr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gol_drh')); ?>:</b>
	<?php echo CHtml::encode($data->gol_drh); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agama')); ?>:</b>
	<?php echo CHtml::encode($data->agama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stat_kwn')); ?>:</b>
	<?php echo CHtml::encode($data->stat_kwn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('akta_kwn')); ?>:</b>
	<?php echo CHtml::encode($data->akta_kwn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_akta_kwn')); ?>:</b>
	<?php echo CHtml::encode($data->no_akta_kwn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_kwn')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_kwn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('akta_crai')); ?>:</b>
	<?php echo CHtml::encode($data->akta_crai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_akta_crai')); ?>:</b>
	<?php echo CHtml::encode($data->no_akta_crai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_crai')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_crai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stat_hbkel')); ?>:</b>
	<?php echo CHtml::encode($data->stat_hbkel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('klain_fsk')); ?>:</b>
	<?php echo CHtml::encode($data->klain_fsk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pnydng_cct')); ?>:</b>
	<?php echo CHtml::encode($data->pnydng_cct); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pddk_akh')); ?>:</b>
	<?php echo CHtml::encode($data->pddk_akh); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_pkrjn')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_pkrjn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nik_ibu')); ?>:</b>
	<?php echo CHtml::encode($data->nik_ibu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_lgkp_ibu')); ?>:</b>
	<?php echo CHtml::encode($data->nama_lgkp_ibu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nik_ayah')); ?>:</b>
	<?php echo CHtml::encode($data->nik_ayah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_lgkp_ayah')); ?>:</b>
	<?php echo CHtml::encode($data->nama_lgkp_ayah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_ket_rt')); ?>:</b>
	<?php echo CHtml::encode($data->nama_ket_rt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_ket_rw')); ?>:</b>
	<?php echo CHtml::encode($data->nama_ket_rw); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_pet_reg')); ?>:</b>
	<?php echo CHtml::encode($data->nama_pet_reg); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nip_pet_reg')); ?>:</b>
	<?php echo CHtml::encode($data->nip_pet_reg); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_pet_entri')); ?>:</b>
	<?php echo CHtml::encode($data->nama_pet_entri); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nip_pet_entri')); ?>:</b>
	<?php echo CHtml::encode($data->nip_pet_entri); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_entri')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_entri); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_kk')); ?>:</b>
	<?php echo CHtml::encode($data->no_kk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_bntu')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_bntu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_prop')); ?>:</b>
	<?php echo CHtml::encode($data->no_prop); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_kab')); ?>:</b>
	<?php echo CHtml::encode($data->no_kab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_kec')); ?>:</b>
	<?php echo CHtml::encode($data->no_kec); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_kel')); ?>:</b>
	<?php echo CHtml::encode($data->no_kel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stat_hidup')); ?>:</b>
	<?php echo CHtml::encode($data->stat_hidup); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_ubah')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_ubah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_cetak_ktp')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_cetak_ktp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_ganti_ktp')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_ganti_ktp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_pjg_ktp')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_pjg_ktp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stat_ktp')); ?>:</b>
	<?php echo CHtml::encode($data->stat_ktp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('als_numpang')); ?>:</b>
	<?php echo CHtml::encode($data->als_numpang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pflag')); ?>:</b>
	<?php echo CHtml::encode($data->pflag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cflag')); ?>:</b>
	<?php echo CHtml::encode($data->cflag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sync_flag')); ?>:</b>
	<?php echo CHtml::encode($data->sync_flag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ket_agama')); ?>:</b>
	<?php echo CHtml::encode($data->ket_agama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kebangsaan')); ?>:</b>
	<?php echo CHtml::encode($data->kebangsaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gelar')); ?>:</b>
	<?php echo CHtml::encode($data->gelar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ket_pkrjn')); ?>:</b>
	<?php echo CHtml::encode($data->ket_pkrjn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('glr_agama')); ?>:</b>
	<?php echo CHtml::encode($data->glr_agama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('glr_akademis')); ?>:</b>
	<?php echo CHtml::encode($data->glr_akademis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('glr_bangsawan')); ?>:</b>
	<?php echo CHtml::encode($data->glr_bangsawan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_pros_datang')); ?>:</b>
	<?php echo CHtml::encode($data->is_pros_datang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('desc_pekerjaan')); ?>:</b>
	<?php echo CHtml::encode($data->desc_pekerjaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('desc_kepercayaan')); ?>:</b>
	<?php echo CHtml::encode($data->desc_kepercayaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_status')); ?>:</b>
	<?php echo CHtml::encode($data->flag_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('count_ktp')); ?>:</b>
	<?php echo CHtml::encode($data->count_ktp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('count_biodata')); ?>:</b>
	<?php echo CHtml::encode($data->count_biodata); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flagsink')); ?>:</b>
	<?php echo CHtml::encode($data->flagsink); ?>
	<br />

	*/ ?>

</div>