<?php
/* @var $this BidanController */
/* @var $model Bidan */

$this->breadcrumbs=array(
	'Bidans'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Bidan', 'url'=>array('index')),
	array('label'=>'Manage Bidan', 'url'=>array('admin')),
);
?>

<h1>Create Bidan</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>