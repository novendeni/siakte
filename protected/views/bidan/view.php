<?php
/* @var $this BidanController */
/* @var $model Bidan */

$this->breadcrumbs=array(
	'Bidans'=>array('index'),
	$model->nik,
);

$this->menu=array(
	array('label'=>'List Bidan', 'url'=>array('index')),
	array('label'=>'Create Bidan', 'url'=>array('create')),
	array('label'=>'Update Bidan', 'url'=>array('update', 'id'=>$model->nik)),
	array('label'=>'Delete Bidan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->nik),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Bidan', 'url'=>array('admin')),
);
?>

<h1>View Bidan #<?php echo $model->nik; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'nik',
		'nama',
		'tmpt_lahir',
		'tgl_lahir',
		'alamat',
		'alamat_praktek',
	),
)); ?>
