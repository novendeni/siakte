<?php
/* @var $this BidanController */
/* @var $model Bidan */

$this->breadcrumbs=array(
	'Bidans'=>array('index'),
	$model->nik=>array('view','id'=>$model->nik),
	'Update',
);

$this->menu=array(
	array('label'=>'List Bidan', 'url'=>array('index')),
	array('label'=>'Create Bidan', 'url'=>array('create')),
	array('label'=>'View Bidan', 'url'=>array('view', 'id'=>$model->nik)),
	array('label'=>'Manage Bidan', 'url'=>array('admin')),
);
?>

<h1>Update Bidan <?php echo $model->nik; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>