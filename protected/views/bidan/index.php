<?php
/* @var $this BidanController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bidans',
);

$this->menu=array(
	array('label'=>'Create Bidan', 'url'=>array('create')),
	array('label'=>'Manage Bidan', 'url'=>array('admin')),
);
?>

<h1>Bidans</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
