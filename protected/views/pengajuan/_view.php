<?php
/* @var $this PengajuanController */
/* @var $data Pengajuan */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('pengajuan_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->pengajuan_id),array('view','id'=>$data->pengajuan_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ad_id')); ?>:</b>
	<?php echo CHtml::encode($data->ad_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nik')); ?>:</b>
	<?php echo CHtml::encode($data->nik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('umur_kehamilan')); ?>:</b>
	<?php echo CHtml::encode($data->umur_kehamilan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_id')); ?>:</b>
	<?php echo CHtml::encode($data->status_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_pengajuan')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_pengajuan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insert_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->insert_datetime); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('insert_by')); ?>:</b>
	<?php echo CHtml::encode($data->insert_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->update_datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_by')); ?>:</b>
	<?php echo CHtml::encode($data->update_by); ?>
	<br />

	*/ ?>

</div>