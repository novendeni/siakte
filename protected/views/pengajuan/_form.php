<?php
/* @var $this PengajuanController */
/* @var $model Pengajuan */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php 
		$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
			'id'=>'pengajuan-form',
	
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Kolom dengan tanda <span class="required">*</span> WAJIB diisi !</p>

    <?php echo $form->errorSummary($model); ?>
	
            <?php 
				$list = CHtml::listData(AdministrasiKependudukan::model()->findAll(array('order' => 'ad_id')), 'ad_id', 'jenis');
				echo $form->dropDownListControlGroup($model,'ad_id',$list); 
			?>

            <?php echo $form->textFieldControlGroup($model,'nik',array('span'=>2,'maxlength'=>16)); ?>

            <?php echo $form->dropDownListControlGroup($model,'umur_kehamilan',array(
				'1'=>'1 Bulan',
				'2'=>'2 Bulan',
				'3'=>'3 Bulan',
				'4'=>'4 Bulan',
				'5'=>'5 Bulan',
				'6'=>'6 Bulan',
				'7'=>'7 Bulan',
				'8'=>'8 Bulan',
				'9'=>'9 Bulan',
			)); ?>

            <?php 
				$status = CHtml::listData(StatusPengajuan::model()->findAll(array('order' => 'status_id')), 'status_id', 'keterangan');
				echo $form->dropDownListControlGroup($model,'status_id',$status); 
			?>

            <?php //echo $form->textFieldControlGroup($model,'tanggal_pengajuan',array('span'=>5)); ?>

            <?php //echo $form->textFieldControlGroup($model,'insert_datetime',array('span'=>5)); ?>

            <?php //echo $form->textFieldControlGroup($model,'insert_by',array('span'=>5,'maxlength'=>20)); ?>

            <?php //echo $form->textFieldControlGroup($model,'update_datetime',array('span'=>5)); ?>

            <?php //echo $form->textFieldControlGroup($model,'update_by',array('span'=>5,'maxlength'=>20)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Update',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    //'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->