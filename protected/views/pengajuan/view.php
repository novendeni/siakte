<?php
/* @var $this PengajuanController */
/* @var $model Pengajuan */
?>

<?php
$this->breadcrumbs=array(
	'Pengajuans'=>array('index'),
	$model->pengajuan_id,
);

$this->menu=array(
	array('label'=>'List Pengajuan', 'url'=>array('index')),
	array('label'=>'Create Pengajuan', 'url'=>array('create')),
	array('label'=>'Update Pengajuan', 'url'=>array('update', 'id'=>$model->pengajuan_id)),
	array('label'=>'Delete Pengajuan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->pengajuan_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Pengajuan', 'url'=>array('admin')),
);
?>

<h1>View Pengajuan #<?php echo $model->pengajuan_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'pengajuan_id',
		'ad_id',
		'nik',
		'umur_kehamilan',
		'status_id',
		'tanggal_pengajuan',
		'insert_datetime',
		'insert_by',
		'update_datetime',
		'update_by',
	),
)); ?>