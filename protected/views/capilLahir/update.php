<?php
/* @var $this CapilLahirController */
/* @var $model CapilLahir */

$this->breadcrumbs=array(
	'Capil Lahirs'=>array('index'),
	$model->bayi_no=>array('view','id'=>$model->bayi_no),
	'Update',
);

$this->menu=array(
	array('label'=>'List CapilLahir', 'url'=>array('index')),
	array('label'=>'Create CapilLahir', 'url'=>array('create')),
	array('label'=>'View CapilLahir', 'url'=>array('view', 'id'=>$model->bayi_no)),
	array('label'=>'Manage CapilLahir', 'url'=>array('admin')),
);
?>

<h1>Update CapilLahir <?php echo $model->bayi_no; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>