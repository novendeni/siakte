<?php
/* @var $this CapilLahirController */
/* @var $model CapilLahir */

$this->breadcrumbs=array(
	'Capil Lahirs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List CapilLahir', 'url'=>array('index')),
	array('label'=>'Create CapilLahir', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#capil-lahir-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Capil Lahirs</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'capil-lahir-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'bayi_no',
		'bayi_org_asing',
		'bayi_nik',
		'bayi_nama_lgkp',
		'bayi_tmpt_lahir',
		'bayi_tgl_lahir',
		/*
		'bayi_wkt_lahir',
		'bayi_jns_kelamin',
		'bayi_jns_klhr',
		'bayi_tmpt_klhr',
		'bayi_anak_ke',
		'bayi_berat',
		'bayi_panjang',
		'bayi_pnlg_klhr',
		'bayi_dom_lahir',
		'bayi_ln_lahir',
		'no_kk',
		'nama_kk',
		'ibu_nik',
		'ibu_nama_lgkp',
		'ibu_tgl_lahir',
		'ibu_tgl_kawin',
		'ibu_wrg_ngr',
		'ibu_pekerjaan',
		'ibu_kebangsaan',
		'ibu_alamat',
		'ibu_no_rt',
		'ibu_no_rw',
		'ibu_no_prov',
		'ibu_no_kab',
		'ibu_no_kec',
		'ibu_no_kel',
		'ayah_nik',
		'ayah_nama_lgkp',
		'ayah_tgl_lahir',
		'ayah_wrg_ngr',
		'ayah_pekerjaan',
		'ayah_kebangsaan',
		'ayah_alamat',
		'ayah_no_rt',
		'ayah_no_rw',
		'ayah_no_prov',
		'ayah_no_kab',
		'ayah_no_kec',
		'ayah_no_kel',
		'plpr_nik',
		'plpr_tgl_lapor',
		'plpr_umur',
		'plpr_nama_lgkp',
		'plpr_kelamin',
		'plpr_pekerjaan',
		'plpr_alamat',
		'plpr_no_rt',
		'plpr_no_rw',
		'plpr_no_prov',
		'plpr_no_kab',
		'plpr_no_kec',
		'plpr_no_kel',
		'plpr_hub_kel',
		'saksi1_nik',
		'saksi1_nama_lgkp',
		'saksi1_umur',
		'saksi1_kelamin',
		'saksi1_pekerjaan',
		'saksi1_alamat',
		'saksi1_no_rt',
		'saksi1_no_rw',
		'saksi1_no_prov',
		'saksi1_no_kab',
		'saksi1_no_kec',
		'saksi1_no_kel',
		'saksi2_nik',
		'saksi2_nama_lgkp',
		'saksi2_umur',
		'saksi2_kelamin',
		'saksi2_pekerjaan',
		'saksi2_alamat',
		'saksi2_no_rt',
		'saksi2_no_rw',
		'saksi2_kode_pos',
		'saksi2_no_prov',
		'saksi2_no_kab',
		'saksi2_no_kec',
		'saksi2_no_kel',
		'adm_no_kedubes',
		'adm_no_konjen',
		'adm_no_konsul',
		'adm_no_prov',
		'adm_no_kab',
		'adm_no_kec',
		'adm_no_kel',
		'adm_akta_no',
		'adm_dokumen',
		'adm_sp_no',
		'adm_sp_tgl',
		'adm_sp_nama',
		'adm_akta_tgl',
		'adm_akta_ktp_tgl',
		'adm_kades_nama',
		'adm_kades_nip',
		'adm_regs_nama',
		'adm_regs_nip',
		'adm_tgl_entry',
		'adm_tgl_update',
		'user_id',
		'flag_update',
		'flag_ctk_akta',
		'flag_ctk_kakta',
		'flag_ctk_sk',
		'flag_status',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
