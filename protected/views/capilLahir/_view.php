<?php
/* @var $this CapilLahirController */
/* @var $data CapilLahir */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('bayi_no')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->bayi_no), array('view', 'id'=>$data->bayi_no)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bayi_org_asing')); ?>:</b>
	<?php echo CHtml::encode($data->bayi_org_asing); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bayi_nik')); ?>:</b>
	<?php echo CHtml::encode($data->bayi_nik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bayi_nama_lgkp')); ?>:</b>
	<?php echo CHtml::encode($data->bayi_nama_lgkp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bayi_tmpt_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->bayi_tmpt_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bayi_tgl_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->bayi_tgl_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bayi_wkt_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->bayi_wkt_lahir); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('bayi_jns_kelamin')); ?>:</b>
	<?php echo CHtml::encode($data->bayi_jns_kelamin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bayi_jns_klhr')); ?>:</b>
	<?php echo CHtml::encode($data->bayi_jns_klhr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bayi_tmpt_klhr')); ?>:</b>
	<?php echo CHtml::encode($data->bayi_tmpt_klhr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bayi_anak_ke')); ?>:</b>
	<?php echo CHtml::encode($data->bayi_anak_ke); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bayi_berat')); ?>:</b>
	<?php echo CHtml::encode($data->bayi_berat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bayi_panjang')); ?>:</b>
	<?php echo CHtml::encode($data->bayi_panjang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bayi_pnlg_klhr')); ?>:</b>
	<?php echo CHtml::encode($data->bayi_pnlg_klhr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bayi_dom_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->bayi_dom_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bayi_ln_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->bayi_ln_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_kk')); ?>:</b>
	<?php echo CHtml::encode($data->no_kk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_kk')); ?>:</b>
	<?php echo CHtml::encode($data->nama_kk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ibu_nik')); ?>:</b>
	<?php echo CHtml::encode($data->ibu_nik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ibu_nama_lgkp')); ?>:</b>
	<?php echo CHtml::encode($data->ibu_nama_lgkp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ibu_tgl_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->ibu_tgl_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ibu_tgl_kawin')); ?>:</b>
	<?php echo CHtml::encode($data->ibu_tgl_kawin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ibu_wrg_ngr')); ?>:</b>
	<?php echo CHtml::encode($data->ibu_wrg_ngr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ibu_pekerjaan')); ?>:</b>
	<?php echo CHtml::encode($data->ibu_pekerjaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ibu_kebangsaan')); ?>:</b>
	<?php echo CHtml::encode($data->ibu_kebangsaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ibu_alamat')); ?>:</b>
	<?php echo CHtml::encode($data->ibu_alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ibu_no_rt')); ?>:</b>
	<?php echo CHtml::encode($data->ibu_no_rt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ibu_no_rw')); ?>:</b>
	<?php echo CHtml::encode($data->ibu_no_rw); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ibu_no_prov')); ?>:</b>
	<?php echo CHtml::encode($data->ibu_no_prov); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ibu_no_kab')); ?>:</b>
	<?php echo CHtml::encode($data->ibu_no_kab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ibu_no_kec')); ?>:</b>
	<?php echo CHtml::encode($data->ibu_no_kec); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ibu_no_kel')); ?>:</b>
	<?php echo CHtml::encode($data->ibu_no_kel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ayah_nik')); ?>:</b>
	<?php echo CHtml::encode($data->ayah_nik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ayah_nama_lgkp')); ?>:</b>
	<?php echo CHtml::encode($data->ayah_nama_lgkp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ayah_tgl_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->ayah_tgl_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ayah_wrg_ngr')); ?>:</b>
	<?php echo CHtml::encode($data->ayah_wrg_ngr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ayah_pekerjaan')); ?>:</b>
	<?php echo CHtml::encode($data->ayah_pekerjaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ayah_kebangsaan')); ?>:</b>
	<?php echo CHtml::encode($data->ayah_kebangsaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ayah_alamat')); ?>:</b>
	<?php echo CHtml::encode($data->ayah_alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ayah_no_rt')); ?>:</b>
	<?php echo CHtml::encode($data->ayah_no_rt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ayah_no_rw')); ?>:</b>
	<?php echo CHtml::encode($data->ayah_no_rw); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ayah_no_prov')); ?>:</b>
	<?php echo CHtml::encode($data->ayah_no_prov); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ayah_no_kab')); ?>:</b>
	<?php echo CHtml::encode($data->ayah_no_kab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ayah_no_kec')); ?>:</b>
	<?php echo CHtml::encode($data->ayah_no_kec); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ayah_no_kel')); ?>:</b>
	<?php echo CHtml::encode($data->ayah_no_kel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plpr_nik')); ?>:</b>
	<?php echo CHtml::encode($data->plpr_nik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plpr_tgl_lapor')); ?>:</b>
	<?php echo CHtml::encode($data->plpr_tgl_lapor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plpr_umur')); ?>:</b>
	<?php echo CHtml::encode($data->plpr_umur); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plpr_nama_lgkp')); ?>:</b>
	<?php echo CHtml::encode($data->plpr_nama_lgkp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plpr_kelamin')); ?>:</b>
	<?php echo CHtml::encode($data->plpr_kelamin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plpr_pekerjaan')); ?>:</b>
	<?php echo CHtml::encode($data->plpr_pekerjaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plpr_alamat')); ?>:</b>
	<?php echo CHtml::encode($data->plpr_alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plpr_no_rt')); ?>:</b>
	<?php echo CHtml::encode($data->plpr_no_rt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plpr_no_rw')); ?>:</b>
	<?php echo CHtml::encode($data->plpr_no_rw); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plpr_no_prov')); ?>:</b>
	<?php echo CHtml::encode($data->plpr_no_prov); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plpr_no_kab')); ?>:</b>
	<?php echo CHtml::encode($data->plpr_no_kab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plpr_no_kec')); ?>:</b>
	<?php echo CHtml::encode($data->plpr_no_kec); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plpr_no_kel')); ?>:</b>
	<?php echo CHtml::encode($data->plpr_no_kel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plpr_hub_kel')); ?>:</b>
	<?php echo CHtml::encode($data->plpr_hub_kel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi1_nik')); ?>:</b>
	<?php echo CHtml::encode($data->saksi1_nik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi1_nama_lgkp')); ?>:</b>
	<?php echo CHtml::encode($data->saksi1_nama_lgkp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi1_umur')); ?>:</b>
	<?php echo CHtml::encode($data->saksi1_umur); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi1_kelamin')); ?>:</b>
	<?php echo CHtml::encode($data->saksi1_kelamin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi1_pekerjaan')); ?>:</b>
	<?php echo CHtml::encode($data->saksi1_pekerjaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi1_alamat')); ?>:</b>
	<?php echo CHtml::encode($data->saksi1_alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi1_no_rt')); ?>:</b>
	<?php echo CHtml::encode($data->saksi1_no_rt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi1_no_rw')); ?>:</b>
	<?php echo CHtml::encode($data->saksi1_no_rw); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi1_no_prov')); ?>:</b>
	<?php echo CHtml::encode($data->saksi1_no_prov); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi1_no_kab')); ?>:</b>
	<?php echo CHtml::encode($data->saksi1_no_kab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi1_no_kec')); ?>:</b>
	<?php echo CHtml::encode($data->saksi1_no_kec); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi1_no_kel')); ?>:</b>
	<?php echo CHtml::encode($data->saksi1_no_kel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi2_nik')); ?>:</b>
	<?php echo CHtml::encode($data->saksi2_nik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi2_nama_lgkp')); ?>:</b>
	<?php echo CHtml::encode($data->saksi2_nama_lgkp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi2_umur')); ?>:</b>
	<?php echo CHtml::encode($data->saksi2_umur); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi2_kelamin')); ?>:</b>
	<?php echo CHtml::encode($data->saksi2_kelamin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi2_pekerjaan')); ?>:</b>
	<?php echo CHtml::encode($data->saksi2_pekerjaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi2_alamat')); ?>:</b>
	<?php echo CHtml::encode($data->saksi2_alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi2_no_rt')); ?>:</b>
	<?php echo CHtml::encode($data->saksi2_no_rt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi2_no_rw')); ?>:</b>
	<?php echo CHtml::encode($data->saksi2_no_rw); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi2_kode_pos')); ?>:</b>
	<?php echo CHtml::encode($data->saksi2_kode_pos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi2_no_prov')); ?>:</b>
	<?php echo CHtml::encode($data->saksi2_no_prov); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi2_no_kab')); ?>:</b>
	<?php echo CHtml::encode($data->saksi2_no_kab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi2_no_kec')); ?>:</b>
	<?php echo CHtml::encode($data->saksi2_no_kec); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saksi2_no_kel')); ?>:</b>
	<?php echo CHtml::encode($data->saksi2_no_kel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_no_kedubes')); ?>:</b>
	<?php echo CHtml::encode($data->adm_no_kedubes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_no_konjen')); ?>:</b>
	<?php echo CHtml::encode($data->adm_no_konjen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_no_konsul')); ?>:</b>
	<?php echo CHtml::encode($data->adm_no_konsul); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_no_prov')); ?>:</b>
	<?php echo CHtml::encode($data->adm_no_prov); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_no_kab')); ?>:</b>
	<?php echo CHtml::encode($data->adm_no_kab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_no_kec')); ?>:</b>
	<?php echo CHtml::encode($data->adm_no_kec); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_no_kel')); ?>:</b>
	<?php echo CHtml::encode($data->adm_no_kel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_akta_no')); ?>:</b>
	<?php echo CHtml::encode($data->adm_akta_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_dokumen')); ?>:</b>
	<?php echo CHtml::encode($data->adm_dokumen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_sp_no')); ?>:</b>
	<?php echo CHtml::encode($data->adm_sp_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_sp_tgl')); ?>:</b>
	<?php echo CHtml::encode($data->adm_sp_tgl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_sp_nama')); ?>:</b>
	<?php echo CHtml::encode($data->adm_sp_nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_akta_tgl')); ?>:</b>
	<?php echo CHtml::encode($data->adm_akta_tgl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_akta_ktp_tgl')); ?>:</b>
	<?php echo CHtml::encode($data->adm_akta_ktp_tgl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_kades_nama')); ?>:</b>
	<?php echo CHtml::encode($data->adm_kades_nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_kades_nip')); ?>:</b>
	<?php echo CHtml::encode($data->adm_kades_nip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_regs_nama')); ?>:</b>
	<?php echo CHtml::encode($data->adm_regs_nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_regs_nip')); ?>:</b>
	<?php echo CHtml::encode($data->adm_regs_nip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_tgl_entry')); ?>:</b>
	<?php echo CHtml::encode($data->adm_tgl_entry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adm_tgl_update')); ?>:</b>
	<?php echo CHtml::encode($data->adm_tgl_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_update')); ?>:</b>
	<?php echo CHtml::encode($data->flag_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_ctk_akta')); ?>:</b>
	<?php echo CHtml::encode($data->flag_ctk_akta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_ctk_kakta')); ?>:</b>
	<?php echo CHtml::encode($data->flag_ctk_kakta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_ctk_sk')); ?>:</b>
	<?php echo CHtml::encode($data->flag_ctk_sk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_status')); ?>:</b>
	<?php echo CHtml::encode($data->flag_status); ?>
	<br />

	*/ ?>

</div>