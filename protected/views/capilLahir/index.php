<?php
/* @var $this CapilLahirController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Capil Lahirs',
);

$this->menu=array(
	array('label'=>'Create CapilLahir', 'url'=>array('create')),
	array('label'=>'Manage CapilLahir', 'url'=>array('admin')),
);
?>

<h1>Capil Lahirs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
