<?php
/* @var $this CapilLahirController */
/* @var $model CapilLahir */

$this->breadcrumbs=array(
	'Capil Lahirs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CapilLahir', 'url'=>array('index')),
	array('label'=>'Manage CapilLahir', 'url'=>array('admin')),
);
?>

<h1>Create CapilLahir</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>