<?php
/* @var $this CapilLahirController */
/* @var $model CapilLahir */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'capil-lahir-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'bayi_org_asing'); ?>
		<?php echo $form->textField($model,'bayi_org_asing',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'bayi_org_asing'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bayi_nik'); ?>
		<?php echo $form->textField($model,'bayi_nik',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'bayi_nik'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bayi_nama_lgkp'); ?>
		<?php echo $form->textField($model,'bayi_nama_lgkp',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'bayi_nama_lgkp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bayi_tmpt_lahir'); ?>
		<?php echo $form->textField($model,'bayi_tmpt_lahir',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'bayi_tmpt_lahir'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bayi_tgl_lahir'); ?>
		<?php echo $form->textField($model,'bayi_tgl_lahir',array('size'=>7,'maxlength'=>7)); ?>
		<?php echo $form->error($model,'bayi_tgl_lahir'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bayi_wkt_lahir'); ?>
		<?php echo $form->textField($model,'bayi_wkt_lahir',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'bayi_wkt_lahir'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bayi_jns_kelamin'); ?>
		<?php echo $form->textField($model,'bayi_jns_kelamin',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'bayi_jns_kelamin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bayi_jns_klhr'); ?>
		<?php echo $form->textField($model,'bayi_jns_klhr',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'bayi_jns_klhr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bayi_tmpt_klhr'); ?>
		<?php echo $form->textField($model,'bayi_tmpt_klhr',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'bayi_tmpt_klhr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bayi_anak_ke'); ?>
		<?php echo $form->textField($model,'bayi_anak_ke',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'bayi_anak_ke'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bayi_berat'); ?>
		<?php echo $form->textField($model,'bayi_berat',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'bayi_berat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bayi_panjang'); ?>
		<?php echo $form->textField($model,'bayi_panjang',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'bayi_panjang'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bayi_pnlg_klhr'); ?>
		<?php echo $form->textField($model,'bayi_pnlg_klhr',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'bayi_pnlg_klhr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bayi_dom_lahir'); ?>
		<?php echo $form->textField($model,'bayi_dom_lahir',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'bayi_dom_lahir'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bayi_ln_lahir'); ?>
		<?php echo $form->textField($model,'bayi_ln_lahir',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'bayi_ln_lahir'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_kk'); ?>
		<?php echo $form->textField($model,'no_kk',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'no_kk'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_kk'); ?>
		<?php echo $form->textField($model,'nama_kk',array('size'=>60,'maxlength'=>65)); ?>
		<?php echo $form->error($model,'nama_kk'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ibu_nik'); ?>
		<?php echo $form->textField($model,'ibu_nik',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'ibu_nik'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ibu_nama_lgkp'); ?>
		<?php echo $form->textField($model,'ibu_nama_lgkp',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'ibu_nama_lgkp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ibu_tgl_lahir'); ?>
		<?php echo $form->textField($model,'ibu_tgl_lahir',array('size'=>7,'maxlength'=>7)); ?>
		<?php echo $form->error($model,'ibu_tgl_lahir'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ibu_tgl_kawin'); ?>
		<?php echo $form->textField($model,'ibu_tgl_kawin',array('size'=>7,'maxlength'=>7)); ?>
		<?php echo $form->error($model,'ibu_tgl_kawin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ibu_wrg_ngr'); ?>
		<?php echo $form->textField($model,'ibu_wrg_ngr',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'ibu_wrg_ngr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ibu_pekerjaan'); ?>
		<?php echo $form->textField($model,'ibu_pekerjaan',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'ibu_pekerjaan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ibu_kebangsaan'); ?>
		<?php echo $form->textField($model,'ibu_kebangsaan',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'ibu_kebangsaan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ibu_alamat'); ?>
		<?php echo $form->textField($model,'ibu_alamat',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'ibu_alamat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ibu_no_rt'); ?>
		<?php echo $form->textField($model,'ibu_no_rt',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'ibu_no_rt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ibu_no_rw'); ?>
		<?php echo $form->textField($model,'ibu_no_rw',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'ibu_no_rw'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ibu_no_prov'); ?>
		<?php echo $form->textField($model,'ibu_no_prov',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'ibu_no_prov'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ibu_no_kab'); ?>
		<?php echo $form->textField($model,'ibu_no_kab',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'ibu_no_kab'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ibu_no_kec'); ?>
		<?php echo $form->textField($model,'ibu_no_kec',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'ibu_no_kec'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ibu_no_kel'); ?>
		<?php echo $form->textField($model,'ibu_no_kel',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'ibu_no_kel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ayah_nik'); ?>
		<?php echo $form->textField($model,'ayah_nik',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'ayah_nik'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ayah_nama_lgkp'); ?>
		<?php echo $form->textField($model,'ayah_nama_lgkp',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'ayah_nama_lgkp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ayah_tgl_lahir'); ?>
		<?php echo $form->textField($model,'ayah_tgl_lahir',array('size'=>7,'maxlength'=>7)); ?>
		<?php echo $form->error($model,'ayah_tgl_lahir'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ayah_wrg_ngr'); ?>
		<?php echo $form->textField($model,'ayah_wrg_ngr',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'ayah_wrg_ngr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ayah_pekerjaan'); ?>
		<?php echo $form->textField($model,'ayah_pekerjaan',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'ayah_pekerjaan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ayah_kebangsaan'); ?>
		<?php echo $form->textField($model,'ayah_kebangsaan',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'ayah_kebangsaan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ayah_alamat'); ?>
		<?php echo $form->textField($model,'ayah_alamat',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'ayah_alamat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ayah_no_rt'); ?>
		<?php echo $form->textField($model,'ayah_no_rt',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'ayah_no_rt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ayah_no_rw'); ?>
		<?php echo $form->textField($model,'ayah_no_rw',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'ayah_no_rw'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ayah_no_prov'); ?>
		<?php echo $form->textField($model,'ayah_no_prov',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'ayah_no_prov'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ayah_no_kab'); ?>
		<?php echo $form->textField($model,'ayah_no_kab',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'ayah_no_kab'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ayah_no_kec'); ?>
		<?php echo $form->textField($model,'ayah_no_kec',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'ayah_no_kec'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ayah_no_kel'); ?>
		<?php echo $form->textField($model,'ayah_no_kel',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'ayah_no_kel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plpr_nik'); ?>
		<?php echo $form->textField($model,'plpr_nik',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'plpr_nik'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plpr_tgl_lapor'); ?>
		<?php echo $form->textField($model,'plpr_tgl_lapor',array('size'=>7,'maxlength'=>7)); ?>
		<?php echo $form->error($model,'plpr_tgl_lapor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plpr_umur'); ?>
		<?php echo $form->textField($model,'plpr_umur',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'plpr_umur'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plpr_nama_lgkp'); ?>
		<?php echo $form->textField($model,'plpr_nama_lgkp',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'plpr_nama_lgkp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plpr_kelamin'); ?>
		<?php echo $form->textField($model,'plpr_kelamin',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'plpr_kelamin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plpr_pekerjaan'); ?>
		<?php echo $form->textField($model,'plpr_pekerjaan',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'plpr_pekerjaan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plpr_alamat'); ?>
		<?php echo $form->textField($model,'plpr_alamat',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'plpr_alamat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plpr_no_rt'); ?>
		<?php echo $form->textField($model,'plpr_no_rt',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'plpr_no_rt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plpr_no_rw'); ?>
		<?php echo $form->textField($model,'plpr_no_rw',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'plpr_no_rw'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plpr_no_prov'); ?>
		<?php echo $form->textField($model,'plpr_no_prov',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'plpr_no_prov'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plpr_no_kab'); ?>
		<?php echo $form->textField($model,'plpr_no_kab',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'plpr_no_kab'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plpr_no_kec'); ?>
		<?php echo $form->textField($model,'plpr_no_kec',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'plpr_no_kec'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plpr_no_kel'); ?>
		<?php echo $form->textField($model,'plpr_no_kel',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'plpr_no_kel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plpr_hub_kel'); ?>
		<?php echo $form->textField($model,'plpr_hub_kel',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'plpr_hub_kel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi1_nik'); ?>
		<?php echo $form->textField($model,'saksi1_nik',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'saksi1_nik'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi1_nama_lgkp'); ?>
		<?php echo $form->textField($model,'saksi1_nama_lgkp',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'saksi1_nama_lgkp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi1_umur'); ?>
		<?php echo $form->textField($model,'saksi1_umur',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'saksi1_umur'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi1_kelamin'); ?>
		<?php echo $form->textField($model,'saksi1_kelamin',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'saksi1_kelamin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi1_pekerjaan'); ?>
		<?php echo $form->textField($model,'saksi1_pekerjaan',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'saksi1_pekerjaan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi1_alamat'); ?>
		<?php echo $form->textField($model,'saksi1_alamat',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'saksi1_alamat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi1_no_rt'); ?>
		<?php echo $form->textField($model,'saksi1_no_rt',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'saksi1_no_rt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi1_no_rw'); ?>
		<?php echo $form->textField($model,'saksi1_no_rw',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'saksi1_no_rw'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi1_no_prov'); ?>
		<?php echo $form->textField($model,'saksi1_no_prov',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'saksi1_no_prov'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi1_no_kab'); ?>
		<?php echo $form->textField($model,'saksi1_no_kab',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'saksi1_no_kab'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi1_no_kec'); ?>
		<?php echo $form->textField($model,'saksi1_no_kec',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'saksi1_no_kec'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi1_no_kel'); ?>
		<?php echo $form->textField($model,'saksi1_no_kel',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'saksi1_no_kel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi2_nik'); ?>
		<?php echo $form->textField($model,'saksi2_nik',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'saksi2_nik'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi2_nama_lgkp'); ?>
		<?php echo $form->textField($model,'saksi2_nama_lgkp',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'saksi2_nama_lgkp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi2_umur'); ?>
		<?php echo $form->textField($model,'saksi2_umur',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'saksi2_umur'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi2_kelamin'); ?>
		<?php echo $form->textField($model,'saksi2_kelamin',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'saksi2_kelamin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi2_pekerjaan'); ?>
		<?php echo $form->textField($model,'saksi2_pekerjaan',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'saksi2_pekerjaan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi2_alamat'); ?>
		<?php echo $form->textField($model,'saksi2_alamat',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'saksi2_alamat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi2_no_rt'); ?>
		<?php echo $form->textField($model,'saksi2_no_rt',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'saksi2_no_rt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi2_no_rw'); ?>
		<?php echo $form->textField($model,'saksi2_no_rw',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'saksi2_no_rw'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi2_kode_pos'); ?>
		<?php echo $form->textField($model,'saksi2_kode_pos',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'saksi2_kode_pos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi2_no_prov'); ?>
		<?php echo $form->textField($model,'saksi2_no_prov',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'saksi2_no_prov'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi2_no_kab'); ?>
		<?php echo $form->textField($model,'saksi2_no_kab',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'saksi2_no_kab'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi2_no_kec'); ?>
		<?php echo $form->textField($model,'saksi2_no_kec',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'saksi2_no_kec'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saksi2_no_kel'); ?>
		<?php echo $form->textField($model,'saksi2_no_kel',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'saksi2_no_kel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_no_kedubes'); ?>
		<?php echo $form->textField($model,'adm_no_kedubes',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'adm_no_kedubes'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_no_konjen'); ?>
		<?php echo $form->textField($model,'adm_no_konjen',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'adm_no_konjen'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_no_konsul'); ?>
		<?php echo $form->textField($model,'adm_no_konsul',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'adm_no_konsul'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_no_prov'); ?>
		<?php echo $form->textField($model,'adm_no_prov',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'adm_no_prov'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_no_kab'); ?>
		<?php echo $form->textField($model,'adm_no_kab',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'adm_no_kab'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_no_kec'); ?>
		<?php echo $form->textField($model,'adm_no_kec',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'adm_no_kec'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_no_kel'); ?>
		<?php echo $form->textField($model,'adm_no_kel',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'adm_no_kel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_akta_no'); ?>
		<?php echo $form->textField($model,'adm_akta_no',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'adm_akta_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_dokumen'); ?>
		<?php echo $form->textField($model,'adm_dokumen',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'adm_dokumen'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_sp_no'); ?>
		<?php echo $form->textField($model,'adm_sp_no',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'adm_sp_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_sp_tgl'); ?>
		<?php echo $form->textField($model,'adm_sp_tgl',array('size'=>7,'maxlength'=>7)); ?>
		<?php echo $form->error($model,'adm_sp_tgl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_sp_nama'); ?>
		<?php echo $form->textField($model,'adm_sp_nama',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'adm_sp_nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_akta_tgl'); ?>
		<?php echo $form->textField($model,'adm_akta_tgl',array('size'=>7,'maxlength'=>7)); ?>
		<?php echo $form->error($model,'adm_akta_tgl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_akta_ktp_tgl'); ?>
		<?php echo $form->textField($model,'adm_akta_ktp_tgl',array('size'=>7,'maxlength'=>7)); ?>
		<?php echo $form->error($model,'adm_akta_ktp_tgl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_kades_nama'); ?>
		<?php echo $form->textField($model,'adm_kades_nama',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'adm_kades_nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_kades_nip'); ?>
		<?php echo $form->textField($model,'adm_kades_nip',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'adm_kades_nip'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_regs_nama'); ?>
		<?php echo $form->textField($model,'adm_regs_nama',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'adm_regs_nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_regs_nip'); ?>
		<?php echo $form->textField($model,'adm_regs_nip',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'adm_regs_nip'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_tgl_entry'); ?>
		<?php echo $form->textField($model,'adm_tgl_entry',array('size'=>7,'maxlength'=>7)); ?>
		<?php echo $form->error($model,'adm_tgl_entry'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adm_tgl_update'); ?>
		<?php echo $form->textField($model,'adm_tgl_update',array('size'=>7,'maxlength'=>7)); ?>
		<?php echo $form->error($model,'adm_tgl_update'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id',array('size'=>24,'maxlength'=>24)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'flag_update'); ?>
		<?php echo $form->textField($model,'flag_update',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'flag_update'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'flag_ctk_akta'); ?>
		<?php echo $form->textField($model,'flag_ctk_akta',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'flag_ctk_akta'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'flag_ctk_kakta'); ?>
		<?php echo $form->textField($model,'flag_ctk_kakta',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'flag_ctk_kakta'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'flag_ctk_sk'); ?>
		<?php echo $form->textField($model,'flag_ctk_sk',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'flag_ctk_sk'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'flag_status'); ?>
		<?php echo $form->textField($model,'flag_status',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'flag_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->