<?php
/* @var $this BiodataWniViewController */
/* @var $model BiodataWniView */

$this->breadcrumbs=array(
	'Biodata Wni Views'=>array('index'),
	$model->nik=>array('view','id'=>$model->nik),
	'Update',
);

$this->menu=array(
	array('label'=>'List BiodataWniView', 'url'=>array('index')),
	array('label'=>'Create BiodataWniView', 'url'=>array('create')),
	array('label'=>'View BiodataWniView', 'url'=>array('view', 'id'=>$model->nik)),
	array('label'=>'Manage BiodataWniView', 'url'=>array('admin')),
);
?>

<h1>Update BiodataWniView <?php echo $model->nik; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>