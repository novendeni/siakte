<?php
/* @var $this BiodataWniViewController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Biodata Wni Views',
);

$this->menu=array(
	array('label'=>'Create BiodataWniView', 'url'=>array('create')),
	array('label'=>'Manage BiodataWniView', 'url'=>array('admin')),
);
?>

<h1>Biodata Wni Views</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
