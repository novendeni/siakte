<?php
/* @var $this BiodataWniViewController */
/* @var $model BiodataWniView */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'nik'); ?>
		<?php echo $form->textField($model,'nik',array('size'=>16,'maxlength'=>16)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_ktp'); ?>
		<?php echo $form->textField($model,'no_ktp',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tmpt_sbl'); ?>
		<?php echo $form->textField($model,'tmpt_sbl',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_paspor'); ?>
		<?php echo $form->textField($model,'no_paspor',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_akh_paspor'); ?>
		<?php echo $form->textField($model,'tgl_akh_paspor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_lgkp'); ?>
		<?php echo $form->textField($model,'nama_lgkp',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jenis_klmin'); ?>
		<?php echo $form->textField($model,'jenis_klmin'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tmpt_lhr'); ?>
		<?php echo $form->textField($model,'tmpt_lhr',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_lhr'); ?>
		<?php echo $form->textField($model,'tgl_lhr'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'akta_lhr'); ?>
		<?php echo $form->textField($model,'akta_lhr'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_akta_lhr'); ?>
		<?php echo $form->textField($model,'no_akta_lhr',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gol_drh'); ?>
		<?php echo $form->textField($model,'gol_drh'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'agama'); ?>
		<?php echo $form->textField($model,'agama',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stat_kwn'); ?>
		<?php echo $form->textField($model,'stat_kwn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'akta_kwn'); ?>
		<?php echo $form->textField($model,'akta_kwn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_akta_kwn'); ?>
		<?php echo $form->textField($model,'no_akta_kwn',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_kwn'); ?>
		<?php echo $form->textField($model,'tgl_kwn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'akta_crai'); ?>
		<?php echo $form->textField($model,'akta_crai'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_akta_crai'); ?>
		<?php echo $form->textField($model,'no_akta_crai',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_crai'); ?>
		<?php echo $form->textField($model,'tgl_crai'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stat_hbkel'); ?>
		<?php echo $form->textField($model,'stat_hbkel'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'klain_fsk'); ?>
		<?php echo $form->textField($model,'klain_fsk'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pnydng_cct'); ?>
		<?php echo $form->textField($model,'pnydng_cct'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pddk_akh'); ?>
		<?php echo $form->textField($model,'pddk_akh'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jenis_pkrjn'); ?>
		<?php echo $form->textField($model,'jenis_pkrjn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nik_ibu'); ?>
		<?php echo $form->textField($model,'nik_ibu',array('size'=>16,'maxlength'=>16)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_lgkp_ibu'); ?>
		<?php echo $form->textField($model,'nama_lgkp_ibu',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nik_ayah'); ?>
		<?php echo $form->textField($model,'nik_ayah',array('size'=>16,'maxlength'=>16)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_lgkp_ayah'); ?>
		<?php echo $form->textField($model,'nama_lgkp_ayah',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_ket_rt'); ?>
		<?php echo $form->textField($model,'nama_ket_rt',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_ket_rw'); ?>
		<?php echo $form->textField($model,'nama_ket_rw',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_pet_reg'); ?>
		<?php echo $form->textField($model,'nama_pet_reg',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nip_pet_reg'); ?>
		<?php echo $form->textField($model,'nip_pet_reg'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_pet_entri'); ?>
		<?php echo $form->textField($model,'nama_pet_entri',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nip_pet_entri'); ?>
		<?php echo $form->textField($model,'nip_pet_entri'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_entri'); ?>
		<?php echo $form->textField($model,'tgl_entri'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_kk'); ?>
		<?php echo $form->textField($model,'no_kk'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jenis_bntu'); ?>
		<?php echo $form->textField($model,'jenis_bntu'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_prop'); ?>
		<?php echo $form->textField($model,'no_prop'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_kab'); ?>
		<?php echo $form->textField($model,'no_kab'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_kec'); ?>
		<?php echo $form->textField($model,'no_kec'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_kel'); ?>
		<?php echo $form->textField($model,'no_kel'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stat_hidup'); ?>
		<?php echo $form->textField($model,'stat_hidup'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_ubah'); ?>
		<?php echo $form->textField($model,'tgl_ubah'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_cetak_ktp'); ?>
		<?php echo $form->textField($model,'tgl_cetak_ktp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_ganti_ktp'); ?>
		<?php echo $form->textField($model,'tgl_ganti_ktp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_pjg_ktp'); ?>
		<?php echo $form->textField($model,'tgl_pjg_ktp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stat_ktp'); ?>
		<?php echo $form->textField($model,'stat_ktp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'als_numpang'); ?>
		<?php echo $form->textField($model,'als_numpang'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pflag'); ?>
		<?php echo $form->textField($model,'pflag',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cflag'); ?>
		<?php echo $form->textField($model,'cflag',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sync_flag'); ?>
		<?php echo $form->textField($model,'sync_flag'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ket_agama'); ?>
		<?php echo $form->textField($model,'ket_agama',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kebangsaan'); ?>
		<?php echo $form->textField($model,'kebangsaan',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gelar'); ?>
		<?php echo $form->textField($model,'gelar',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ket_pkrjn'); ?>
		<?php echo $form->textField($model,'ket_pkrjn',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'glr_agama'); ?>
		<?php echo $form->textField($model,'glr_agama',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'glr_akademis'); ?>
		<?php echo $form->textField($model,'glr_akademis',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'glr_bangsawan'); ?>
		<?php echo $form->textField($model,'glr_bangsawan',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_pros_datang'); ?>
		<?php echo $form->textField($model,'is_pros_datang',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'desc_pekerjaan'); ?>
		<?php echo $form->textField($model,'desc_pekerjaan',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'desc_kepercayaan'); ?>
		<?php echo $form->textField($model,'desc_kepercayaan',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'flag_status'); ?>
		<?php echo $form->textField($model,'flag_status',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'count_ktp'); ?>
		<?php echo $form->textField($model,'count_ktp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'count_biodata'); ?>
		<?php echo $form->textField($model,'count_biodata'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'flagsink'); ?>
		<?php echo $form->textField($model,'flagsink',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->