<?php
/* @var $this BiodataWniViewController */
/* @var $model BiodataWniView */

$this->breadcrumbs=array(
	'Biodata Wni Views'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BiodataWniView', 'url'=>array('index')),
	array('label'=>'Manage BiodataWniView', 'url'=>array('admin')),
);
?>

<h1>Create BiodataWniView</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>