<?php
/* @var $this KlinikController */
/* @var $model Klinik */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_klinik'); ?>
		<?php echo $form->textField($model,'id_klinik'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_klinik'); ?>
		<?php echo $form->textField($model,'nama_klinik',array('size'=>60,'maxlength'=>80)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alamat'); ?>
		<?php echo $form->textField($model,'alamat',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_ho'); ?>
		<?php echo $form->textField($model,'no_ho',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_ho'); ?>
		<?php echo $form->textField($model,'tgl_ho'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'penanggung_jawab'); ?>
		<?php echo $form->textField($model,'penanggung_jawab',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pimpinan'); ?>
		<?php echo $form->textField($model,'pimpinan',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pekerjaan'); ?>
		<?php echo $form->textField($model,'pekerjaan',array('size'=>60,'maxlength'=>80)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_izin'); ?>
		<?php echo $form->textField($model,'no_izin',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_izin_berlaku'); ?>
		<?php echo $form->textField($model,'tgl_izin_berlaku'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_izin_keluar'); ?>
		<?php echo $form->textField($model,'tgl_izin_keluar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jns_klinik'); ?>
		<?php echo $form->textField($model,'jns_klinik',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->