<?php
/* @var $this KlinikController */
/* @var $data Klinik */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_klinik')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_klinik), array('view', 'id'=>$data->id_klinik)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_klinik')); ?>:</b>
	<?php echo CHtml::encode($data->nama_klinik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_ho')); ?>:</b>
	<?php echo CHtml::encode($data->no_ho); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_ho')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_ho); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penanggung_jawab')); ?>:</b>
	<?php echo CHtml::encode($data->penanggung_jawab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pimpinan')); ?>:</b>
	<?php echo CHtml::encode($data->pimpinan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('pekerjaan')); ?>:</b>
	<?php echo CHtml::encode($data->pekerjaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_izin')); ?>:</b>
	<?php echo CHtml::encode($data->no_izin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_izin_berlaku')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_izin_berlaku); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_izin_keluar')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_izin_keluar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jns_klinik')); ?>:</b>
	<?php echo CHtml::encode($data->jns_klinik); ?>
	<br />

	*/ ?>

</div>