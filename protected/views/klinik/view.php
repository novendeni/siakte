<?php
/* @var $this KlinikController */
/* @var $model Klinik */

$this->breadcrumbs=array(
	'Kliniks'=>array('index'),
	$model->id_klinik,
);

$this->menu=array(
	array('label'=>'List Klinik', 'url'=>array('index')),
	array('label'=>'Create Klinik', 'url'=>array('create')),
	array('label'=>'Update Klinik', 'url'=>array('update', 'id'=>$model->id_klinik)),
	array('label'=>'Delete Klinik', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_klinik),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Klinik', 'url'=>array('admin')),
);
?>

<h1>View Klinik #<?php echo $model->id_klinik; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_klinik',
		'nama_klinik',
		'alamat',
		'no_ho',
		'tgl_ho',
		'penanggung_jawab',
		'pimpinan',
		'pekerjaan',
		'no_izin',
		'tgl_izin_berlaku',
		'tgl_izin_keluar',
		'jns_klinik',
	),
)); ?>
