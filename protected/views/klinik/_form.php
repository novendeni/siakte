<?php
/* @var $this KlinikController */
/* @var $model Klinik */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'klinik-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_klinik'); ?>
		<?php echo $form->textField($model,'nama_klinik',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'nama_klinik'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alamat'); ?>
		<?php echo $form->textField($model,'alamat',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'alamat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_ho'); ?>
		<?php echo $form->textField($model,'no_ho',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'no_ho'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgl_ho'); ?>
		<?php echo $form->textField($model,'tgl_ho'); ?>
		<?php echo $form->error($model,'tgl_ho'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'penanggung_jawab'); ?>
		<?php echo $form->textField($model,'penanggung_jawab',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'penanggung_jawab'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pimpinan'); ?>
		<?php echo $form->textField($model,'pimpinan',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'pimpinan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pekerjaan'); ?>
		<?php echo $form->textField($model,'pekerjaan',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'pekerjaan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_izin'); ?>
		<?php echo $form->textField($model,'no_izin',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'no_izin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgl_izin_berlaku'); ?>
		<?php echo $form->textField($model,'tgl_izin_berlaku'); ?>
		<?php echo $form->error($model,'tgl_izin_berlaku'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgl_izin_keluar'); ?>
		<?php echo $form->textField($model,'tgl_izin_keluar'); ?>
		<?php echo $form->error($model,'tgl_izin_keluar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jns_klinik'); ?>
		<?php echo $form->textField($model,'jns_klinik',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'jns_klinik'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->