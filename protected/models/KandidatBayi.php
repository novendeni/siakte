<?php

/**
 * This is the model class for table "tbl_kandidat_bayi".
 *
 * The followings are the available columns in table 'tbl_kandidat_bayi':
 * @property integer $kandidat_id
 * @property integer $pengajuan_id
 * @property integer $no_kk
 * @property string $nik_bayi
 * @property string $nik_ibu
 * @property string $nik_ayah
 * @property string $nik_pelapor
 * @property string $nik_saksi1
 * @property string $nik_saksi2
 * @property string $keterangan
 * @property integer $sk_id
 * @property string $insert_datetime
 * @property string $insert_by
 * @property string $update_datetime
 * @property string $update_by
 *
 * The followings are the available model relations:
 * @property CapilLahir[] $tblCapilLahirs
 * @property DetailKelahiran $detailKelahiran
 * @property DetailPelapor $detailPelapor
 * @property Pengajuan $pengajuan
 * @property DataKeluarga $noKk
 * @property StatusKandidat $sk
 * @property BiodataWni $nikBayi
 * @property BiodataWni $nikIbu
 * @property BiodataWni $nikAyah
 * @property BiodataWni $nikPelapor
 * @property BiodataWni $nikSaksi1
 * @property BiodataWni $nikSaksi2
 * @property StatusKandidat[] $tblStatusKandidats
 */
class KandidatBayi extends CActiveRecord
{
	// new criteria for search 
	public $kecamatanSearch;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_kandidat_bayi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pengajuan_id, no_kk, nik_ibu, nik_ayah, nik_pelapor, nik_saksi1, nik_saksi2, sk_id, insert_datetime, insert_by', 'required'),
			array('pengajuan_id, no_kk, sk_id, nik_ibu, nik_ayah, nik_pelapor, nik_saksi1, nik_saksi2', 'numerical', 'integerOnly'=>true, 'message'=>'{attribute} Harus berupa angka.'),
			array('nik_bayi, nik_ibu, nik_ayah, nik_pelapor, nik_saksi1, nik_saksi2', 'length', 'max'=>16),
			array('insert_by, update_by', 'length', 'max'=>20),
			array('update_datetime', 'safe'),
			array('nik_bayi', 'default', 'setOnEmpty'=>true, 'value'=>null),
			array('nik_ibu, nik_ayah, nik_pelapor, nik_saksi1, nik_saksi2', 'exist', 'allowEmpty'=>false, 'attributeName'=>'nik', 'className'=>'BiodataWni', 'message'=>'{attribute} tidak ditemukan.'),
			array('no_kk', 'exist', 'allowEmpty'=>false, 'attributeName'=>'no_kk', 'className'=>'DataKeluarga', 'message'=>'{attribute} tidak ditemukan.'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kandidat_id, pengajuan_id, no_kk, nik_bayi, nik_ibu, nik_ayah, nik_pelapor, nik_saksi1, nik_saksi2, sk_id, insert_datetime, insert_by, update_datetime, update_by, kecamatanSearch', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblCapilLahirs' => array(self::MANY_MANY, 'CapilLahir', 'tbl_akta_lahir_relation(kandidat_id, bayi_no)'),
			'detailKelahiran' => array(self::HAS_ONE, 'DetailKelahiran', 'kandidat_id'),
			'detailPelapor' => array(self::HAS_ONE, 'DetailPelapor', 'kandidat_id'),
			'pengajuan' => array(self::BELONGS_TO, 'Pengajuan', 'pengajuan_id'),
			'noKk' => array(self::BELONGS_TO, 'DataKeluarga', 'no_kk'),
			'sk' => array(self::BELONGS_TO, 'StatusKandidat', 'sk_id'),
			'nikBayi' => array(self::BELONGS_TO, 'BiodataWni', 'nik_bayi'),
			'nikIbu' => array(self::BELONGS_TO, 'BiodataWni', 'nik_ibu'),
			'nikAyah' => array(self::BELONGS_TO, 'BiodataWni', 'nik_ayah'),
			'nikPelapor' => array(self::BELONGS_TO, 'BiodataWni', 'nik_pelapor'),
			'nikSaksi1' => array(self::BELONGS_TO, 'BiodataWni', 'nik_saksi1'),
			'nikSaksi2' => array(self::BELONGS_TO, 'BiodataWni', 'nik_saksi2'),
			'tblStatusKandidats' => array(self::MANY_MANY, 'StatusKandidat', 'tbl_status_kandidat_history(kandidat_id, sk_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kandidat_id' => 'No. Kandidat',
			'pengajuan_id' => 'No. Pengajuan',
			'no_kk' => 'No KK',
			'nik_bayi' => 'NIK Bayi',
			'nik_ibu' => 'NIK Ibu',
			'nik_ayah' => 'NIK Ayah',
			'nik_pelapor' => 'NIK Pelapor',
			'nik_saksi1' => 'NIK Saksi1',
			'nik_saksi2' => 'NIK Saksi2',
			'sk_id' => 'Status Kandidat',
			'insert_datetime' => 'Insert Datetime',
			'insert_by' => 'Insert By',
			'update_datetime' => 'Update Datetime',
			'update_by' => 'Update By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.kandidat_id',$this->kandidat_id);
		$criteria->compare('t.pengajuan_id',$this->pengajuan_id);
		$criteria->compare('t.no_kk',$this->no_kk);
		$criteria->compare('noKk',$this->no_kk,true);
		$criteria->compare('noKk.no_kec',$this->kecamatanSearch,true);
		$criteria->compare('t.nik_bayi',$this->nik_bayi,true);
		$criteria->compare('nikBayi',$this->nik_bayi,true);
		$criteria->compare('t.nik_ibu',$this->nik_ibu,true);
		$criteria->compare('nikIbu',$this->nik_ibu,true);
		$criteria->compare('t.nik_ayah',$this->nik_ayah,true);
		$criteria->compare('nikAyah',$this->nik_ayah,true);
		$criteria->compare('t.nik_pelapor',$this->nik_pelapor,true);
		$criteria->compare('nikPelapor',$this->nik_pelapor,true);
		$criteria->compare('t.nik_saksi1',$this->nik_saksi1,true);
		//$criteria->compare('nikSaksi1',$this->nikSaksi1,true);
		$criteria->compare('t.nik_saksi2',$this->nik_saksi2,true);
		//$criteria->compare('nikSaksi2',$this->nikSaksi2,true);
		$criteria->compare('t.sk_id',$this->sk_id);
		$criteria->compare('t.insert_datetime',$this->insert_datetime,true);
		$criteria->compare('t.insert_by',$this->insert_by,true);
		$criteria->compare('t.update_datetime',$this->update_datetime,true);
		$criteria->compare('t.update_by',$this->update_by,true);
		
		//load related table
		$criteria->with=array('nikBayi', 'nikIbu', 'nikAyah', 'nikPelapor', 'noKk'); //'nikSaksi1', 'nikSaksi2'

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return KandidatBayi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
