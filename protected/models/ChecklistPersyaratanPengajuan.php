<?php

/**
 * This is the model class for table "tbl_checklist_persyaratan_pengajuan".
 *
 * The followings are the available columns in table 'tbl_checklist_persyaratan_pengajuan':
 * @property integer $pengajuan_id
 * @property integer $pa_id
 * @property integer $kuantitas
 * @property integer $isQualified
 */
class ChecklistPersyaratanPengajuan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_checklist_persyaratan_pengajuan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pengajuan_id, pa_id, kuantitas, isQualified', 'required'),
			array('pengajuan_id, pa_id, kuantitas, isQualified', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pengajuan_id, pa_id, kuantitas, isQualified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pengajuan_id' => 'Pengajuan',
			'pa_id' => 'Pa',
			'kuantitas' => 'Kuantitas',
			'isQualified' => 'Is Qualified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pengajuan_id',$this->pengajuan_id);
		$criteria->compare('pa_id',$this->pa_id);
		$criteria->compare('kuantitas',$this->kuantitas);
		$criteria->compare('isQualified',$this->isQualified);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ChecklistPersyaratanPengajuan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}	
}