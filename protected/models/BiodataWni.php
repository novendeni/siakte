<?php

/**
 * This is the model class for table "tbl_biodata_wni".
 *
 * The followings are the available columns in table 'tbl_biodata_wni':
 * @property string $nik
 * @property string $no_ktp
 * @property string $tmpt_sbl
 * @property string $no_paspor
 * @property string $tgl_akh_paspor
 * @property string $nama_lgkp
 * @property integer $jenis_klmin
 * @property string $tmpt_lhr
 * @property string $tgl_lhr
 * @property integer $akta_lhr
 * @property string $no_akta_lhr
 * @property integer $gol_drh
 * @property string $agama
 * @property integer $stat_kwn
 * @property integer $akta_kwn
 * @property string $no_akta_kwn
 * @property string $tgl_kwn
 * @property integer $akta_crai
 * @property string $no_akta_crai
 * @property string $tgl_crai
 * @property integer $stat_hbkel
 * @property integer $klain_fsk
 * @property integer $pnydng_cct
 * @property integer $pddk_akh
 * @property integer $jenis_pkrjn
 * @property string $nik_ibu
 * @property string $nama_lgkp_ibu
 * @property string $nik_ayah
 * @property string $nama_lgkp_ayah
 * @property string $nama_ket_rt
 * @property string $nama_ket_rw
 * @property string $nama_pet_reg
 * @property integer $nip_pet_reg
 * @property string $nama_pet_entri
 * @property integer $nip_pet_entri
 * @property string $tgl_entri
 * @property integer $no_kk
 * @property integer $jenis_bntu
 * @property integer $no_prop
 * @property integer $no_kab
 * @property integer $no_kec
 * @property integer $no_kel
 * @property integer $stat_hidup
 * @property string $tgl_ubah
 * @property string $tgl_cetak_ktp
 * @property string $tgl_ganti_ktp
 * @property string $tgl_pjg_ktp
 * @property integer $stat_ktp
 * @property integer $als_numpang
 * @property string $pflag
 * @property string $cflag
 * @property integer $sync_flag
 * @property string $ket_agama
 * @property string $kebangsaan
 * @property string $gelar
 * @property string $ket_pkrjn
 * @property string $glr_agama
 * @property string $glr_akademis
 * @property string $glr_bangsawan
 * @property string $is_pros_datang
 * @property string $desc_pekerjaan
 * @property string $desc_kepercayaan
 * @property string $flag_status
 * @property integer $count_ktp
 * @property integer $count_biodata
 * @property string $flagsink
 *
 * The followings are the available model relations:
 * @property DataKeluarga $noKk
 * @property DetailPelapor[] $detailPelapors
 * @property KandidatBayi[] $kandidatBayis
 * @property KandidatBayi[] $kandidatBayis1
 * @property KandidatBayi[] $kandidatBayis2
 * @property KandidatBayi[] $kandidatBayis3
 * @property KandidatBayi[] $kandidatBayis4
 * @property KandidatBayi[] $kandidatBayis5
 * @property Pengajuan[] $pengajuans
 */
class BiodataWni extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_biodata_wni';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nik, nama_lgkp, jenis_klmin, tmpt_lhr, tgl_lhr, gol_drh, agama, stat_kwn, no_prop, no_kab, no_kec, no_kel', 'required'),
			//array('jenis_klmin, akta_lhr, gol_drh, stat_kwn, akta_kwn, akta_crai, stat_hbkel, klain_fsk, pnydng_cct, pddk_akh, jenis_pkrjn, nip_pet_reg, nip_pet_entri, no_kk, jenis_bntu, no_prop, no_kab, no_kec, no_kel, stat_hidup, stat_ktp, als_numpang, sync_flag, count_ktp, count_biodata', 'numerical', 'integerOnly'=>true),
			array('nik, nik_ibu, nik_ayah', 'length', 'max'=>16),
			array('no_ktp, no_akta_lhr, no_akta_kwn, no_akta_crai', 'length', 'max'=>40),
			array('tmpt_sbl', 'length', 'max'=>300),
			array('no_paspor, desc_pekerjaan, desc_kepercayaan', 'length', 'max'=>30),
			array('nama_lgkp, tmpt_lhr, nama_lgkp_ibu, nama_lgkp_ayah, nama_ket_rt, nama_ket_rw, nama_pet_reg, nama_pet_entri, ket_agama, kebangsaan, ket_pkrjn', 'length', 'max'=>60),
			array('agama', 'length', 'max'=>20),
			array('pflag, cflag, glr_agama, glr_akademis, glr_bangsawan, is_pros_datang, flag_status', 'length', 'max'=>1),
			array('gelar', 'length', 'max'=>5),
			array('flagsink', 'length', 'max'=>50),
			array('tgl_lhr', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('nik, no_ktp, tmpt_sbl, no_paspor, tgl_akh_paspor, nama_lgkp, jenis_klmin, tmpt_lhr, tgl_lhr, akta_lhr, no_akta_lhr, gol_drh, agama, stat_kwn, akta_kwn, no_akta_kwn, tgl_kwn, akta_crai, no_akta_crai, tgl_crai, stat_hbkel, klain_fsk, pnydng_cct, pddk_akh, jenis_pkrjn, nik_ibu, nama_lgkp_ibu, nik_ayah, nama_lgkp_ayah, nama_ket_rt, nama_ket_rw, nama_pet_reg, nip_pet_reg, nama_pet_entri, nip_pet_entri, tgl_entri, no_kk, jenis_bntu, no_prop, no_kab, no_kec, no_kel, stat_hidup, tgl_ubah, tgl_cetak_ktp, tgl_ganti_ktp, tgl_pjg_ktp, stat_ktp, als_numpang, pflag, cflag, sync_flag, ket_agama, kebangsaan, gelar, ket_pkrjn, glr_agama, glr_akademis, glr_bangsawan, is_pros_datang, desc_pekerjaan, desc_kepercayaan, flag_status, count_ktp, count_biodata, flagsink', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'noKk' => array(self::BELONGS_TO, 'DataKeluarga', 'no_kk'),
			'detailPelapors' => array(self::HAS_MANY, 'DetailPelapor', 'nik'),
			'kandidatBayis' => array(self::HAS_MANY, 'KandidatBayi', 'nik_bayi'),
			'kandidatBayis1' => array(self::HAS_MANY, 'KandidatBayi', 'nik_ibu'),
			'kandidatBayis2' => array(self::HAS_MANY, 'KandidatBayi', 'nik_ayah'),
			'kandidatBayis3' => array(self::HAS_MANY, 'KandidatBayi', 'nik_pelapor'),
			'kandidatBayis4' => array(self::HAS_MANY, 'KandidatBayi', 'nik_saksi1'),
			'kandidatBayis5' => array(self::HAS_MANY, 'KandidatBayi', 'nik_saksi2'),
			'pengajuans' => array(self::HAS_MANY, 'Pengajuan', 'nik'),
                        'bidans' => array(self::HAS_MANY, 'Bidan', 'nik'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nik' => 'Nik',
			'no_ktp' => 'No Ktp',
			'tmpt_sbl' => 'Tmpt Sbl',
			'no_paspor' => 'No Paspor',
			'tgl_akh_paspor' => 'Tgl Akh Paspor',
			'nama_lgkp' => 'Nama Lgkp',
			'jenis_klmin' => 'Jenis Klmin',
			'tmpt_lhr' => 'Tmpt Lhr',
			'tgl_lhr' => 'Tgl Lhr',
			'akta_lhr' => 'Akta Lhr',
			'no_akta_lhr' => 'No Akta Lhr',
			'gol_drh' => 'Gol Drh',
			'agama' => 'Agama',
			'stat_kwn' => 'Stat Kwn',
			'akta_kwn' => 'Akta Kwn',
			'no_akta_kwn' => 'No Akta Kwn',
			'tgl_kwn' => 'Tgl Kwn',
			'akta_crai' => 'Akta Crai',
			'no_akta_crai' => 'No Akta Crai',
			'tgl_crai' => 'Tgl Crai',
			'stat_hbkel' => 'Stat Hbkel',
			'klain_fsk' => 'Klain Fsk',
			'pnydng_cct' => 'Pnydng Cct',
			'pddk_akh' => 'Pddk Akh',
			'jenis_pkrjn' => 'Jenis Pkrjn',
			'nik_ibu' => 'Nik Ibu',
			'nama_lgkp_ibu' => 'Nama Lgkp Ibu',
			'nik_ayah' => 'Nik Ayah',
			'nama_lgkp_ayah' => 'Nama Lgkp Ayah',
			'nama_ket_rt' => 'Nama Ket Rt',
			'nama_ket_rw' => 'Nama Ket Rw',
			'nama_pet_reg' => 'Nama Pet Reg',
			'nip_pet_reg' => 'Nip Pet Reg',
			'nama_pet_entri' => 'Nama Pet Entri',
			'nip_pet_entri' => 'Nip Pet Entri',
			'tgl_entri' => 'Tgl Entri',
			'no_kk' => 'No Kk',
			'jenis_bntu' => 'Jenis Bntu',
			'no_prop' => 'No Prop',
			'no_kab' => 'No Kab',
			'no_kec' => 'No Kec',
			'no_kel' => 'No Kel',
			'stat_hidup' => 'Stat Hidup',
			'tgl_ubah' => 'Tgl Ubah',
			'tgl_cetak_ktp' => 'Tgl Cetak Ktp',
			'tgl_ganti_ktp' => 'Tgl Ganti Ktp',
			'tgl_pjg_ktp' => 'Tgl Pjg Ktp',
			'stat_ktp' => 'Stat Ktp',
			'als_numpang' => 'Als Numpang',
			'pflag' => 'Pflag',
			'cflag' => 'Cflag',
			'sync_flag' => 'Sync Flag',
			'ket_agama' => 'Ket Agama',
			'kebangsaan' => 'Kebangsaan',
			'gelar' => 'Gelar',
			'ket_pkrjn' => 'Ket Pkrjn',
			'glr_agama' => 'Glr Agama',
			'glr_akademis' => 'Glr Akademis',
			'glr_bangsawan' => 'Glr Bangsawan',
			'is_pros_datang' => 'Is Pros Datang',
			'desc_pekerjaan' => 'Desc Pekerjaan',
			'desc_kepercayaan' => 'Desc Kepercayaan',
			'flag_status' => 'Flag Status',
			'count_ktp' => 'Count Ktp',
			'count_biodata' => 'Count Biodata',
			'flagsink' => 'Flagsink',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('no_ktp',$this->no_ktp,true);
		$criteria->compare('tmpt_sbl',$this->tmpt_sbl,true);
		$criteria->compare('no_paspor',$this->no_paspor,true);
		$criteria->compare('tgl_akh_paspor',$this->tgl_akh_paspor,true);
		$criteria->compare('nama_lgkp',$this->nama_lgkp,true);
		$criteria->compare('jenis_klmin',$this->jenis_klmin);
		$criteria->compare('tmpt_lhr',$this->tmpt_lhr,true);
		$criteria->compare('tgl_lhr',$this->tgl_lhr,true);
		$criteria->compare('akta_lhr',$this->akta_lhr);
		$criteria->compare('no_akta_lhr',$this->no_akta_lhr,true);
		$criteria->compare('gol_drh',$this->gol_drh);
		$criteria->compare('agama',$this->agama,true);
		$criteria->compare('stat_kwn',$this->stat_kwn);
		$criteria->compare('akta_kwn',$this->akta_kwn);
		$criteria->compare('no_akta_kwn',$this->no_akta_kwn,true);
		$criteria->compare('tgl_kwn',$this->tgl_kwn,true);
		$criteria->compare('akta_crai',$this->akta_crai);
		$criteria->compare('no_akta_crai',$this->no_akta_crai,true);
		$criteria->compare('tgl_crai',$this->tgl_crai,true);
		$criteria->compare('stat_hbkel',$this->stat_hbkel);
		$criteria->compare('klain_fsk',$this->klain_fsk);
		$criteria->compare('pnydng_cct',$this->pnydng_cct);
		$criteria->compare('pddk_akh',$this->pddk_akh);
		$criteria->compare('jenis_pkrjn',$this->jenis_pkrjn);
		$criteria->compare('nik_ibu',$this->nik_ibu,true);
		$criteria->compare('nama_lgkp_ibu',$this->nama_lgkp_ibu,true);
		$criteria->compare('nik_ayah',$this->nik_ayah,true);
		$criteria->compare('nama_lgkp_ayah',$this->nama_lgkp_ayah,true);
		$criteria->compare('nama_ket_rt',$this->nama_ket_rt,true);
		$criteria->compare('nama_ket_rw',$this->nama_ket_rw,true);
		$criteria->compare('nama_pet_reg',$this->nama_pet_reg,true);
		$criteria->compare('nip_pet_reg',$this->nip_pet_reg);
		$criteria->compare('nama_pet_entri',$this->nama_pet_entri,true);
		$criteria->compare('nip_pet_entri',$this->nip_pet_entri);
		$criteria->compare('tgl_entri',$this->tgl_entri,true);
		$criteria->compare('no_kk',$this->no_kk);
		$criteria->compare('jenis_bntu',$this->jenis_bntu);
		$criteria->compare('no_prop',$this->no_prop);
		$criteria->compare('no_kab',$this->no_kab);
		$criteria->compare('no_kec',$this->no_kec);
		$criteria->compare('no_kel',$this->no_kel);
		$criteria->compare('stat_hidup',$this->stat_hidup);
		$criteria->compare('tgl_ubah',$this->tgl_ubah,true);
		$criteria->compare('tgl_cetak_ktp',$this->tgl_cetak_ktp,true);
		$criteria->compare('tgl_ganti_ktp',$this->tgl_ganti_ktp,true);
		$criteria->compare('tgl_pjg_ktp',$this->tgl_pjg_ktp,true);
		$criteria->compare('stat_ktp',$this->stat_ktp);
		$criteria->compare('als_numpang',$this->als_numpang);
		$criteria->compare('pflag',$this->pflag,true);
		$criteria->compare('cflag',$this->cflag,true);
		$criteria->compare('sync_flag',$this->sync_flag);
		$criteria->compare('ket_agama',$this->ket_agama,true);
		$criteria->compare('kebangsaan',$this->kebangsaan,true);
		$criteria->compare('gelar',$this->gelar,true);
		$criteria->compare('ket_pkrjn',$this->ket_pkrjn,true);
		$criteria->compare('glr_agama',$this->glr_agama,true);
		$criteria->compare('glr_akademis',$this->glr_akademis,true);
		$criteria->compare('glr_bangsawan',$this->glr_bangsawan,true);
		$criteria->compare('is_pros_datang',$this->is_pros_datang,true);
		$criteria->compare('desc_pekerjaan',$this->desc_pekerjaan,true);
		$criteria->compare('desc_kepercayaan',$this->desc_kepercayaan,true);
		$criteria->compare('flag_status',$this->flag_status,true);
		$criteria->compare('count_ktp',$this->count_ktp);
		$criteria->compare('count_biodata',$this->count_biodata);
		$criteria->compare('flagsink',$this->flagsink,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BiodataWni the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
