<?php

/**
 * This is the model class for table "tbl_bidan".
 *
 * The followings are the available columns in table 'tbl_bidan':
 * @property string $nik
 * @property string $nama
 * @property string $tmpt_lahir
 * @property string $tgl_lahir
 * @property string $alamat
 * @property string $alamat_praktek
 *
 * The followings are the available model relations:
 * @property BiodataWni $nik0
 */
class Bidan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_bidan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nik, nama, tmpt_lahir, tgl_lahir, alamat, alamat_praktek', 'required'),
			array('nik', 'length', 'max'=>16),
			array('nama, tmpt_lahir', 'length', 'max'=>40),
			array('alamat, alamat_praktek', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('nik, nama, tmpt_lahir, tgl_lahir, alamat, alamat_praktek', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nik0' => array(self::BELONGS_TO, 'BiodataWni', 'nik'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nik' => 'Nik',
			'nama' => 'Nama',
			'tmpt_lahir' => 'Tmpt Lahir',
			'tgl_lahir' => 'Tgl Lahir',
			'alamat' => 'Alamat',
			'alamat_praktek' => 'Alamat Praktek',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('tmpt_lahir',$this->tmpt_lahir,true);
		$criteria->compare('tgl_lahir',$this->tgl_lahir,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('alamat_praktek',$this->alamat_praktek,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bidan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
