<?php

/**
 * This is the model class for table "tbl_detail_kelahiran".
 *
 * The followings are the available columns in table 'tbl_detail_kelahiran':
 * @property integer $kandidat_id
 * @property string $nama
 * @property string $tanggal_lahir
 * @property string $waktu_lahir
 * @property string $jenis_kelamin
 * @property string $jenis_kelahiran
 * @property string $tempat_lahir
 * @property integer $anak_ke
 * @property integer $berat
 * @property integer $panjang
 * @property string $penolong_kelahiran
 *
 * The followings are the available model relations:
 * @property KandidatBayi $kandidat
 */
class DetailKelahiran extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_detail_kelahiran';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kandidat_id, nama, tanggal_lahir, waktu_lahir, jenis_kelamin, jenis_kelahiran, tempat_lahir, anak_ke, berat, panjang, penolong_kelahiran', 'required'),
			array('kandidat_id, anak_ke, berat, panjang', 'numerical', 'integerOnly'=>true),
			array('nama, tempat_lahir', 'length', 'max'=>40),
			array('jenis_kelamin, penolong_kelahiran', 'length', 'max'=>20),
			array('jenis_kelahiran', 'length', 'max'=>12),
			array('tanggal_lahir, waktu_lahir', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kandidat_id, nama, tanggal_lahir, waktu_lahir, jenis_kelamin, jenis_kelahiran, tempat_lahir, anak_ke, berat, panjang, penolong_kelahiran', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kandidat' => array(self::BELONGS_TO, 'KandidatBayi', 'kandidat_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kandidat_id' => 'Kandidat',
			'nama' => 'Nama',
			'tanggal_lahir' => 'Tanggal Lahir',
			'waktu_lahir' => 'Waktu Lahir',
			'jenis_kelamin' => 'Jenis Kelamin',
			'jenis_kelahiran' => 'Jenis Kelahiran',
			'tempat_lahir' => 'Tempat Lahir',
			'anak_ke' => 'Anak Ke',
			'berat' => 'Berat',
			'panjang' => 'Panjang',
			'penolong_kelahiran' => 'Penolong Kelahiran',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kandidat_id',$this->kandidat_id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('waktu_lahir',$this->waktu_lahir,true);
		$criteria->compare('jenis_kelamin',$this->jenis_kelamin,true);
		$criteria->compare('jenis_kelahiran',$this->jenis_kelahiran,true);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('anak_ke',$this->anak_ke);
		$criteria->compare('berat',$this->berat);
		$criteria->compare('panjang',$this->panjang);
		$criteria->compare('penolong_kelahiran',$this->penolong_kelahiran,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DetailKelahiran the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
