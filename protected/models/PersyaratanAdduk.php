<?php

/**
 * This is the model class for table "tbl_persyaratan_adduk".
 *
 * The followings are the available columns in table 'tbl_persyaratan_adduk':
 * @property integer $pa_id
 * @property integer $ad_id
 * @property integer $persyaratan_id
 * @property integer $kuatitas
 * @property integer $isWajib
 *
 * The followings are the available model relations:
 * @property Pengajuan[] $tblPengajuans
 * @property AdministrasiKependudukan $ad
 * @property Persyaratan $persyaratan
 */
class PersyaratanAdduk extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_persyaratan_adduk';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ad_id, persyaratan_id, kuatitas, isWajib', 'required'),
			array('ad_id, persyaratan_id, kuatitas, isWajib', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pa_id, ad_id, persyaratan_id, kuatitas, isWajib', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblPengajuans' => array(self::MANY_MANY, 'Pengajuan', 'tbl_checklist_persyaratan_pengajuan(pa_id, pengajuan_id)'),
			'ad' => array(self::BELONGS_TO, 'AdministrasiKependudukan', 'ad_id'),
			'persyaratan' => array(self::BELONGS_TO, 'Persyaratan', 'persyaratan_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pa_id' => 'Pa',
			'ad_id' => 'Ad',
			'persyaratan_id' => 'Persyaratan',
			'kuatitas' => 'Kuatitas',
			'isWajib' => 'Is Wajib',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pa_id',$this->pa_id);
		$criteria->compare('ad_id',$this->ad_id);
		$criteria->compare('persyaratan_id',$this->persyaratan_id);
		$criteria->compare('kuatitas',$this->kuatitas);
		$criteria->compare('isWajib',$this->isWajib);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PersyaratanAdduk the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
