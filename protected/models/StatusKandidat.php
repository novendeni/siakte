<?php

/**
 * This is the model class for table "tbl_status_kandidat".
 *
 * The followings are the available columns in table 'tbl_status_kandidat':
 * @property integer $sk_id
 * @property string $judul
 *
 * The followings are the available model relations:
 * @property KandidatBayi[] $kandidatBayis
 * @property KandidatBayi[] $tblKandidatBayis
 */
class StatusKandidat extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_status_kandidat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('judul', 'required'),
			array('judul', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('sk_id, judul', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kandidatBayis' => array(self::HAS_MANY, 'KandidatBayi', 'sk_id'),
			'tblKandidatBayis' => array(self::MANY_MANY, 'KandidatBayi', 'tbl_status_kandidat_history(sk_id, kandidat_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'sk_id' => 'Sk',
			'judul' => 'Judul',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('sk_id',$this->sk_id);
		$criteria->compare('judul',$this->judul,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StatusKandidat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
