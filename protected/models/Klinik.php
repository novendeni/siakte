<?php

/**
 * This is the model class for table "tbl_klinik".
 *
 * The followings are the available columns in table 'tbl_klinik':
 * @property integer $id_klinik
 * @property string $nama_klinik
 * @property string $alamat
 * @property string $no_ho
 * @property string $tgl_ho
 * @property string $penanggung_jawab
 * @property string $pimpinan
 * @property string $pekerjaan
 * @property string $no_izin
 * @property string $tgl_izin_berlaku
 * @property string $tgl_izin_keluar
 * @property string $jns_klinik
 */
class Klinik extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_klinik';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_klinik, alamat', 'required'),
			array('nama_klinik, pekerjaan', 'length', 'max'=>80),
			array('alamat', 'length', 'max'=>128),
			array('no_ho, jns_klinik', 'length', 'max'=>40),
			array('penanggung_jawab, no_izin', 'length', 'max'=>50),
			array('pimpinan', 'length', 'max'=>60),
			array('tgl_ho, tgl_izin_berlaku, tgl_izin_keluar', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_klinik, nama_klinik, alamat, no_ho, tgl_ho, penanggung_jawab, pimpinan, pekerjaan, no_izin, tgl_izin_berlaku, tgl_izin_keluar, jns_klinik', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_klinik' => 'Id Klinik',
			'nama_klinik' => 'Nama Klinik',
			'alamat' => 'Alamat',
			'no_ho' => 'No Ho',
			'tgl_ho' => 'Tgl Ho',
			'penanggung_jawab' => 'Penanggung Jawab',
			'pimpinan' => 'Pimpinan',
			'pekerjaan' => 'Pekerjaan',
			'no_izin' => 'No Izin',
			'tgl_izin_berlaku' => 'Tgl Izin Berlaku',
			'tgl_izin_keluar' => 'Tgl Izin Keluar',
			'jns_klinik' => 'Jns Klinik',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_klinik',$this->id_klinik);
		$criteria->compare('nama_klinik',$this->nama_klinik,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('no_ho',$this->no_ho,true);
		$criteria->compare('tgl_ho',$this->tgl_ho,true);
		$criteria->compare('penanggung_jawab',$this->penanggung_jawab,true);
		$criteria->compare('pimpinan',$this->pimpinan,true);
		$criteria->compare('pekerjaan',$this->pekerjaan,true);
		$criteria->compare('no_izin',$this->no_izin,true);
		$criteria->compare('tgl_izin_berlaku',$this->tgl_izin_berlaku,true);
		$criteria->compare('tgl_izin_keluar',$this->tgl_izin_keluar,true);
		$criteria->compare('jns_klinik',$this->jns_klinik,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Klinik the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
