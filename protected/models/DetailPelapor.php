<?php

/**
 * This is the model class for table "tbl_detail_pelapor".
 *
 * The followings are the available columns in table 'tbl_detail_pelapor':
 * @property integer $kandidat_id
 * @property string $tanggal_lapor
 * @property string $hubungan_keluarga
 * @property string $nik
 *
 * The followings are the available model relations:
 * @property KandidatBayi $kandidat
 * @property BiodataWni $nik0
 */
class DetailPelapor extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_detail_pelapor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kandidat_id, tanggal_lapor, hubungan_keluarga, nik', 'required'),
			array('kandidat_id', 'numerical', 'integerOnly'=>true),
			array('hubungan_keluarga', 'length', 'max'=>40),
			array('nik', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kandidat_id, tanggal_lapor, hubungan_keluarga, nik', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kandidat' => array(self::BELONGS_TO, 'KandidatBayi', 'kandidat_id'),
			'nik0' => array(self::BELONGS_TO, 'BiodataWni', 'nik'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kandidat_id' => 'Nomor Kandidat',
			'tanggal_lapor' => 'Tanggal Lapor',
			'hubungan_keluarga' => 'Hubungan Keluarga',
			'nik' => 'NIK',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kandidat_id',$this->kandidat_id);
		$criteria->compare('tanggal_lapor',$this->tanggal_lapor,true);
		$criteria->compare('hubungan_keluarga',$this->hubungan_keluarga,true);
		$criteria->compare('nik',$this->nik,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DetailPelapor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
