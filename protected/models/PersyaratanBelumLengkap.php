<?php

/**
 * This is the model class for table "tbl_persyaratan_belum_lengkap".
 *
 * The followings are the available columns in table 'tbl_persyaratan_belum_lengkap':
 * @property integer $pengajuan_id
 * @property string $jenis
 * @property string $nama_lengkap
 */
class PersyaratanBelumLengkap extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_persyaratan_belum_lengkap';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pengajuan_id, jenis, nama_lengkap', 'required'),
			array('pengajuan_id', 'numerical', 'integerOnly'=>true),
			array('jenis, nama_lengkap', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pengajuan_id, jenis, nama_lengkap', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pengajuan_id' => 'Pengajuan',
			'jenis' => 'Jenis',
			'nama_lengkap' => 'Nama Lengkap',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pengajuan_id',$this->pengajuan_id);
		$criteria->compare('jenis',$this->jenis,true);
		$criteria->compare('nama_lengkap',$this->nama_lengkap,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PersyaratanBelumLengkap the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
