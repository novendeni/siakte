<?php

/**
 * This is the model class for table "tbl_data_keluarga".
 *
 * The followings are the available columns in table 'tbl_data_keluarga':
 * @property integer $no_kk
 * @property string $nama_kep
 * @property string $alamat
 * @property integer $no_rt
 * @property integer $no_rw
 * @property string $dusun
 * @property integer $kode_pos
 * @property string $telp
 * @property integer $als_prmohon
 * @property integer $als_numpang
 * @property integer $no_prop
 * @property integer $no_kab
 * @property integer $no_kec
 * @property integer $no_kel
 * @property integer $userid
 * @property string $tgl_insertion
 * @property string $tgl_updation
 * @property string $pflag
 * @property string $cflag
 * @property integer $sync_flag
 * @property string $oa_nama_pertama
 * @property string $oa_nama_keluarga
 * @property string $tipe_kk
 * @property string $nik_kk
 * @property integer $count_kk
 * @property string $flagsink
 * @property string $tgl_siak_plus
 *
 * The followings are the available model relations:
 * @property BiodataWni[] $biodataWnis
 * @property KandidatBayi[] $kandidatBayis
 */
class DataKeluarga extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_data_keluarga';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('no_kk, nama_kep, alamat, no_rt, no_rw, dusun, kode_pos, telp, als_prmohon, als_numpang, userid, tgl_insertion, tgl_updation, pflag, cflag, sync_flag, oa_nama_pertama, oa_nama_keluarga, tipe_kk, nik_kk, count_kk, flagsink, tgl_siak_plus', 'required'),
			array('no_kk, no_rt, no_rw, kode_pos, als_prmohon, als_numpang, no_prop, no_kab, no_kec, no_kel, userid, sync_flag, count_kk', 'numerical', 'integerOnly'=>true),
			array('nama_kep, dusun, oa_nama_pertama, oa_nama_keluarga', 'length', 'max'=>60),
			array('alamat', 'length', 'max'=>120),
			array('telp', 'length', 'max'=>30),
			array('pflag, cflag, tipe_kk', 'length', 'max'=>1),
			array('nik_kk', 'length', 'max'=>16),
			array('flagsink', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('no_kk, nama_kep, alamat, no_rt, no_rw, dusun, kode_pos, telp, als_prmohon, als_numpang, no_prop, no_kab, no_kec, no_kel, userid, tgl_insertion, tgl_updation, pflag, cflag, sync_flag, oa_nama_pertama, oa_nama_keluarga, tipe_kk, nik_kk, count_kk, flagsink, tgl_siak_plus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'biodataWnis' => array(self::HAS_MANY, 'BiodataWni', 'no_kk'),
			'kandidatBayis' => array(self::HAS_MANY, 'KandidatBayi', 'no_kk'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'no_kk' => 'No Kk',
			'nama_kep' => 'Nama Kep',
			'alamat' => 'Alamat',
			'no_rt' => 'No Rt',
			'no_rw' => 'No Rw',
			'dusun' => 'Dusun',
			'kode_pos' => 'Kode Pos',
			'telp' => 'Telp',
			'als_prmohon' => 'Als Prmohon',
			'als_numpang' => 'Als Numpang',
			'no_prop' => 'No Prop',
			'no_kab' => 'No Kab',
			'no_kec' => 'No Kec',
			'no_kel' => 'No Kel',
			'userid' => 'Userid',
			'tgl_insertion' => 'Tgl Insertion',
			'tgl_updation' => 'Tgl Updation',
			'pflag' => 'Pflag',
			'cflag' => 'Cflag',
			'sync_flag' => 'Sync Flag',
			'oa_nama_pertama' => 'Oa Nama Pertama',
			'oa_nama_keluarga' => 'Oa Nama Keluarga',
			'tipe_kk' => 'Tipe Kk',
			'nik_kk' => 'Nik Kk',
			'count_kk' => 'Count Kk',
			'flagsink' => 'Flagsink',
			'tgl_siak_plus' => 'Tgl Siak Plus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('no_kk',$this->no_kk);
		$criteria->compare('nama_kep',$this->nama_kep,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('no_rt',$this->no_rt);
		$criteria->compare('no_rw',$this->no_rw);
		$criteria->compare('dusun',$this->dusun,true);
		$criteria->compare('kode_pos',$this->kode_pos);
		$criteria->compare('telp',$this->telp,true);
		$criteria->compare('als_prmohon',$this->als_prmohon);
		$criteria->compare('als_numpang',$this->als_numpang);
		$criteria->compare('no_prop',$this->no_prop);
		$criteria->compare('no_kab',$this->no_kab);
		$criteria->compare('no_kec',$this->no_kec);
		$criteria->compare('no_kel',$this->no_kel);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tgl_insertion',$this->tgl_insertion,true);
		$criteria->compare('tgl_updation',$this->tgl_updation,true);
		$criteria->compare('pflag',$this->pflag,true);
		$criteria->compare('cflag',$this->cflag,true);
		$criteria->compare('sync_flag',$this->sync_flag);
		$criteria->compare('oa_nama_pertama',$this->oa_nama_pertama,true);
		$criteria->compare('oa_nama_keluarga',$this->oa_nama_keluarga,true);
		$criteria->compare('tipe_kk',$this->tipe_kk,true);
		$criteria->compare('nik_kk',$this->nik_kk,true);
		$criteria->compare('count_kk',$this->count_kk);
		$criteria->compare('flagsink',$this->flagsink,true);
		$criteria->compare('tgl_siak_plus',$this->tgl_siak_plus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DataKeluarga the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
