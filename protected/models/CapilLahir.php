<?php

/**
 * This is the model class for table "tbl_capil_lahir".
 *
 * The followings are the available columns in table 'tbl_capil_lahir':
 * @property integer $bayi_no
 * @property string $bayi_org_asing
 * @property string $bayi_nik
 * @property string $bayi_nama_lgkp
 * @property string $bayi_tmpt_lahir
 * @property string $bayi_tgl_lahir
 * @property string $bayi_wkt_lahir
 * @property string $bayi_jns_kelamin
 * @property string $bayi_jns_klhr
 * @property string $bayi_tmpt_klhr
 * @property string $bayi_anak_ke
 * @property string $bayi_berat
 * @property string $bayi_panjang
 * @property string $bayi_pnlg_klhr
 * @property string $bayi_dom_lahir
 * @property string $bayi_ln_lahir
 * @property string $no_kk
 * @property string $nama_kk
 * @property string $ibu_nik
 * @property string $ibu_nama_lgkp
 * @property string $ibu_tgl_lahir
 * @property string $ibu_tgl_kawin
 * @property string $ibu_wrg_ngr
 * @property string $ibu_pekerjaan
 * @property string $ibu_kebangsaan
 * @property string $ibu_alamat
 * @property string $ibu_no_rt
 * @property string $ibu_no_rw
 * @property string $ibu_no_prov
 * @property string $ibu_no_kab
 * @property string $ibu_no_kec
 * @property string $ibu_no_kel
 * @property string $ayah_nik
 * @property string $ayah_nama_lgkp
 * @property string $ayah_tgl_lahir
 * @property string $ayah_wrg_ngr
 * @property string $ayah_pekerjaan
 * @property string $ayah_kebangsaan
 * @property string $ayah_alamat
 * @property string $ayah_no_rt
 * @property string $ayah_no_rw
 * @property string $ayah_no_prov
 * @property string $ayah_no_kab
 * @property string $ayah_no_kec
 * @property string $ayah_no_kel
 * @property string $plpr_nik
 * @property string $plpr_tgl_lapor
 * @property string $plpr_umur
 * @property string $plpr_nama_lgkp
 * @property string $plpr_kelamin
 * @property string $plpr_pekerjaan
 * @property string $plpr_alamat
 * @property string $plpr_no_rt
 * @property string $plpr_no_rw
 * @property string $plpr_no_prov
 * @property string $plpr_no_kab
 * @property string $plpr_no_kec
 * @property string $plpr_no_kel
 * @property string $plpr_hub_kel
 * @property string $saksi1_nik
 * @property string $saksi1_nama_lgkp
 * @property string $saksi1_umur
 * @property string $saksi1_kelamin
 * @property string $saksi1_pekerjaan
 * @property string $saksi1_alamat
 * @property string $saksi1_no_rt
 * @property string $saksi1_no_rw
 * @property string $saksi1_no_prov
 * @property string $saksi1_no_kab
 * @property string $saksi1_no_kec
 * @property string $saksi1_no_kel
 * @property string $saksi2_nik
 * @property string $saksi2_nama_lgkp
 * @property string $saksi2_umur
 * @property string $saksi2_kelamin
 * @property string $saksi2_pekerjaan
 * @property string $saksi2_alamat
 * @property string $saksi2_no_rt
 * @property string $saksi2_no_rw
 * @property string $saksi2_kode_pos
 * @property string $saksi2_no_prov
 * @property string $saksi2_no_kab
 * @property string $saksi2_no_kec
 * @property string $saksi2_no_kel
 * @property string $adm_no_kedubes
 * @property string $adm_no_konjen
 * @property string $adm_no_konsul
 * @property string $adm_no_prov
 * @property string $adm_no_kab
 * @property string $adm_no_kec
 * @property string $adm_no_kel
 * @property string $adm_akta_no
 * @property string $adm_dokumen
 * @property string $adm_sp_no
 * @property string $adm_sp_tgl
 * @property string $adm_sp_nama
 * @property string $adm_akta_tgl
 * @property string $adm_akta_ktp_tgl
 * @property string $adm_kades_nama
 * @property string $adm_kades_nip
 * @property string $adm_regs_nama
 * @property string $adm_regs_nip
 * @property string $adm_tgl_entry
 * @property string $adm_tgl_update
 * @property string $user_id
 * @property string $flag_update
 * @property string $flag_ctk_akta
 * @property string $flag_ctk_kakta
 * @property string $flag_ctk_sk
 * @property string $flag_status
 *
 * The followings are the available model relations:
 * @property KandidatBayi[] $tblKandidatBayis
 */
class CapilLahir extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_capil_lahir';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bayi_org_asing, bayi_dom_lahir, bayi_ln_lahir', 'length', 'max'=>1),
			array('bayi_nik, no_kk, ibu_nik, ayah_nik, plpr_nik, saksi1_nik, saksi2_nik, adm_kades_nip, adm_regs_nip', 'length', 'max'=>16),
			array('bayi_nama_lgkp, bayi_tmpt_lahir, ibu_nama_lgkp, ibu_alamat, ayah_nama_lgkp, ayah_alamat, plpr_nama_lgkp, plpr_alamat, saksi1_nama_lgkp, saksi1_alamat, saksi2_nama_lgkp, saksi2_alamat, adm_kades_nama, adm_regs_nama', 'length', 'max'=>128),
			array('bayi_tgl_lahir, ibu_tgl_lahir, ibu_tgl_kawin, ayah_tgl_lahir, plpr_tgl_lapor, adm_sp_tgl, adm_akta_tgl, adm_akta_ktp_tgl, adm_tgl_entry, adm_tgl_update', 'length', 'max'=>10),
			array('bayi_wkt_lahir', 'length', 'max'=>6),
			array('bayi_jns_kelamin, bayi_jns_klhr, bayi_tmpt_klhr, bayi_anak_ke, bayi_berat, bayi_panjang, bayi_pnlg_klhr, ibu_wrg_ngr, ibu_pekerjaan, ibu_no_prov, ibu_no_kab, ibu_no_kec, ibu_no_kel, ayah_wrg_ngr, ayah_pekerjaan, ayah_no_prov, ayah_no_kab, ayah_no_kec, ayah_no_kel, plpr_umur, plpr_kelamin, plpr_pekerjaan, plpr_no_prov, plpr_no_kab, plpr_no_kec, plpr_no_kel, saksi1_umur, saksi1_kelamin, saksi1_pekerjaan, saksi1_no_prov, saksi1_no_kab, saksi1_no_kec, saksi1_no_kel, saksi2_umur, saksi2_kelamin, saksi2_pekerjaan, saksi2_kode_pos, saksi2_no_prov, saksi2_no_kab, saksi2_no_kec, saksi2_no_kel, adm_no_kedubes, adm_no_konjen, adm_no_konsul, adm_no_prov, adm_no_kab, adm_no_kec, adm_no_kel, flag_update, flag_ctk_akta, flag_ctk_kakta, flag_ctk_sk, flag_status', 'length', 'max'=>22),
			array('nama_kk', 'length', 'max'=>65),
			array('ibu_kebangsaan, ayah_kebangsaan, plpr_hub_kel, adm_akta_no, adm_sp_no', 'length', 'max'=>64),
			array('ibu_no_rt, ibu_no_rw, ayah_no_rt, ayah_no_rw, plpr_no_rt, plpr_no_rw, saksi1_no_rt, saksi1_no_rw, saksi2_no_rt, saksi2_no_rw', 'length', 'max'=>2),
			array('adm_dokumen, adm_sp_nama', 'length', 'max'=>255),
			array('user_id', 'length', 'max'=>24),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('bayi_no, bayi_org_asing, bayi_nik, bayi_nama_lgkp, bayi_tmpt_lahir, bayi_tgl_lahir, bayi_wkt_lahir, bayi_jns_kelamin, bayi_jns_klhr, bayi_tmpt_klhr, bayi_anak_ke, bayi_berat, bayi_panjang, bayi_pnlg_klhr, bayi_dom_lahir, bayi_ln_lahir, no_kk, nama_kk, ibu_nik, ibu_nama_lgkp, ibu_tgl_lahir, ibu_tgl_kawin, ibu_wrg_ngr, ibu_pekerjaan, ibu_kebangsaan, ibu_alamat, ibu_no_rt, ibu_no_rw, ibu_no_prov, ibu_no_kab, ibu_no_kec, ibu_no_kel, ayah_nik, ayah_nama_lgkp, ayah_tgl_lahir, ayah_wrg_ngr, ayah_pekerjaan, ayah_kebangsaan, ayah_alamat, ayah_no_rt, ayah_no_rw, ayah_no_prov, ayah_no_kab, ayah_no_kec, ayah_no_kel, plpr_nik, plpr_tgl_lapor, plpr_umur, plpr_nama_lgkp, plpr_kelamin, plpr_pekerjaan, plpr_alamat, plpr_no_rt, plpr_no_rw, plpr_no_prov, plpr_no_kab, plpr_no_kec, plpr_no_kel, plpr_hub_kel, saksi1_nik, saksi1_nama_lgkp, saksi1_umur, saksi1_kelamin, saksi1_pekerjaan, saksi1_alamat, saksi1_no_rt, saksi1_no_rw, saksi1_no_prov, saksi1_no_kab, saksi1_no_kec, saksi1_no_kel, saksi2_nik, saksi2_nama_lgkp, saksi2_umur, saksi2_kelamin, saksi2_pekerjaan, saksi2_alamat, saksi2_no_rt, saksi2_no_rw, saksi2_kode_pos, saksi2_no_prov, saksi2_no_kab, saksi2_no_kec, saksi2_no_kel, adm_no_kedubes, adm_no_konjen, adm_no_konsul, adm_no_prov, adm_no_kab, adm_no_kec, adm_no_kel, adm_akta_no, adm_dokumen, adm_sp_no, adm_sp_tgl, adm_sp_nama, adm_akta_tgl, adm_akta_ktp_tgl, adm_kades_nama, adm_kades_nip, adm_regs_nama, adm_regs_nip, adm_tgl_entry, adm_tgl_update, user_id, flag_update, flag_ctk_akta, flag_ctk_kakta, flag_ctk_sk, flag_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblKandidatBayis' => array(self::MANY_MANY, 'KandidatBayi', 'tbl_akta_lahir_relation(bayi_no, kandidat_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bayi_no' => 'Bayi No',
			'bayi_org_asing' => 'Bayi Org Asing',
			'bayi_nik' => 'Bayi Nik',
			'bayi_nama_lgkp' => 'Bayi Nama Lgkp',
			'bayi_tmpt_lahir' => 'Bayi Tmpt Lahir',
			'bayi_tgl_lahir' => 'Bayi Tgl Lahir',
			'bayi_wkt_lahir' => 'Bayi Wkt Lahir',
			'bayi_jns_kelamin' => 'Bayi Jns Kelamin',
			'bayi_jns_klhr' => 'Bayi Jns Klhr',
			'bayi_tmpt_klhr' => 'Bayi Tmpt Klhr',
			'bayi_anak_ke' => 'Bayi Anak Ke',
			'bayi_berat' => 'Bayi Berat',
			'bayi_panjang' => 'Bayi Panjang',
			'bayi_pnlg_klhr' => 'Bayi Pnlg Klhr',
			'bayi_dom_lahir' => 'Bayi Dom Lahir',
			'bayi_ln_lahir' => 'Bayi Ln Lahir',
			'no_kk' => 'No Kk',
			'nama_kk' => 'Nama Kk',
			'ibu_nik' => 'Ibu Nik',
			'ibu_nama_lgkp' => 'Ibu Nama Lgkp',
			'ibu_tgl_lahir' => 'Ibu Tgl Lahir',
			'ibu_tgl_kawin' => 'Ibu Tgl Kawin',
			'ibu_wrg_ngr' => 'Ibu Wrg Ngr',
			'ibu_pekerjaan' => 'Ibu Pekerjaan',
			'ibu_kebangsaan' => 'Ibu Kebangsaan',
			'ibu_alamat' => 'Ibu Alamat',
			'ibu_no_rt' => 'Ibu No Rt',
			'ibu_no_rw' => 'Ibu No Rw',
			'ibu_no_prov' => 'Ibu No Prov',
			'ibu_no_kab' => 'Ibu No Kab',
			'ibu_no_kec' => 'Ibu No Kec',
			'ibu_no_kel' => 'Ibu No Kel',
			'ayah_nik' => 'Ayah Nik',
			'ayah_nama_lgkp' => 'Ayah Nama Lgkp',
			'ayah_tgl_lahir' => 'Ayah Tgl Lahir',
			'ayah_wrg_ngr' => 'Ayah Wrg Ngr',
			'ayah_pekerjaan' => 'Ayah Pekerjaan',
			'ayah_kebangsaan' => 'Ayah Kebangsaan',
			'ayah_alamat' => 'Ayah Alamat',
			'ayah_no_rt' => 'Ayah No Rt',
			'ayah_no_rw' => 'Ayah No Rw',
			'ayah_no_prov' => 'Ayah No Prov',
			'ayah_no_kab' => 'Ayah No Kab',
			'ayah_no_kec' => 'Ayah No Kec',
			'ayah_no_kel' => 'Ayah No Kel',
			'plpr_nik' => 'Plpr Nik',
			'plpr_tgl_lapor' => 'Plpr Tgl Lapor',
			'plpr_umur' => 'Plpr Umur',
			'plpr_nama_lgkp' => 'Plpr Nama Lgkp',
			'plpr_kelamin' => 'Plpr Kelamin',
			'plpr_pekerjaan' => 'Plpr Pekerjaan',
			'plpr_alamat' => 'Plpr Alamat',
			'plpr_no_rt' => 'Plpr No Rt',
			'plpr_no_rw' => 'Plpr No Rw',
			'plpr_no_prov' => 'Plpr No Prov',
			'plpr_no_kab' => 'Plpr No Kab',
			'plpr_no_kec' => 'Plpr No Kec',
			'plpr_no_kel' => 'Plpr No Kel',
			'plpr_hub_kel' => 'Plpr Hub Kel',
			'saksi1_nik' => 'Saksi1 Nik',
			'saksi1_nama_lgkp' => 'Saksi1 Nama Lgkp',
			'saksi1_umur' => 'Saksi1 Umur',
			'saksi1_kelamin' => 'Saksi1 Kelamin',
			'saksi1_pekerjaan' => 'Saksi1 Pekerjaan',
			'saksi1_alamat' => 'Saksi1 Alamat',
			'saksi1_no_rt' => 'Saksi1 No Rt',
			'saksi1_no_rw' => 'Saksi1 No Rw',
			'saksi1_no_prov' => 'Saksi1 No Prov',
			'saksi1_no_kab' => 'Saksi1 No Kab',
			'saksi1_no_kec' => 'Saksi1 No Kec',
			'saksi1_no_kel' => 'Saksi1 No Kel',
			'saksi2_nik' => 'Saksi2 Nik',
			'saksi2_nama_lgkp' => 'Saksi2 Nama Lgkp',
			'saksi2_umur' => 'Saksi2 Umur',
			'saksi2_kelamin' => 'Saksi2 Kelamin',
			'saksi2_pekerjaan' => 'Saksi2 Pekerjaan',
			'saksi2_alamat' => 'Saksi2 Alamat',
			'saksi2_no_rt' => 'Saksi2 No Rt',
			'saksi2_no_rw' => 'Saksi2 No Rw',
			'saksi2_kode_pos' => 'Saksi2 Kode Pos',
			'saksi2_no_prov' => 'Saksi2 No Prov',
			'saksi2_no_kab' => 'Saksi2 No Kab',
			'saksi2_no_kec' => 'Saksi2 No Kec',
			'saksi2_no_kel' => 'Saksi2 No Kel',
			'adm_no_kedubes' => 'Adm No Kedubes',
			'adm_no_konjen' => 'Adm No Konjen',
			'adm_no_konsul' => 'Adm No Konsul',
			'adm_no_prov' => 'Adm No Prov',
			'adm_no_kab' => 'Adm No Kab',
			'adm_no_kec' => 'Adm No Kec',
			'adm_no_kel' => 'Adm No Kel',
			'adm_akta_no' => 'Adm Akta No',
			'adm_dokumen' => 'Adm Dokumen',
			'adm_sp_no' => 'Adm Sp No',
			'adm_sp_tgl' => 'Adm Sp Tgl',
			'adm_sp_nama' => 'Adm Sp Nama',
			'adm_akta_tgl' => 'Adm Akta Tgl',
			'adm_akta_ktp_tgl' => 'Adm Akta Ktp Tgl',
			'adm_kades_nama' => 'Adm Kades Nama',
			'adm_kades_nip' => 'Adm Kades Nip',
			'adm_regs_nama' => 'Adm Regs Nama',
			'adm_regs_nip' => 'Adm Regs Nip',
			'adm_tgl_entry' => 'Adm Tgl Entry',
			'adm_tgl_update' => 'Adm Tgl Update',
			'user_id' => 'User',
			'flag_update' => 'Flag Update',
			'flag_ctk_akta' => 'Flag Ctk Akta',
			'flag_ctk_kakta' => 'Flag Ctk Kakta',
			'flag_ctk_sk' => 'Flag Ctk Sk',
			'flag_status' => 'Flag Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('bayi_no',$this->bayi_no);
		$criteria->compare('bayi_org_asing',$this->bayi_org_asing,true);
		$criteria->compare('bayi_nik',$this->bayi_nik,true);
		$criteria->compare('bayi_nama_lgkp',$this->bayi_nama_lgkp,true);
		$criteria->compare('bayi_tmpt_lahir',$this->bayi_tmpt_lahir,true);
		$criteria->compare('bayi_tgl_lahir',$this->bayi_tgl_lahir,true);
		$criteria->compare('bayi_wkt_lahir',$this->bayi_wkt_lahir,true);
		$criteria->compare('bayi_jns_kelamin',$this->bayi_jns_kelamin,true);
		$criteria->compare('bayi_jns_klhr',$this->bayi_jns_klhr,true);
		$criteria->compare('bayi_tmpt_klhr',$this->bayi_tmpt_klhr,true);
		$criteria->compare('bayi_anak_ke',$this->bayi_anak_ke,true);
		$criteria->compare('bayi_berat',$this->bayi_berat,true);
		$criteria->compare('bayi_panjang',$this->bayi_panjang,true);
		$criteria->compare('bayi_pnlg_klhr',$this->bayi_pnlg_klhr,true);
		$criteria->compare('bayi_dom_lahir',$this->bayi_dom_lahir,true);
		$criteria->compare('bayi_ln_lahir',$this->bayi_ln_lahir,true);
		$criteria->compare('no_kk',$this->no_kk,true);
		$criteria->compare('nama_kk',$this->nama_kk,true);
		$criteria->compare('ibu_nik',$this->ibu_nik,true);
		$criteria->compare('ibu_nama_lgkp',$this->ibu_nama_lgkp,true);
		$criteria->compare('ibu_tgl_lahir',$this->ibu_tgl_lahir,true);
		$criteria->compare('ibu_tgl_kawin',$this->ibu_tgl_kawin,true);
		$criteria->compare('ibu_wrg_ngr',$this->ibu_wrg_ngr,true);
		$criteria->compare('ibu_pekerjaan',$this->ibu_pekerjaan,true);
		$criteria->compare('ibu_kebangsaan',$this->ibu_kebangsaan,true);
		$criteria->compare('ibu_alamat',$this->ibu_alamat,true);
		$criteria->compare('ibu_no_rt',$this->ibu_no_rt,true);
		$criteria->compare('ibu_no_rw',$this->ibu_no_rw,true);
		$criteria->compare('ibu_no_prov',$this->ibu_no_prov,true);
		$criteria->compare('ibu_no_kab',$this->ibu_no_kab,true);
		$criteria->compare('ibu_no_kec',$this->ibu_no_kec,true);
		$criteria->compare('ibu_no_kel',$this->ibu_no_kel,true);
		$criteria->compare('ayah_nik',$this->ayah_nik,true);
		$criteria->compare('ayah_nama_lgkp',$this->ayah_nama_lgkp,true);
		$criteria->compare('ayah_tgl_lahir',$this->ayah_tgl_lahir,true);
		$criteria->compare('ayah_wrg_ngr',$this->ayah_wrg_ngr,true);
		$criteria->compare('ayah_pekerjaan',$this->ayah_pekerjaan,true);
		$criteria->compare('ayah_kebangsaan',$this->ayah_kebangsaan,true);
		$criteria->compare('ayah_alamat',$this->ayah_alamat,true);
		$criteria->compare('ayah_no_rt',$this->ayah_no_rt,true);
		$criteria->compare('ayah_no_rw',$this->ayah_no_rw,true);
		$criteria->compare('ayah_no_prov',$this->ayah_no_prov,true);
		$criteria->compare('ayah_no_kab',$this->ayah_no_kab,true);
		$criteria->compare('ayah_no_kec',$this->ayah_no_kec,true);
		$criteria->compare('ayah_no_kel',$this->ayah_no_kel,true);
		$criteria->compare('plpr_nik',$this->plpr_nik,true);
		$criteria->compare('plpr_tgl_lapor',$this->plpr_tgl_lapor,true);
		$criteria->compare('plpr_umur',$this->plpr_umur,true);
		$criteria->compare('plpr_nama_lgkp',$this->plpr_nama_lgkp,true);
		$criteria->compare('plpr_kelamin',$this->plpr_kelamin,true);
		$criteria->compare('plpr_pekerjaan',$this->plpr_pekerjaan,true);
		$criteria->compare('plpr_alamat',$this->plpr_alamat,true);
		$criteria->compare('plpr_no_rt',$this->plpr_no_rt,true);
		$criteria->compare('plpr_no_rw',$this->plpr_no_rw,true);
		$criteria->compare('plpr_no_prov',$this->plpr_no_prov,true);
		$criteria->compare('plpr_no_kab',$this->plpr_no_kab,true);
		$criteria->compare('plpr_no_kec',$this->plpr_no_kec,true);
		$criteria->compare('plpr_no_kel',$this->plpr_no_kel,true);
		$criteria->compare('plpr_hub_kel',$this->plpr_hub_kel,true);
		$criteria->compare('saksi1_nik',$this->saksi1_nik,true);
		$criteria->compare('saksi1_nama_lgkp',$this->saksi1_nama_lgkp,true);
		$criteria->compare('saksi1_umur',$this->saksi1_umur,true);
		$criteria->compare('saksi1_kelamin',$this->saksi1_kelamin,true);
		$criteria->compare('saksi1_pekerjaan',$this->saksi1_pekerjaan,true);
		$criteria->compare('saksi1_alamat',$this->saksi1_alamat,true);
		$criteria->compare('saksi1_no_rt',$this->saksi1_no_rt,true);
		$criteria->compare('saksi1_no_rw',$this->saksi1_no_rw,true);
		$criteria->compare('saksi1_no_prov',$this->saksi1_no_prov,true);
		$criteria->compare('saksi1_no_kab',$this->saksi1_no_kab,true);
		$criteria->compare('saksi1_no_kec',$this->saksi1_no_kec,true);
		$criteria->compare('saksi1_no_kel',$this->saksi1_no_kel,true);
		$criteria->compare('saksi2_nik',$this->saksi2_nik,true);
		$criteria->compare('saksi2_nama_lgkp',$this->saksi2_nama_lgkp,true);
		$criteria->compare('saksi2_umur',$this->saksi2_umur,true);
		$criteria->compare('saksi2_kelamin',$this->saksi2_kelamin,true);
		$criteria->compare('saksi2_pekerjaan',$this->saksi2_pekerjaan,true);
		$criteria->compare('saksi2_alamat',$this->saksi2_alamat,true);
		$criteria->compare('saksi2_no_rt',$this->saksi2_no_rt,true);
		$criteria->compare('saksi2_no_rw',$this->saksi2_no_rw,true);
		$criteria->compare('saksi2_kode_pos',$this->saksi2_kode_pos,true);
		$criteria->compare('saksi2_no_prov',$this->saksi2_no_prov,true);
		$criteria->compare('saksi2_no_kab',$this->saksi2_no_kab,true);
		$criteria->compare('saksi2_no_kec',$this->saksi2_no_kec,true);
		$criteria->compare('saksi2_no_kel',$this->saksi2_no_kel,true);
		$criteria->compare('adm_no_kedubes',$this->adm_no_kedubes,true);
		$criteria->compare('adm_no_konjen',$this->adm_no_konjen,true);
		$criteria->compare('adm_no_konsul',$this->adm_no_konsul,true);
		$criteria->compare('adm_no_prov',$this->adm_no_prov,true);
		$criteria->compare('adm_no_kab',$this->adm_no_kab,true);
		$criteria->compare('adm_no_kec',$this->adm_no_kec,true);
		$criteria->compare('adm_no_kel',$this->adm_no_kel,true);
		$criteria->compare('adm_akta_no',$this->adm_akta_no,true);
		$criteria->compare('adm_dokumen',$this->adm_dokumen,true);
		$criteria->compare('adm_sp_no',$this->adm_sp_no,true);
		$criteria->compare('adm_sp_tgl',$this->adm_sp_tgl,true);
		$criteria->compare('adm_sp_nama',$this->adm_sp_nama,true);
		$criteria->compare('adm_akta_tgl',$this->adm_akta_tgl,true);
		$criteria->compare('adm_akta_ktp_tgl',$this->adm_akta_ktp_tgl,true);
		$criteria->compare('adm_kades_nama',$this->adm_kades_nama,true);
		$criteria->compare('adm_kades_nip',$this->adm_kades_nip,true);
		$criteria->compare('adm_regs_nama',$this->adm_regs_nama,true);
		$criteria->compare('adm_regs_nip',$this->adm_regs_nip,true);
		$criteria->compare('adm_tgl_entry',$this->adm_tgl_entry,true);
		$criteria->compare('adm_tgl_update',$this->adm_tgl_update,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('flag_update',$this->flag_update,true);
		$criteria->compare('flag_ctk_akta',$this->flag_ctk_akta,true);
		$criteria->compare('flag_ctk_kakta',$this->flag_ctk_kakta,true);
		$criteria->compare('flag_ctk_sk',$this->flag_ctk_sk,true);
		$criteria->compare('flag_status',$this->flag_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CapilLahir the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
