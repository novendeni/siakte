<?php

/**
 * This is the model class for table "tbl_administrasi_kependudukan".
 *
 * The followings are the available columns in table 'tbl_administrasi_kependudukan':
 * @property integer $ad_id
 * @property string $jenis
 * @property string $keterangan
 *
 * The followings are the available model relations:
 * @property Pengajuan[] $pengajuans
 * @property PersyaratanAdduk[] $persyaratanAdduks
 */
class AdministrasiKependudukan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_administrasi_kependudukan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ad_id, jenis, keterangan', 'required'),
			array('ad_id', 'numerical', 'integerOnly'=>true),
			array('jenis, keterangan', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ad_id, jenis, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pengajuans' => array(self::HAS_MANY, 'Pengajuan', 'ad_id'),
			'persyaratanAdduks' => array(self::HAS_MANY, 'PersyaratanAdduk', 'ad_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ad_id' => 'Ad',
			'jenis' => 'Jenis',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ad_id',$this->ad_id);
		$criteria->compare('jenis',$this->jenis,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdministrasiKependudukan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
